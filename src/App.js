import React from 'react';

import ReduxView from './store';
import I18n from './components/Base/I18n';
import NetworkIndicator from './components/Base/NetworkIndicator';
import Toast from './components/Base/Toast';
import Route from './route';

export default class App extends React.PureComponent {
  render() {
    return (
      <ReduxView>
        <I18n>
          <NetworkIndicator />
          <Toast />
          <Route />
        </I18n>
      </ReduxView>
    );
  }
}
