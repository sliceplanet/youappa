import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonWhite from '../../../Base/Buttons/ButtonWhite';
import Input from '../../../Base/Input';
import AvatarSelection from '../../../Base/Avatars/AvatarSelection';
import RelativeSelection from '../../../Base/RelativeSelection';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';
import {RELATIVE, getUser} from '../../../../helpers';

// Style
import {base} from './styles';

export default class AvatarFriend extends React.PureComponent {
  constructor(props) {
    super(props);

    const {type} = getUser(props.user);

    this.state = {
      relative: type || -1,
      avatar: '',
    };
  }

  onPressRelativeSelection = relative => {
    this.setState({relative});
  };

  onPressIcon = () => {
    this.setState({is_your_type: false});
  };

  onChangeYourType = yourType => {
    this.setState({yourType});
  };

  changeYourType = () => {
    this.setState({is_your_type: true});
  };

  onChange = avatar => {
    this.setState({avatar});
  };

  navigateInfoFriend = () => {
    NavigationService.navigate('InfoFriend');
  };

  check = () => {
    const {relative, avatar} = this.state;

    if (relative === -1) {
      this.props.showToast(I18n.t('choose_friend_type'));
      return;
    }
    if (avatar.length === 0) {
      this.props.showToast(I18n.t('upload_your_photo'));
      return;
    }

    const {token} = this.props.user;
    const data = {
      type: relative,
      avatar_image: avatar,
      used_avatar_source: 'url',
    };

    this.props.postUpdate({token, data});
    NavigationService.navigate('InfoFriend');
  };

  renderYourType = () => {
    if (this.state.is_your_type) {
      return (
        <Input
          style={base.padding}
          onChangeText={this.onChangeYourType}
          value={this.state.yourType}
          placeholder={I18n.t('for_example')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          autoCapitalize="sentences"
          icon={Images.close}
          onPressIcon={this.onPressIcon}
        />
      );
    }
    return (
      <ButtonWhite
        onPress={this.changeYourType}
        style={base.padding}
        gradientColors={['#A3A3A3', '#A3A3A3']}
        titleColor="#838080"
        title={I18n.t('add_your_type')}
      />
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        <AvatarSelection friend onChange={this.onChange} />

        <RelativeSelection
          data={RELATIVE()}
          value={this.state.relative}
          onPress={this.onPressRelativeSelection}
        />

        {/* {this.renderYourType()} */}

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </View>
    );
  }
}
