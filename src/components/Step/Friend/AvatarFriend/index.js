import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {postUpdate} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
    postUpdate: item => dispatch(postUpdate(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
