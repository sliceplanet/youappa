import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import Input from '../../../Base/Input';

// Helpers
import {getUser} from '../../../../helpers';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InfoFriend extends React.PureComponent {
  constructor(props) {
    super(props);

    const {name, city, about} = getUser(props.user);

    this.state = {
      name,
      city,
      about,
    };
  }

  onChangeTextName = name => {
    this.setState({name});
  };

  onChangeTextCity = city => {
    this.setState({city});
  };

  onChangeTextAbout = about => {
    this.setState({about});
  };

  check = () => {
    const {name, city, about} = this.state;
    if (name.length === 0) {
      this.props.showToast(I18n.t('enter_baby_name'));
      return;
    }
    if (city.length === 0) {
      this.props.showToast(I18n.t('enter_the_name_of_the_city'));
      return;
    }

    const {token} = this.props.user;
    const data = {
      name,
      city,
      about,
      registrationStep: 1,
    };

    this.props.postUpdate({token, data});
  };

  render() {
    return (
      <View style={base.wrap}>
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextName}
          value={this.state.name}
          placeholder={I18n.t('enter_your_name')}
          keyboardType="default"
          returnKeyType="next"
          onSubmitEditing={this.nextCity}
          icon={Images.warning}
        />

        <Input
          style={base.margin}
          onChangeText={this.onChangeTextCity}
          value={this.state.city}
          placeholder={I18n.t('enter_your_city')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          icon={Images.target}
        />

        <Input
          style={base.margin}
          onChangeText={this.onChangeTextAbout}
          value={this.state.about}
          placeholder={I18n.t('tell_us_about_yourself')}
          keyboardType="default"
          multiline
        />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="white"
          title={I18n.t('to_complete')}
        />
      </View>
    );
  }
}
