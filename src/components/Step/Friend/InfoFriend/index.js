import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {postUpdate} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
    postUpdate: data => dispatch(postUpdate(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
