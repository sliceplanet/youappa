import {connect} from 'react-redux';
import component from './component';

import {putUpdatePregnancy} from '../../../../store/actions/pregnancy';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putUpdatePregnancy: item => dispatch(putUpdatePregnancy(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
