import React, {Component} from 'react';
import {View, Text} from 'react-native';
import I18n from 'i18n-js';

// Components
import ButtonWhite from '../../../Base/Buttons/ButtonWhite';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import GestationalSwiper from '../../../Base/GestationalSwiper';

// Helpers
import NavigationService from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class GestationalAge extends Component {
  constructor(props) {
    super(props);

    this.state = {
      week: 0,
    };
  }

  navigateAddChild = () => {
    NavigationService.navigate('InfoMother');
  };

  navigateAddChildParams = () => {
    const {token} = this.props.user;
    const data = {
      active: true,
      babyStage: 1,
      currentWeek: this.state.week,
    };
    this.props.putUpdatePregnancy({token, data});
    // NavigationService.reset('InfoMother');
    NavigationService.reset('AddChild', {born: false});
  };

  onChangeWeek = week => {
    this.setState({week});
  };

  render() {
    return (
      <View style={base.wrap}>
        <Text style={base.title}>{I18n.t('enter_your_week')}</Text>

        <View style={base.flex} />

        <GestationalSwiper onChangeWeek={this.onChangeWeek} />

        <ButtonWhite
          onPress={this.navigateAddChild}
          style={base.padding}
          gradientColors={['#969696', '#969696']}
          titleColor="#969696"
          title={I18n.t('skip_this_step')}
        />
        <ButtonGradient
          onPress={this.navigateAddChildParams}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </View>
    );
  }
}
