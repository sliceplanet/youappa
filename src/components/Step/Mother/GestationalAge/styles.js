import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    // width: wp(100),
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
  },
  padding: {
    paddingVertical: wp(2),
  },
  text: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#555555',
    paddingLeft: wp(2),
  },
  title: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(5),
    color: '#969696',
  },
});

export default {base};
