import {connect} from 'react-redux';
import component from './component';

import {postAddChild} from '../../../../store/actions/child';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postAddChild: data => dispatch(postAddChild(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
