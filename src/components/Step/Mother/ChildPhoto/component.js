import React from 'react';
import {View, Text} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonIconText from '../../../Base/Buttons/ButtonIconText';
import ChildCarousel from '../../../Base/Children/ChildCarousel';
import AvatarSelection from '../../../Base/Avatars/AvatarSelection';
import Wrap from '../../../Base/Wrap';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ChildPhoto extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      key: 0,
      photo: '',
      child: props.navigation.getParam('child', null),
    };
  }

  onSnapToItem = key => {
    this.setState({key});
  };

  onPressCamera = () => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          photo: `data:${response.type};base64,${response.data}`,
        });
      }
    });
  };

  onPressDeleteAvatar = () => {
    this.setState({
      photo: '',
    });
  };

  navigateInfoMother = () => {
    NavigationService.navigate('InfoMother');
  };

  check = () => {
    const {key, photo} = this.state;
    const {name, sex, date, cover, born, access} = this.state.child;
    const {token} = this.props.user;

    const data = {
      name,
      gender: sex === 'boy' ? 2 : 1,
      accessLevel: access,
      backgroundId: cover,
    };

    if (born) {
      data.isBorn = true;
      data.bornDate = date;
    }

    if (photo.length > 0) {
      data.photoImage = photo;
      data.usedPhotoSource = 'url';
    } else {
      data.photoId = key;
      data.usedPhotoSource = 'id';
    }

    this.props.postAddChild({token, data});
    NavigationService.reset('InfoMother');
  };

  onChange = photo => {
    this.setState({photo});
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        {this.state.photo.length > 0 ? (
          <AvatarSelection
            mother
            onChange={this.onChange}
            value={this.state.photo}
          />
        ) : (
          <ChildCarousel onSnapToItem={this.onSnapToItem} />
        )}

        <Text style={base.title}>
          {I18n.t('you_can_upload_a_real_photo_of_your_child')}
        </Text>

        <View style={base.flex} />

        {this.state.photo.length > 0 ? (
          <ButtonIconText
            onPress={this.onPressDeleteAvatar}
            icon={Images.camera}
            width={wp(50)}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="#D96F22"
            title={I18n.t('delete_avatar')}
          />
        ) : (
          <ButtonIconText
            onPress={this.onPressCamera}
            icon={Images.camera}
            width={wp(50)}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="#D96F22"
            title={I18n.t('choose_from_gallery')}
          />
        )}

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </Wrap>
    );
  }
}
