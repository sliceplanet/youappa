import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    width: wp(100),
    alignItems: 'center',
    paddingBottom: wp(4),
    backgroundColor: '#F4F4F6',
  },
  padding: {
    paddingVertical: wp(2),
  },
  gallery: {
    paddingVertical: wp(2),
    width: wp(50),
  },
  margin: {
    marginBottom: wp(2),
  },
  text: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#555555',
    paddingLeft: wp(2),
  },
  title: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    textAlign: 'center',
    color: '#949494',
    paddingVertical: wp(4),
  },
});

export default {base};
