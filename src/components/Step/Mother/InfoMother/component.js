import React from 'react';
import {View} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonFull from '../../../Base/Buttons/ButtonFull';
import Input from '../../../Base/Input';
import ChildList from '../../../Base/Children/ChildList';
import Wrap from '../../../Base/Wrap';

// Helpers
import {getUser} from '../../../../helpers';
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InfoMother extends React.PureComponent {
  constructor(props) {
    super(props);

    const {name, city, about} = getUser(props.user);

    this.state = {
      name,
      city,
      about,
    };
  }

  componentDidMount() {
    const {token} = this.props.user;
    this.props.getMyChild({token});
  }

  onChangeTextName = name => {
    this.setState({name});
  };

  onChangeTextCity = city => {
    this.setState({city});
  };

  onChangeTextAbout = about => {
    this.setState({about});
  };

  nextCity = () => {
    this.city.focus();
  };

  navigateAddChild = () => {
    const {name, city, about} = this.state;
    const {token} = this.props.user;
    const data = {
      name,
      city,
      about,
    };
    this.props.postUpdate({token, data});
    NavigationService.navigate('AddChild');
  };

  check = () => {
    const {name, city, about} = this.state;
    const {token} = this.props.user;
    if (name.length === 0) {
      this.props.showToast(I18n.t('enter_baby_name'));
      return;
    }
    if (city.length === 0) {
      this.props.showToast(I18n.t('enter_the_name_of_the_city'));
      return;
    }

    const data = {
      name,
      city,
      about,
    };
    this.props.postUpdate({token, data});

    const u = getUser(this.props.user);
    if (u.registrationStep === 1) {
      NavigationService.reset('Home');
    } else {
      NavigationService.reset('LoadAvatar');
    }
  };

  ref = ref => {
    this.city = ref;
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextName}
          value={this.state.name}
          placeholder={I18n.t('enter_your_name')}
          keyboardType="default"
          returnKeyType="next"
          onSubmitEditing={this.nextCity}
          icon={Images.warning}
        />

        <Input
          ref={this.ref}
          style={base.margin}
          onChangeText={this.onChangeTextCity}
          value={this.state.city}
          placeholder={I18n.t('enter_your_city')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          icon={Images.target}
        />

        <ButtonFull
          icon={Images.childPlus}
          title={I18n.t('add_a_child')}
          onPress={this.navigateAddChild}
          height={wp(8)}
        />

        <ChildList />

        <Input
          style={base.margin}
          onChangeText={this.onChangeTextAbout}
          value={this.state.about}
          placeholder={I18n.t('tell_us_about_yourself')}
          keyboardType="default"
          multiline
        />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </Wrap>
    );
  }
}
