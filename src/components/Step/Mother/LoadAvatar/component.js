import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import AvatarSelection from '../../../Base/Avatars/AvatarSelection';
import Wrap from '../../../Base/Wrap';

// Style
import {base} from './styles';

export default class LoadAvatar extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      avatar: '',
    };
  }

  onChange = avatar => {
    this.setState({avatar});
  };

  check = () => {
    const {avatar} = this.state;
    const {token} = this.props.user;
    const data = {
      avatar_image: avatar,
      used_avatar_source: 'url',
      registrationStep: 1,
    };
    this.props.postUpdate({token, data});
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <AvatarSelection mother onChange={this.onChange} />
        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('to_complete')}
        />
      </Wrap>
    );
  }
}
