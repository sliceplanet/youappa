import {connect} from 'react-redux';
import component from './component';

import {postUpdate} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    babies: state.babies,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postUpdate: item => dispatch(postUpdate(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
