import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    paddingBottom: wp(4),
    paddingHorizontal: wp(5),
    backgroundColor: '#F4F4F6',
  },
  padding: {
    paddingVertical: wp(2),
  },
  margin: {
    marginBottom: wp(2),
  },
  text: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#555555',
    paddingLeft: wp(2),
  },
});

export default {base};
