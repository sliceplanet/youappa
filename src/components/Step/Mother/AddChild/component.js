import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';
import moment from 'moment';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonIconText from '../../../Base/Buttons/ButtonIconText';
import Input from '../../../Base/Input';
import ChildSex from '../../../Base/Children/ChildSex';
import CoverSelection from '../../../Base/CoverSelection';
import Wrap from '../../../Base/Wrap';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class AddChild extends React.PureComponent {
  constructor(props) {
    super(props);

    const born = props.navigation.getParam('born', true);

    this.state = {
      name: '',
      date: null,
      sex: '',
      cover: 0,
      access: 0,
      born,
    };
  }

  onChangeTextName = name => {
    this.setState({name});
  };

  onChangeTextDate = date => {
    this.setState({date});
  };

  onPressChildSex = sex => {
    this.setState({sex});
  };

  onPressCoverSelection = cover => {
    this.setState({cover});
  };

  onPressAccess = () => {
    this.setState({
      access: this.state.access === 0 ? 1 : 0,
    });
  };

  navigateChildPhoto = () => {
    NavigationService.navigate('ChildPhoto');
  };

  check = () => {
    const {name, date, sex, cover, born, access} = this.state;
    if (name.length === 0) {
      this.props.showToast(I18n.t('enter_baby_name'));
      return;
    }
    if (born) {
      if (date === null) {
        this.props.showToast(I18n.t('enter_your_baby_date_of_birth'));
        return;
      }
    }
    if (sex.length === 0) {
      this.props.showToast(I18n.t('choose_the_gender_of_the_child'));
      return;
    }

    if (born) {
      NavigationService.navigate('ChildPhoto', {
        child: {
          name,
          date: moment(date).unix(),
          sex,
          cover,
          born,
          access,
        },
      });
    } else {
      NavigationService.navigate('ChildPhoto', {
        child: {
          name,
          sex,
          cover,
          born,
          access,
        },
      });
    }
  };

  render() {
    const {born, access} = this.state;
    return (
      <Wrap style={base.wrap}>
        <Input
          onChangeText={this.onChangeTextName}
          value={this.state.name}
          placeholder={I18n.t('name')}
          keyboardType="default"
          returnKeyType="done"
          autoCapitalize="sentences"
          onSubmitEditing={this.nextDate}
        />
        {born && (
          <Input
            style={base.margin}
            onChangeText={this.onChangeTextDate}
            value={this.state.date}
            placeholder={I18n.t('date_of_birth')}
            date
            maximumDate={moment().toDate()}
          />
        )}

        <ChildSex onPress={this.onPressChildSex} />
        <ButtonIconText
          icon={Images.lock}
          iconStyle={access === 0 ? null : base.tintColor}
          gradientColors={
            access === 0 ? ['#FE695F', '#FE695F'] : ['#969696', '#5E5E5E']
          }
          titleColor={access === 0 ? '#FE695F' : '#5E5E5E'}
          title={I18n.t('access_limitation')}
          onPress={this.onPressAccess}
        />

        <CoverSelection onPress={this.onPressCoverSelection} />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </Wrap>
    );
  }
}
