import React from 'react';
import {View} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonFull from '../../../Base/Buttons/ButtonFull';
import ChildList from '../../../Base/Children/ChildList';
import Wrap from '../../../Base/Wrap';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InfoMother extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const {token} = this.props.user;
    this.props.getMyChild({token});
  }

  navigateAddChild = () => {
    NavigationService.navigate('AddChild');
  };

  check = () => {
    NavigationService.reset('Home');
  };

  ref = ref => {
    this.city = ref;
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <ButtonFull
          icon={Images.childPlus}
          title={I18n.t('add_a_child')}
          onPress={this.navigateAddChild}
          height={wp(8)}
        />

        <ChildList />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </Wrap>
    );
  }
}
