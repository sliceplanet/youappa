import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {postUpdate} from '../../../../store/actions/user';
import {getMyChild, deleteChild} from '../../../../store/actions/child';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
    postUpdate: item => dispatch(postUpdate(item)),
    getMyChild: item => dispatch(getMyChild(item)),
    deleteChild: item => dispatch(deleteChild(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
