import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
