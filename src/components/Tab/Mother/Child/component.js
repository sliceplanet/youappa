import React from 'react';
import {View, Image, Text} from 'react-native';
import I18n from 'i18n-js';
import Swiper from 'react-native-swiper';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ScalableImage from 'react-native-scalable-image';
import moment from 'moment';

// Components
import Wrap from '../../../Base/Wrap';
import Img from '../../../Base/Image';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';
import {AVATARS} from '../../../../helpers';
import {URI} from '../../../../store/api';

// Style
import {base} from './styles';

export default class Child extends React.PureComponent {
  constructor(props) {
    super(props);

    const child = props.navigation.getParam('child', null);
    this.state = {
      child,
    };
  }

  navigateWishlist = () => {
    const {child} = this.state;
    NavigationService.navigate('Wishlist', {child, type: 2});
  };

  render() {
    const {
      name,
      bornDateMs,
      usedPhotoSource,
      photoId,
      photoUrl,
      description,
    } = this.state.child;

    const now = moment();
    const date = moment.unix(bornDateMs);
    const diffYears = now.diff(date, 'years');
    const diffMonth = now.diff(date, 'month');
    const diffDay = now.diff(date, 'day');

    let bornDate = '';
    if (diffYears > 0) {
      bornDate = `${diffYears} ${I18n.t('year')}`;
    } else if (diffMonth > 0) {
      bornDate = `${diffMonth} ${I18n.t(diffMonth > 1 ? 'months' : 'month')}`;
    } else if (diffDay > 0) {
      bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
    } else {
      bornDate = I18n.t('newborn');
    }

    return (
      <Wrap style={base.wrap}>
        <View style={base.position}>
          <Swiper
            showsButtons
            showsPagination={false}
            loop={false}
            nextButton={<Text style={base.button}>›</Text>}
            prevButton={<Text style={base.button}>‹</Text>}>
            {usedPhotoSource === 'url' ? (
              <Img
                style={base.slide}
                containerStyle={base.slide}
                uri={URI + photoUrl}
              />
            ) : (
              <Image style={base.slide} source={AVATARS[photoId]} />
            )}
          </Swiper>
        </View>
        <View style={base.childWrap}>
          <View style={base.padding}>
            <View style={base.row}>
              <View>
                <Text style={base.name}>{name}</Text>
                <Text style={base.year}>{bornDate}</Text>
              </View>
              <View style={base.flex} />
              <ScalableImage
                source={Images.noteButton}
                width={wp(8)}
                onPress={this.navigateWishlist}
              />
            </View>
          </View>
          <View style={base.line} />
          <View style={base.padding}>
            <Text style={base.text}>{description}</Text>
          </View>
        </View>
      </Wrap>
    );
  }
}
