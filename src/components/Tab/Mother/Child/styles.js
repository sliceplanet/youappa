import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    width: wp(100),
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
    paddingTop: wp(82),
  },
  position: {
    position: 'absolute',
    height: wp(100),
  },
  slide: {
    width: wp(100),
    height: wp(100),
  },
  button: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(8),
    color: '#F4F4F6',
    padding: wp(2),
  },
  childWrap: {
    width: wp(80),
    backgroundColor: 'white',
    borderRadius: wp(4),
  },
  name: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    fontWeight: '800',
    color: 'black',
    paddingLeft: wp(2),
  },
  year: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#969696',
    paddingLeft: wp(2),
  },
  padding: {
    padding: wp(4),
  },
  line: {
    width: wp(80),
    height: 0.7,
    backgroundColor: '#F4F4F6',
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#3F3F3F',
    lineHeight: wp('4.5'),
  },
});

export default {base};
