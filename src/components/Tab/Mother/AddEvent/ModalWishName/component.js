import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';

import Input from '../../../../Base/Input';
import ButtonGradient from '../../../../Base/Buttons/ButtonGradient';

// Style
import {base} from './styles';

export default class ModalWishName extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
    };
  }

  onChangeTextTitle = title => {
    this.setState({title});
  };

  check = () => {
    const {title} = this.state;
    if (title.length === 0) {
      this.props.showToast(I18n.t('show_title'));
      return;
    }

    this.props.onSelect(title);
  };

  render() {
    const {isVisible} = this.props;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <Input
            style={base.margin}
            onChangeText={this.onChangeTextTitle}
            value={this.state.title}
            placeholder={I18n.t('title')}
            onSubmitEditing={this.check}
            returnKeyType="done"
          />

          <ButtonGradient
            onPress={this.check}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="white"
            title={I18n.t('next')}
          />
        </View>
      </Modal>
    );
  }
}
