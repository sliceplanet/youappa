import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    alignItems: 'center',
    width: wp(90),
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp(4),
  },
  center: {
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
    marginRight: wp(4),
  },
  desires: {
    marginRight: wp(2),
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  years: {
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  name: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    color: 'black',
    marginBottom: wp(1),
  },
  margin: {
    marginVertical: wp(2),
    width: wp(80),
  },
});

export default {base};
