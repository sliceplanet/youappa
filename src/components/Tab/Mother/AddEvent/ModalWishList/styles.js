import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    maxHeight: hp(70),
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp(4),
  },
  center: {
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: wp(2),
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
    marginRight: wp(4),
  },
  desires: {
    marginRight: wp(2),
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  years: {
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  name: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    color: 'black',
    marginBottom: wp(1),
  },
  align: {
    alignSelf: 'center',
  },
});

export default {base};
