import {connect} from 'react-redux';
import component from './component';

import {getFeedEvent} from '../../../../../store/actions/event';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getFeedEvent: item => dispatch(getFeedEvent(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
