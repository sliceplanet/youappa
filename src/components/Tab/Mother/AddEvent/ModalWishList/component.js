import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Img from '../../../../Base/Image';
import ButtonIconText from '../../../../Base/Buttons/ButtonIconText';

// Helpers
import {AVATARS, getUser} from '../../../../../helpers';
import * as Images from '../../../../../helpers/images';

// Api
import {URI} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class ModalWishList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {isVisible, wishlist, user} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <ScrollView>
            {wishlist.map((e, i) => {
              const {title, type, child} = e;

              let Avatar;
              if (type !== 2) {
                const {avatar_url, avatar_id, used_avatar_source} = getUser(
                  user,
                );

                Avatar =
                  used_avatar_source === 'url' ? (
                    avatar_url ? (
                      <Img
                        uri={`${URI}${avatar_url}`}
                        style={base.image}
                        containerStyle={base.image}
                      />
                    ) : (
                      <Image source={Images.profile} style={base.image} />
                    )
                  ) : (
                    <Image style={base.image} source={AVATARS[avatar_id]} />
                  );
              } else {
                const {usedPhotoSource, photoUrl, photoId} = child;

                Avatar =
                  usedPhotoSource === 'url' ? (
                    photoUrl ? (
                      <Img
                        uri={`${URI}${photoUrl}`}
                        style={base.image}
                        containerStyle={base.image}
                      />
                    ) : (
                      <Image source={Images.profile} style={base.image} />
                    )
                  ) : (
                    <Image style={base.image} source={AVATARS[photoId]} />
                  );
              }

              return (
                <TouchableOpacity
                  key={i}
                  style={base.item}
                  onPress={() => this.props.onSelect(e)}
                  onLongPress={() => this.props.onLongSelect(e)}>
                  {Avatar}
                  <View>
                    <Text style={base.name}>{title}</Text>
                  </View>
                  <View style={base.flex} />
                  <Text style={base.desires}>
                    {e.products.length} {I18n.t('desires')}
                  </Text>
                </TouchableOpacity>
              );
            })}

            <ButtonIconText
              style={base.align}
              onPress={this.props.onPressCreateWishlist}
              width={wp(50)}
              icon={Images.add}
              gradientColors={['#F1A737', '#F2A737']}
              titleColor="#DD7B27"
              title={I18n.t('create_desire')}
            />
          </ScrollView>
        </View>
      </Modal>
    );
  }
}
