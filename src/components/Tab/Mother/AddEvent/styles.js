import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  bgc: {
    width: wp(100),
    paddingTop: wp(4),
    backgroundColor: '#F4F4F6',
  },
  margin: {
    marginVertical: wp(2),
  },
  row: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: wp(7),
  },
  text: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
  },
  wrap: {
    width: wp(90),
    paddingVertical: wp(2),
  },
  center: {
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: wp(12),
    padding: wp(2),
    paddingRight: wp(4),
  },
  image: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(12),
    overflow: 'hidden',
    marginRight: wp(4),
  },
  desires: {
    marginRight: wp(2),
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  years: {
    fontSize: wp(3),
    color: '#949494',
    fontFamily: 'Roboto-Regular',
  },
  name: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    color: 'black',
    marginBottom: wp(1),
  },
});

export default {base};
