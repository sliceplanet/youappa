import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {getFeedEvent} from '../../../../store/actions/event';
import {reducerAddWishlist} from '../../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getFeedEvent: item => dispatch(getFeedEvent(item)),
    reducerAddWishlist: item => dispatch(reducerAddWishlist(item)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
