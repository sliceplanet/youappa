import React from 'react';
import {View, Text, Image} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import Input from '../../../Base/Input';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import Img from '../../../Base/Image';
import Checker from './Checker';
import ModalWishList from './ModalWishList';
import ModalWishName from './ModalWishName';

// Helpers
import * as Images from '../../../../helpers/images';
import {AVATARS, getUser} from '../../../../helpers';
import NavigationService from '../../../../helpers/navigation';

// Api
import {URI} from '../../../../store/api';
import {apiPostAddWishlist} from '../../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class AddEvent extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      checker: 0,
      title: '',
      dateTime: null,
      about: '',
      wl: [],
      isVisibleWishlist: false,
      isVisibleWishName: false,
    };
  }

  onPressChecker = checker => {
    this.setState({checker});
  };

  onPressNote = () => {
    this.setState({
      isVisibleWishlist: true,
    });
  };

  onPressCloseWishlist = () => {
    this.setState({
      isVisibleWishlist: false,
    });
  };

  onPressCloseWishName = () => {
    this.setState({
      isVisibleWishName: false,
    });
  };

  onSelectWishlist = wl => {
    if (this.state.wl.filter(e => e === wl).length === 0) {
      this.setState({
        wl: [...this.state.wl, wl],
        isVisibleWishlist: false,
      });
    } else {
      this.setState({
        isVisibleWishlist: false,
      });
    }
  };

  onLongSelectWishlist = wishlist => {
    this.setState({
      isVisibleWishlist: false,
    });

    NavigationService.navigate('Wishlist', {
      wishlistId: wishlist._id,
      type: 3,
    });
  };

  onSelectWishName = title => {
    this.setState({
      isVisibleWishName: false,
    });

    const {token} = this.props.user;
    const data = {
      title,
      type: 3,
      category: 9,
    };

    apiPostAddWishlist({token, data})
      .then(result => {
        this.props.reducerAddWishlist(result.data.data);
        NavigationService.navigate('Wishlist', {
          wishlistId: result.data.data._id,
          type: 3,
        });
      })
      .catch(e => console.log(e));
  };

  onPressCreateWishlist = () => {
    this.setState({
      isVisibleWishlist: false,
      isVisibleWishName: true,
    });
  };

  onChangeTextTitle = title => {
    this.setState({title});
  };

  onChangeTextDate = dateTime => {
    this.setState({dateTime});
  };

  onChangeTextAbout = about => {
    this.setState({about});
  };

  check = () => {
    const {title, dateTime, about} = this.state;

    if (title.length === 0) {
      this.props.showToast(I18n.t('show_title'));
      console.log('object');
      return;
    }
    if (dateTime === null) {
      this.props.showToast(I18n.t('show_date'));
      return;
    }
    if (about.length === 0) {
      this.props.showToast(I18n.t('show_about'));
      return;
    }

    NavigationService.navigate('InviteFriends', {item: this.state});
  };

  removeItem = item => {
    this.setState({
      wl: this.state.wl.filter(e => e !== item),
    });
  };

  render() {
    const {isVisibleWishlist, isVisibleWishName, wl} = this.state;
    const {wishlist, user} = this.props;

    return (
      <Wrap style={base.bgc}>
        <Checker onPress={this.onPressChecker} />
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextTitle}
          value={this.state.title}
          placeholder={I18n.t('title')}
          returnKeyType="next"
        />
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextDate}
          value={this.state.dateTime}
          date
          time
          placeholder={I18n.t('dateTime')}
          returnKeyType="done"
          icon={Images.clock}
        />

        <View style={base.row}>
          <Text style={base.text}>{I18n.t('select_wishlist')}</Text>
          <View style={base.flex} />
          <ScalableImage
            source={Images.addNote}
            height={wp(8)}
            onPress={this.onPressNote}
          />
        </View>

        {isVisibleWishlist && (
          <ModalWishList
            isVisible
            wishlist={wishlist}
            onPressClose={this.onPressCloseWishlist}
            onSelect={this.onSelectWishlist}
            onLongSelect={this.onLongSelectWishlist}
            onPressCreateWishlist={this.onPressCreateWishlist}
          />
        )}

        {isVisibleWishName && (
          <ModalWishName
            isVisible
            onPressClose={this.onPressCloseWishName}
            onSelect={this.onSelectWishName}
          />
        )}

        <View style={base.wrap}>
          {wl.length > 0 &&
            wl.map((e, i) => {
              const {title, type, child} = e;

              let Avatar;
              if (type !== 2) {
                const {avatar_url, avatar_id, used_avatar_source} = getUser(
                  user,
                );

                Avatar =
                  used_avatar_source === 'url' ? (
                    avatar_url ? (
                      <Img
                        uri={`${URI}${avatar_url}`}
                        style={base.image}
                        containerStyle={base.image}
                      />
                    ) : (
                      <Image source={Images.profile} style={base.image} />
                    )
                  ) : (
                    <Image style={base.image} source={AVATARS[avatar_id]} />
                  );
              } else {
                const {usedPhotoSource, photoUrl, photoId} = child;

                Avatar =
                  usedPhotoSource === 'url' ? (
                    photoUrl ? (
                      <Img
                        uri={`${URI}${photoUrl}`}
                        style={base.image}
                        containerStyle={base.image}
                      />
                    ) : (
                      <Image source={Images.profile} style={base.image} />
                    )
                  ) : (
                    <Image style={base.image} source={AVATARS[photoId]} />
                  );
              }

              return (
                <View style={base.item} key={i}>
                  {Avatar}

                  <View>
                    <Text style={base.name}>{title}</Text>
                  </View>
                  <View style={base.flex} />
                  <Text style={base.desires}>
                    {e.products.length} {I18n.t('desires')}
                  </Text>
                  <ScalableImage
                    source={Images.close}
                    width={wp(4)}
                    onPress={() => this.removeItem(e)}
                  />
                </View>
              );
            })}
        </View>

        <Input
          style={base.margin}
          onChangeText={this.onChangeTextAbout}
          value={this.state.about}
          placeholder={I18n.t('description')}
          keyboardType="default"
          multiline
        />
        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('next')}
        />
      </Wrap>
    );
  }
}
