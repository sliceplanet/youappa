import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import I18n from 'i18n-js';

// Style
import {base} from './styles';

export default class Checker extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      checker: 0,
    };
  }

  onPressMeasures = () => {
    this.setState({
      checker: 0,
    });
    this.props.onPress(0);
  };

  onPressEvents = () => {
    this.setState({
      checker: 1,
    });
    this.props.onPress(1);
  };

  render() {
    const {checker} = this.state;
    return (
      <View style={base.bgBorder}>
        <View style={[base.itemWrap, checker === 0 ? base.select : null]}>
          <TouchableOpacity onPress={this.onPressMeasures}>
            <Text style={[base.text, checker === 0 ? base.selectText : null]}>
              {I18n.t('measures')}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={[base.itemWrap, checker === 1 ? base.select : null]}>
          <TouchableOpacity onPress={this.onPressEvents}>
            <Text style={[base.text, checker === 1 ? base.selectText : null]}>
              {I18n.t('events')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
