import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  itemWrap: {
    flex: 1,
    alignItems: 'center',
  },
  bgBorder: {
    flexDirection: 'row',
    width: wp(90),
    backgroundColor: '#CECCCC',
    borderRadius: wp(6),
  },
  text: {
    fontSize: wp(4),
    color: '#797979',
    paddingVertical: wp(3),
    fontFamily: 'Roboto-Regular',
  },
  select: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: wp(6),
  },
  selectText: {
    color: '#DD7B27',
  },
});

export default {base};
