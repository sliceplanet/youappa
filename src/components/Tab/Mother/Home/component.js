import React from 'react';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarView from './AvatarView';
import ListView from './ListView';

// Style
import {base} from './styles';

export default class Home extends React.PureComponent {
  render() {
    return (
      <Wrap noScroll tab type="mother" style={base.wrap}>
        <AvatarView />
        <ListView />
      </Wrap>
    );
  }
}
