import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    backgroundColor: '#F4F4F6',
  },
});

export default {base};
