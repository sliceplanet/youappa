import React from 'react';
import {View, Text, Image as Img} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Image from '../../../../Base/Image';

// Api
import {URI} from '../../../../../store/api';

// Helpers
import {getUser} from '../../../../../helpers';
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class AvatarView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const user = getUser(this.props.user);
    if (user === null) {
      return null;
    }

    const {name, city, avatar_url} = user;
    return (
      <View style={base.wrap}>
        {avatar_url ? (
          <Image
            uri={URI + avatar_url}
            style={base.image}
            containerStyle={base.image}
          />
        ) : (
          <Img source={Images.profile} style={base.image} />
        )}

        <View style={base.row}>
          <View>
            <View style={base.row}>
              <ScalableImage source={Images.location} height={wp(4)} />
              <Text style={base.location}>
                {I18n.t('c')} {city}
              </Text>
            </View>
            <Text style={base.text}>{name}</Text>
          </View>
        </View>
        <View style={base.flex} />
      </View>
    );
  }
}
