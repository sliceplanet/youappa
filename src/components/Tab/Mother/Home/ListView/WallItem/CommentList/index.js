import {connect} from 'react-redux';
import component from './component';

import {getCommentPost} from '../../../../../../../store/actions/comment';
import {
  putLikeComment,
  putUnlikeComment,
} from '../../../../../../../store/actions/like';

function mapStateToProps(state) {
  return {
    user: state.user,
    comments: state.comments,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getCommentPost: data => dispatch(getCommentPost(data)),
    putLikeComment: data => dispatch(putLikeComment(data)),
    putUnlikeComment: data => dispatch(putUnlikeComment(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
