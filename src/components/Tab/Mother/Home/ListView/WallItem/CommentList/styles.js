import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  allComments: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#949494',
    paddingHorizontal: wp(5),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(2),
  },
  time: {
    marginLeft: wp(2),
    fontSize: wp(3),
    width: wp(30),
  },
  count: {
    fontSize: wp(3),
  },
  text: {
    width: wp(80),
    paddingHorizontal: wp(2),
    fontSize: wp(3),
    fontFamily: 'Roboto-Regular',
    color: 'black',
  },
  bold: {
    fontWeight: 'bold',
  },
  image: {
    width: wp(6),
    height: wp(6),
    marginLeft: wp(4),
    borderRadius: wp(6),
    overflow: 'hidden',
  },
});

export default {base};
