import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wall: {
    width: wp(100),
    height: wp(100),
  },
  width: {
    width: wp(100),
  },
  position: {
    position: 'absolute',
    alignSelf: 'center',
    top: wp(50),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp(5),
    paddingVertical: wp(2),
  },
  likeText: {
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(4),
    color: '#949494',
    paddingHorizontal: wp(2),
  },
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp('3.5'),
    color: '#555555',
    paddingHorizontal: wp(5),
  },
  bold: {
    fontFamily: 'SFUIDisplay-Medium',
    fontWeight: '800',
  },
  date: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#949494',
    paddingHorizontal: wp(5),
    paddingTop: wp(1),
    paddingBottom: wp(2),
  },
  indicator: {
    marginVertical: wp(2),
  },
});

export default {base};
