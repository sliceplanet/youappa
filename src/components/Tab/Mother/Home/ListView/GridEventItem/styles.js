import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  grid: {
    width: wp(30),
    height: wp(30),
    margin: wp(1),
    borderRadius: wp(30),
  },
  linearGradient: {
    width: wp(30),
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    bottom: wp(1),
    left: wp(1),
    borderRadius: wp(9),
    borderWidth: wp(1),
    borderColor: 'white',
  },
  title: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: 'white',
    paddingLeft: wp(2),
    paddingRight: wp(4),
  },
  noteWrap: {
    width: wp(9),
    height: wp(9),
    borderRadius: wp(9),
    backgroundColor: '#069986',
    alignItems: 'center',
  },
  note: {
    tintColor: 'white',
  },
});

export default {base};
