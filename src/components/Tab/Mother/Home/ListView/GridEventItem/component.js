import React from 'react';
import {View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import Image from '../../../../../Base/Image';

// Helpers
import * as Images from '../../../../../../helpers/images';

// Api
import {URI} from '../../../../../../store/api';
import {apiGetEvent} from '../../../../../../store/api/event';

// Style
import {base} from './styles';

export default class GridEventItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  componentDidMount() {
    const {token} = this.props.user;
    const {_id} = this.props;
    const patch = {
      eventId: _id,
    };
    apiGetEvent({token, patch})
      .then(result => {
        this.setState({
          isVisible: true,
          ...result.data.data,
        });
      })
      .catch(e => console.log(e));
  }

  render() {
    const {isVisible, title} = this.state;
    if (isVisible) {
      const {photos} = this.state;
      if (photos.length > 0) {
        return (
          <View>
            <Image
              style={base.grid}
              containerStyle={base.grid}
              uri={URI + photos[0].path}
            />
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#4B4B4B', '#7C7C7C']}
              style={base.linearGradient}>
              <View style={base.noteWrap}>
                <View style={base.flex} />
                <ScalableImage
                  style={base.note}
                  source={Images.note}
                  height={wp(5)}
                />
                <View style={base.flex} />
              </View>
              <Text style={base.title}>{title}</Text>
            </LinearGradient>
          </View>
        );
      }
    }
    return null;
  }
}
