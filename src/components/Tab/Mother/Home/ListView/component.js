import React from 'react';
import moment from 'moment';
import {View, Text, FlatList} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import GridItem from './GridItem';
import GridEventItem from './GridEventItem';
import WallItem from './WallItem';
import WallEventItem from './WallEventItem';

// Helpers
import {getUser} from '../../../../../helpers';
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class ListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      view: 'grid',
    };
  }

  onPressIconWall = () => {
    this.setState({view: 'wall'});
  };

  onPressIconGrid = () => {
    this.setState({view: 'grid'});
  };

  renderWallItem = ({item, index}) => {
    if (item.typePost === 'post') return <WallItem key={index} {...item} />;
    return <WallEventItem key={index} {...item} />;
  };

  renderGridItem = ({item, index}) => {
    if (item.typePost === 'post') return <GridItem key={index} {...item} />;
    return <GridEventItem key={index} {...item} />;
  };

  renderFlatList = data => {
    if (this.state.view === 'wall') {
      return (
        <FlatList
          key="wall"
          data={data}
          extraData={this.props}
          renderItem={this.renderWallItem}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
    return (
      <FlatList
        key="grid"
        data={data}
        extraData={this.props}
        horizontal={false}
        numColumns={3}
        renderItem={this.renderGridItem}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  };

  render() {
    const {postCount, eventCount} = getUser(this.props.user);
    const {post, events} = this.props;
    const data = [
      ...post.data.map(e => {
        return {...e, typePost: 'post'};
      }),
      ...events.map(e => {
        return {...e, typePost: 'event'};
      }),
    ].sort((a, b) => moment(b.createdAt) - moment(a.createdAt));

    const count =
      postCount + eventCount < data.length
        ? data.length
        : postCount + eventCount;

    return (
      <View style={base.wrap}>
        <View style={base.row}>
          <Text style={base.title}>
            {count} {I18n.t('photo')}
          </Text>
          <View style={base.flex} />
          {this.state.view === 'wall' ? (
            <ScalableImage
              source={Images.wall}
              height={wp(4)}
              style={[base.padding, base.select]}
              onPress={this.onPressIconWall}
            />
          ) : (
            <ScalableImage
              source={Images.wall}
              height={wp(4)}
              style={base.padding}
              onPress={this.onPressIconWall}
            />
          )}
          {this.state.view === 'grid' ? (
            <ScalableImage
              source={Images.grid}
              height={wp(4)}
              style={[base.padding, base.select]}
              onPress={this.onPressIconGrid}
            />
          ) : (
            <ScalableImage
              source={Images.grid}
              height={wp(4)}
              style={base.padding}
              onPress={this.onPressIconGrid}
            />
          )}
        </View>
        {this.renderFlatList(data)}
      </View>
    );
  }
}
