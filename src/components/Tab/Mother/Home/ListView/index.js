import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    user: state.user,
    post: state.post,
    events: state.events,
  };
}

export default connect(mapStateToProps, null)(component);
