import React from 'react';

import Image from '../../../../../Base/Image';

// Api
import {URI} from '../../../../../../store/api';

// Style
import {base} from './styles';

export default class GridItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {photos} = this.props;

    if (photos) {
      if (photos.length > 0) {
        return (
          <Image
            style={base.grid}
            containerStyle={base.grid}
            uri={URI + photos[0].path}
          />
        );
      }
    }
    return null;
  }
}
