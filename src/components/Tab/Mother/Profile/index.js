import {connect} from 'react-redux';
import component from './component';

import {putUpdateChild} from '../../../../store/actions/child';

function mapStateToProps(state) {
  return {
    user: state.user,
    babies: state.babies,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putUpdateChild: data => dispatch(putUpdateChild(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
