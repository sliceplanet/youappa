import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
    paddingTop: wp(8),
  },
  carousel: {
    // height: hp(60)
  },
  center: {
    alignSelf: 'center',
  },
  img: {
    width: wp(68),
    height: hp(30),
    borderRadius: wp(2),
    overflow: 'hidden',
  },
  dotStyle: {
    width: wp(3),
    height: wp(3),
    borderRadius: wp(3),
    marginHorizontal: 2,
    backgroundColor: 'white',
    borderColor: '#F1A737',
    borderWidth: 1.5,
  },
  inactiveDotStyle: {
    backgroundColor: '#C4C4C4',
    borderWidth: 0,
  },
  itemWrap: {
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp('2.5'),
    alignItems: 'center',
  },
  image: {
    position: 'absolute',
    width: wp(70) - 1,
    height: wp(70),
    left: wp('2.5'),
    top: wp('3'),
    backgroundColor: 'black',
  },
  bImage: {
    width: wp(60),
    height: hp(30),
  },
  height: {
    height: hp(32),
  },
  iconWrapLeft: {
    backgroundColor: 'white',
    padding: wp(3),
    borderBottomRightRadius: wp(2),
  },
  iconWrapRight: {
    backgroundColor: 'white',
    padding: wp(3),
    borderBottomLeftRadius: wp(2),
  },
  name: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    fontWeight: '800',
    color: 'black',
  },
  year: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#969696',
  },
  wishlist: {
    width: wp(60),
    marginVertical: wp(4),
  },
  tintColor: {
    tintColor: '#A0A0A0',
  },
  tintColorO: {
    tintColor: '#E07F2A',
  },
});

export default {base};
