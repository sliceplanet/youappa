import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import I18n from 'i18n-js';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import ImagePicker from 'react-native-image-picker';
import ScalableImage from 'react-native-scalable-image';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import moment from 'moment';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonIconText from '../../../Base/Buttons/ButtonIconText';
import Image from '../../../Base/Image';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';
import {AVATARS} from '../../../../helpers';

// Api
import {URI} from '../../../../store/api';

// Style
import {base} from './styles';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSlide: 0,
      wishlistId: '',
    };
  }

  onSnapToItem = activeSlide => {
    this.setState({activeSlide});
  };

  onPressChildEdit = child => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const {token} = this.props.user;
        const data = {
          photoImage: `data:${response.type};base64,${response.data}`,
          usedPhotoSource: 'url',
        };
        const patch = {
          childId: child._id,
        };

        this.props.putUpdateChild({token, data, patch});
      }
    });
  };

  navigateWishlist = child => {
    NavigationService.navigate('Wishlist', {child, type: 2});
  };

  navigateChild = child => {
    NavigationService.navigate('Child', {child});
  };

  renderItem = ({item, index}) => {
    const {name, bornDateMs, usedPhotoSource, photoUrl, photoId} = item;

    const now = moment();
    const date = moment.unix(bornDateMs);
    const diffYears = now.diff(date, 'years');
    const diffMonth = now.diff(date, 'month');
    const diffDay = now.diff(date, 'day');

    let bornDate = '';
    if (diffYears > 0) {
      bornDate = `${diffYears} ${I18n.t('year')}`;
    } else if (diffMonth > 0) {
      bornDate = `${diffMonth} ${I18n.t(diffMonth > 1 ? 'months' : 'month')}`;
    } else if (diffDay > 0) {
      bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
    } else {
      bornDate = I18n.t('newborn');
    }

    return (
      <TouchableOpacity key={index} onPress={() => this.navigateChild(item)}>
        <View style={base.itemWrap}>
          <View style={base.height}>
            {usedPhotoSource === 'url' && photoUrl ? (
              <Image
                uri={URI + photoUrl}
                style={base.img}
                containerStyle={base.img}
              />
            ) : (
              <ScalableImage
                source={AVATARS[photoId]}
                width={wp(60)}
                height={hp(30)}
              />
            )}
          </View>
          <Text style={base.name}>{name}</Text>
          <Text style={base.year}>{bornDate}</Text>
          <View style={base.wishlist}>
            <View style={[base.center, base.row]}>
              <ButtonIconText
                onPress={() => this.navigateWishlist(item)}
                icon={Images.note}
                iconStyle={base.tintColorO}
                width={wp(42)}
                gradientColors={['#E07F2A', '#E07F2A']}
                titleColor="#E07F2A"
                title={I18n.t('wishlist')}
              />
            </View>
          </View>
          <View style={[base.row, {position: 'absolute'}]}>
            <View style={base.flex} />
            <View style={base.iconWrapRight}>
              <ScalableImage
                source={Images.pencil}
                height={wp(4)}
                onPress={() => this.onPressChildEdit(item)}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <Wrap tab type="mother" style={base.wrap}>
        <View style={base.carousel}>
          {this.props.babies.length > 0 && (
            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              data={this.props.babies}
              renderItem={this.renderItem}
              sliderWidth={wp(100)}
              itemWidth={wp(75)}
              removeClippedSubviews={false}
              extraData={this.props.babies}
              onSnapToItem={this.onSnapToItem}
            />
          )}
        </View>

        <Pagination
          dotsLength={this.props.babies.length}
          activeDotIndex={this.state.activeSlide}
          dotStyle={base.dotStyle}
          inactiveDotStyle={base.inactiveDotStyle}
          inactiveDotOpacity={1}
        />
        <View style={base.flex} />
      </Wrap>
    );
  }
}
