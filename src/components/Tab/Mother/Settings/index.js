import {connect} from 'react-redux';
import component from './component';

import {postUpdateSettings} from '../../../../store/actions/settings';
import {logout} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
    settings: state.settings,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: data => dispatch(logout(data)),
    postUpdateSettings: data => dispatch(postUpdateSettings(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
