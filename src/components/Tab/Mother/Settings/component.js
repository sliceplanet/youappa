import React from 'react';
import {View, Switch, TouchableOpacity, Text} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';

// Helpers
import NavigationService from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class Settings extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      switch: props.settings.notify,
    };
  }

  onChangeSwitch = () => {
    this.setState({switch: !this.state.switch}, () => {
      const {token} = this.props.user;
      const data = {
        notify: this.state.switch,
      };
      this.props.postUpdateSettings({token, data});
    });
  };

  onPressLogout = () => {
    const {token} = this.props.user;
    this.props.logout({token});
  };

  navigateSupport = () => {
    NavigationService.navigate('Support');
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <View style={base.row}>
          <Text style={base.text}>{I18n.t('notification')}</Text>
          <View style={base.flex} />
          <Switch value={this.state.switch} onChange={this.onChangeSwitch} />
        </View>

        {/* <TouchableOpacity onPress={this.onPressEdit}>
          <Text style={base.text}>{I18n.t('edit_profile')}</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={base.text}>{I18n.t('change_user')}</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={base.text}>{I18n.t('account_freeze')}</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={base.text}>{I18n.t('change_language')}</Text>
        </TouchableOpacity> */}

        <TouchableOpacity onPress={this.onPressLogout}>
          <Text style={base.text}>{I18n.t('logout')}</Text>
        </TouchableOpacity>

        <View style={base.line} />

        <TouchableOpacity onPress={this.navigateSupport}>
          <Text style={base.text}>{I18n.t('tech_support')}</Text>
        </TouchableOpacity>
      </Wrap>
    );
  }
}
