import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator} from '../../../../store/actions';
import {
  putAddProductWishlist,
  putAddWishlist,
  putUpdateWishlist,
  wishAdd,
  wishUpdate,
  reducerAddWishlist,
} from '../../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reducerAddWishlist: item => dispatch(reducerAddWishlist(item)),
    putAddProductWishlist: item => dispatch(putAddProductWishlist(item)),
    putAddWishlist: item => dispatch(putAddWishlist(item)),
    putUpdateWishlist: item => dispatch(putUpdateWishlist(item)),
    wishAdd: item => dispatch(wishAdd(item)),
    wishUpdate: item => dispatch(wishUpdate(item)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
