import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
  },
  bg: {
    flex: 1,
    backgroundColor: '#F4F4F6',
  },
});

export default {base};
