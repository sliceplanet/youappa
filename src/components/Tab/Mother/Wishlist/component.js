import React from 'react';
import {View, FlatList, ScrollView} from 'react-native';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import AvatarWishlist from '../../../Base/Avatars/AvatarWishlist';
import Wrap from '../../../Base/Wrap';
import Rubric from '../../../Base/Rubric';
import Wish from '../../../Base/Wish';
import ModalProducts from '../../../Base/Modals/ModalProducts';
import AvatarView from '../Home/AvatarView';

// Helpers
import {WISH, getUser} from '../../../../helpers';

// Api
import {apiPostAddWishlist} from '../../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class WishList extends React.PureComponent {
  constructor(props) {
    super(props);

    const type = props.navigation.getParam('type', 2);
    const child = props.navigation.getParam('child', null);
    const wishlistId = props.navigation.getParam('wishlistId', null);

    this.state = {
      type,
      child,
      wishlistId,
      category: -1,
      isVisible: false,
    };
  }

  onPressProductItem = item => {
    const {token} = this.props.user;
    const {category, type, child, wishlistId} = this.state;

    if (type === 3) {
      this.handleAddProduct(
        item,
        this.props.wishlist.filter(e => e._id === wishlistId)[0],
      );
      return;
    }

    let wishlist;
    switch (parseInt(type, 10)) {
      case 1: {
        wishlist = this.props.wishlist.filter(
          e =>
            e.type === 1 &&
            parseInt(e.category, 10) >= 0 &&
            parseInt(e.category, 10) <= 8,
        );
        break;
      }
      case 2: {
        wishlist = this.props.wishlist.filter(
          e =>
            e.type === 2 &&
            e.child._id === child._id &&
            parseInt(e.category, 10) >= 0 &&
            parseInt(e.category, 10) <= 8,
        );
        break;
      }
      default:
    }

    const wishlistFilter = wishlist.filter(
      e => parseInt(e.category, 10) === category,
    );
    if (wishlistFilter.length > 0) {
      this.handleAddProduct(item, wishlistFilter[0]);
    } else {
      let data = {
        title: WISH().filter(e => e.category === category)[0].title,
        category,
        type,
      };
      if (type === 2) {
        data = {
          ...data,
          child: child._id,
        };
      }
      this.props.setNetworkIndicator(true);
      apiPostAddWishlist({token, data})
        .then(result => {
          this.props.reducerAddWishlist({...result.data.data, child});
          this.handleAddProduct(item, {...result.data.data, child});
          this.setState({
            isVisible: false,
          });
          this.props.setNetworkIndicator(false);
        })
        .catch(e => {
          console.log(e);
          this.props.setNetworkIndicator(false);
        });
    }
  };

  onPressProductClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  onPressRubricItem = category => {
    this.setState({
      isVisible: true,
      category,
    });
  };

  onChangeWishlistLock = wish => {
    const {token} = this.props.user;
    const patch = {
      wishlistId: wish._id,
    };
    const data = {
      privateLevel: wish.privateLevel === 0 ? 1 : 0,
    };

    this.props.putUpdateWishlist({token, patch, data});
  };

  onPressCreateProduct = () => {
    this.setState({
      isVisible: true,
      category: 9,
    });
  };

  handleAddProduct = (item, wl) => {
    const {token} = this.props.user;
    const patch = {
      wishlistId: wl._id,
    };
    const data = {
      title: item.title,
      imageUrl: item.image,
      buyLink: item.link,
      quantity: 1,
      currency: item.currency,
      price: item.price,
      marketType: item.market,
    };

    this.props.putAddProductWishlist({token, patch, data});

    this.setState({
      category: -1,
      isVisible: false,
    });
  };

  renderRubricItem = ({item, index}) => {
    return (
      <Rubric
        key={index}
        icon={item.icon}
        category={item.category}
        onPress={this.onPressRubricItem}
        title={item.title}
      />
    );
  };

  render() {
    const {child, type, wishlistId, isVisible} = this.state;

    switch (type) {
      case 1: {
        const wishlist = this.props.wishlist.filter(
          e =>
            e.type === 1 &&
            parseInt(e.category, 10) >= 0 &&
            parseInt(e.category, 10) <= 8,
        );

        return (
          <View style={base.bg}>
            <Wrap tab type="mother" style={base.wrap}>
              <AvatarView />
              {wishlist
                .filter(e => e.products.length > 0)
                .map(e => {
                  return (
                    <Wish
                      key={e._id}
                      wish={{...e}}
                      onChangeWishlistLock={this.onChangeWishlistLock}
                    />
                  );
                })}
              <FlatList
                data={WISH()}
                horizontal={false}
                numColumns={3}
                renderItem={this.renderRubricItem}
                keyExtractor={(item, index) => index.toString()}
              />

              <ModalProducts
                isVisible={isVisible}
                onPressItem={this.onPressProductItem}
                onPressClose={this.onPressProductClose}
              />
            </Wrap>
          </View>
        );
      }
      case 2: {
        const {name, bornDateMs, usedPhotoSource, photoUrl, photoId} = child;
        const wishlist = this.props.wishlist.filter(
          e =>
            e.type === 2 &&
            e.child._id === child._id &&
            parseInt(e.category, 10) >= 0 &&
            parseInt(e.category, 10) <= 8,
        );

        const now = moment();
        const date = moment.unix(bornDateMs);
        const diffYears = now.diff(date, 'years');
        const diffMonth = now.diff(date, 'month');
        const diffDay = now.diff(date, 'day');

        let bornDate = '';
        if (diffYears > 0) {
          bornDate = `${diffYears} ${I18n.t('year')}`;
        } else if (diffMonth > 0) {
          bornDate = `${diffMonth} ${I18n.t(
            diffMonth > 1 ? 'months' : 'month',
          )}`;
        } else if (diffDay > 0) {
          bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
        } else {
          bornDate = I18n.t('newborn');
        }

        const avatar = {
          usedPhotoSource,
          photoUrl,
          photoId,
        };

        return (
          <View style={base.bg}>
            <Wrap tab type="mother" style={base.wrap}>
              <AvatarWishlist
                name={name}
                bornDate={bornDate}
                avatar={avatar}
                createWishlist={false}
                countWishlist={wishlist.length}
              />
              <ScrollView>
                {wishlist
                  .filter(e => e.products.length > 0)
                  .map(e => {
                    return (
                      <Wish
                        key={e._id}
                        wish={{...e}}
                        onChangeWishlistLock={this.onChangeWishlistLock}
                      />
                    );
                  })}
                <FlatList
                  data={WISH()}
                  horizontal={false}
                  numColumns={3}
                  renderItem={this.renderRubricItem}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>

              <ModalProducts
                isVisible={isVisible}
                onPressItem={this.onPressProductItem}
                onPressClose={this.onPressProductClose}
              />
            </Wrap>
          </View>
        );
      }
      case 3: {
        const wishlist = this.props.wishlist.filter(e => e._id === wishlistId);
        const {name, used_avatar_source, avatar_url, avatar_id} = getUser(
          this.props.user,
        );
        const avatar = {
          usedPhotoSource: used_avatar_source,
          photoUrl: avatar_url,
          photoId: avatar_id,
        };

        return (
          <View style={base.bg}>
            <Wrap tab type="mother" style={base.wrap}>
              <AvatarWishlist
                name={name}
                avatar={avatar}
                createWishlist
                onPressCreateWishlist={this.onPressCreateProduct}
                countWishlist={wishlist.length}
              />

              {wishlist
                .filter(e => e.products.length > 0)
                .map(e => {
                  return (
                    <Wish
                      key={e._id}
                      wish={{...e}}
                      onChangeWishlistLock={this.onChangeWishlistLock}
                    />
                  );
                })}

              <ModalProducts
                isVisible={isVisible}
                onPressItem={this.onPressProductItem}
                onPressClose={this.onPressProductClose}
              />
            </Wrap>
          </View>
        );
      }
      default: {
        return null;
      }
    }
  }
}
