import React from 'react';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import Input from '../../../Base/Input';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';

// Style
import {base} from './styles';

export default class Support extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      about: '',
    };
  }

  onPress = () => {
    const {token} = this.props.user;

    const {about} = this.state;

    const data = {
      title: '',
      text: about,
      attachmentImage: '',
    };
    this.props.postPrivateReport({token, data});
    this.props.navigation.goBack();
  };

  onChangeTextAbout = about => {
    this.setState({about});
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextAbout}
          value={this.state.about}
          placeholder={I18n.t('description')}
          keyboardType="default"
          multiline
        />

        <ButtonGradient
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('send')}
          onPress={this.onPress}
        />
      </Wrap>
    );
  }
}
