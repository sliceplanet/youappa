import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    width: wp(90),
  },
  icon: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(8),
    overflow: 'hidden',
    marginRight: wp(2),
  },
});

export default {base};
