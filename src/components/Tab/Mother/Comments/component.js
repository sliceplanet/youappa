import React from 'react';
import {View, FlatList} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import Input from '../../../Base/Input';
import Image from '../../../Base/Image';
import CommentItem from './CommentItem';

// Helpers
import {getUser} from '../../../../helpers';
import {URI} from '../../../../store/api';

// Style
import {base} from './styles';

export default class Comments extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      comment: '',
    };
  }

  onChangeTextComment = comment => {
    this.setState({comment});
  };

  check = () => {
    const {comment} = this.state;

    if (comment.length > 0) {
      const {token} = this.props.user;
      const postId = this.props.navigation.getParam('postId', null);
      const patch = {
        postId,
      };
      const data = {
        text: comment,
      };
      this.props.postCreateComment({token, patch, data});
      this.setState({
        comment: '',
      });
    }
  };

  renderItem = ({item, index}) => {
    return <CommentItem key={index} {...item} />;
  };

  render() {
    const {avatar_url} = getUser(this.props.user);
    const postId = this.props.navigation.getParam('postId', null);

    let comments = [];
    this.props.comments.forEach(e => {
      if (e.data.length > 0) {
        e.data.forEach(m => {
          if (m.post === postId) {
            comments = [...comments, m];
          }
        });
      } else if (e.data.post === postId) {
        comments = [...comments, e.data];
      }
    });

    return (
      <Wrap>
        <FlatList
          data={comments}
          extraData={this.props.comments}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={base.row}>
          <Image uri={`${URI}${avatar_url}`} style={base.icon} />
          <Input
            onChangeText={this.onChangeTextComment}
            value={this.state.comment}
            placeholder={I18n.t('add_a_comment')}
            returnKeyType="done"
            onSubmitEditing={this.check}
          />
        </View>
      </Wrap>
    );
  }
}
