import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(2),
  },
  text: {
    width: wp(80),
    paddingHorizontal: wp(2),
    fontSize: wp(4),
    fontFamily: 'Roboto-Regular',
    color: 'black',
  },
  bold: {
    fontWeight: 'bold',
  },
  image: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(8),
    overflow: 'hidden',
  },
  time: {
    marginLeft: wp(2),
    fontSize: wp(3),
    width: wp(30),
  },
  count: {
    fontSize: wp(3),
  },
});

export default {base};
