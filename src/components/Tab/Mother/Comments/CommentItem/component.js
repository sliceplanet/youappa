import React from 'react';
import {View, Text} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import Image from '../../../../Base/Image';

// Helpers
import {URI} from '../../../../../store/api';
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class CommentItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressLike = () => {
    const {token, _id, isLike} = this.props;

    const patch = {
      commentId: _id,
    };

    if (isLike) {
      this.props.putUnlikeComment({token, patch});
    } else {
      this.props.putLikeComment({token, patch});
    }
  };

  render() {
    const {text, user, isLike, likesCount, createdAt} = this.props;
    console.log(URI + user.avatar_url);
    return (
      <View style={base.row}>
        <Image uri={URI + user.avatar_url} style={base.image} />
        <View>
          <Text style={base.text}>
            <Text style={base.bold}>{user.name}</Text> {text}
          </Text>
          <View style={base.row}>
            <Text style={base.time}>{moment(createdAt).fromNow()}</Text>
            <Text style={base.count}>
              {I18n.t('like')}: {likesCount}
            </Text>
          </View>
        </View>
        <ScalableImage
          source={isLike ? Images.liking : Images.like}
          width={wp(4)}
          onPress={this.onPressLike}
        />
      </View>
    );
  }
}
