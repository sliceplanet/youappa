import {connect} from 'react-redux';
import component from './component';

import {
  putLikeComment,
  putUnlikeComment,
} from '../../../../../store/actions/like';

function mapStateToProps(state) {
  return {
    token: state.user.token,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putLikeComment: data => dispatch(putLikeComment(data)),
    putUnlikeComment: data => dispatch(putUnlikeComment(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
