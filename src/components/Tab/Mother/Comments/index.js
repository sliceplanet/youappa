import {connect} from 'react-redux';
import component from './component';

import {postCreateComment} from '../../../../store/actions/comment';

function mapStateToProps(state) {
  return {
    user: state.user,
    comments: state.comments,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postCreateComment: data => dispatch(postCreateComment(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
