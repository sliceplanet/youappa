import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
    paddingVertical: wp(2),
  },
  wrapItem: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
  newText: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    color: '#949494',
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
  },
  imageDouble: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(12),
    overflow: 'hidden',
  },
  bold: {
    fontWeight: '700',
  },
  itemText: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'black',
  },
  textWrap: {
    width: wp(60),
    paddingHorizontal: wp(2),
  },
  textWrapDouble: {
    width: wp(60),
    paddingRight: wp(2),
    paddingLeft: wp(6),
  },
  position: {
    position: 'absolute',
    bottom: -wp(4),
    right: -wp(4),
    borderColor: 'white',
    borderWidth: 4,
  },
  yellowWrap: {
    backgroundColor: '#FDE0BE88',
  },
});

export default {base};
