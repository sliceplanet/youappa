import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../Base/Wrap';
import FriendRequest from './FriendRequest';

// API
import {apiGetIncomingFriend} from '../../../../store/api/friend';

// Style
import {base} from './styles';

export default class Events extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const {token} = this.props.user;
    apiGetIncomingFriend({token})
      .then(result => {
        const data = result.data.data.map(el => {
          return {t: 0, ...el};
        });

        this.setState({
          data,
        });
      })
      .catch(e => console.log(e));
  }

  onAccept = _id => {
    this.setState({
      data: this.state.data.filter(e => e._id !== _id),
    });
  };

  onDecline = _id => {
    this.setState({
      data: this.state.data.filter(e => e._id !== _id),
    });
  };

  renderItem = ({item, index}) => {
    const {t} = item;

    switch (t) {
      case 0: {
        return (
          <FriendRequest
            onAccept={this.onAccept}
            onDecline={this.onDecline}
            style={base.wrapItem}
            key={index}
            {...item}
          />
        );
      }
      default: {
        return (
          <View style={base.wrapItem} key={index}>
            {/* <Text style={base.newText}>{action}</Text> */}
          </View>
        );
      }
    }
  };

  render() {
    const {data} = this.state;
    return (
      <Wrap tab type="mother" style={base.wrap}>
        <FlatList
          data={data}
          extraData={data}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </Wrap>
    );
  }
}
