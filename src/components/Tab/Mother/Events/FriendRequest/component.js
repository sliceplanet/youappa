import React from 'react';
import {View, Image as Img, Text} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import ButtonEvent from '../../../../Base/Buttons/ButtonEvent';
import Image from '../../../../Base/Image';

// Helpers
import * as Images from '../../../../../helpers/images';
import {URI} from '../../../../../store/api';

// API
import {
  apiGetAcceptFriend,
  apiGetRemoveFriend,
} from '../../../../../store/api/friend';

// Style
import {base} from './styles';

export default class FriendRequest extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      loading: false,
    };
  }

  componentDidMount() {}

  onPressAccept = () => {
    const {_id} = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        const {token} = this.props.user;
        const patch = {
          userId: _id,
        };
        apiGetAcceptFriend({token, patch})
          .then(() => {
            this.setState({
              loading: false,
            });
            this.props.onAccept(_id);
          })
          .catch(e => {
            console.log(e);
            this.setState({
              loading: false,
            });
          });
      },
    );
  };

  onPressDecline = () => {
    const {_id} = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        const {token} = this.props.user;
        const patch = {
          userId: _id,
        };
        apiGetRemoveFriend({token, patch})
          .then(() => {
            this.setState({
              loading: false,
            });
            this.props.onDecline(_id);
          })
          .catch(e => {
            console.log(e);
            this.setState({
              loading: false,
            });
          });
      },
    );
  };

  render() {
    const {loading} = this.state;
    const {name, avatar_url} = this.props;

    if (loading) {
      return null;
    }
    return (
      <View style={base.wrapItem}>
        {avatar_url ? (
          <Image
            uri={`${URI}${avatar_url}`}
            style={base.image}
            containerStyle={base.image}
          />
        ) : (
          <Img source={Images.profile} style={base.image} />
        )}

        <View style={base.textWrap}>
          <Text style={base.itemText}>
            <Text style={base.bold}>{name}</Text> подписался(-ась) на ваши
            обновления
          </Text>
          <View style={base.row}>
            <ButtonEvent accept onPress={this.onPressAccept} />
            <ButtonEvent onPress={this.onPressDecline} />
          </View>
        </View>
        <View style={base.flex} />

        <ScalableImage source={Images.addUser} width={wp(16)} />
      </View>
    );
  }
}
