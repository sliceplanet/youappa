import {connect} from 'react-redux';
import component from './component';

import {setToast, setNetworkIndicator} from '../../../../store/actions';
import {postCreatePost} from '../../../../store/actions/post';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
    postCreatePost: data => dispatch(postCreatePost(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
