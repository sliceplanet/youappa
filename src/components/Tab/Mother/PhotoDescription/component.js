import React from 'react';
import {View} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import Wrap from '../../../Base/Wrap';
import Input from '../../../Base/Input';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Api
import {apiPostUploadPhoto} from '../../../../store/api/photo';

// Style
import {base} from './styles';

export default class PhotoDescription extends React.PureComponent {
  constructor(props) {
    super(props);

    const image = props.navigation.getParam('image', '');
    this.state = {
      image,
      button: false,
      about: '',
    };
  }

  onPress = () => {
    this.setState({
      button: !this.state.button,
    });
  };

  onChangeTextAbout = about => {
    this.setState({about});
  };

  check = () => {
    const {image, button, about} = this.state;
    // if (about.length === 0) {
    //   this.props.showToast('add_a_description');
    //   return;
    // }

    const {token} = this.props.user;
    this.props.setNetworkIndicator(true);
    apiPostUploadPhoto({
      token,
      data: {
        label: about,
        geoLat: 0,
        geoLon: 0,
        image,
      },
    })
      .then(result => {
        const data = {
          title: about,
          text: about,
          privateLevel: button ? 0 : 2,
          photos: [result.data.data._id],
        };
        this.props.setNetworkIndicator(false);
        this.props.postCreatePost({token, data});
        NavigationService.reset('Home');
      })
      .catch(error => {
        const {message} = error.response.data;
        console.log('apiPostUploadPhoto -> ', error.response);
        this.props.showToast(message);
        this.props.setNetworkIndicator(false);
      });
  };

  render() {
    const {button} = this.state;
    return (
      <Wrap style={base.wrap}>
        <Input
          style={base.margin}
          onChangeText={this.onChangeTextAbout}
          value={this.state.about}
          placeholder={I18n.t('description')}
          keyboardType="default"
          multiline
        />

        <View style={base.row}>
          <ScalableImage
            source={Images.btnLock}
            style={button ? base.true : base.false}
            height={wp(12)}
            onPress={this.onPress}
          />
          <ButtonGradient
            onPress={this.check}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="white"
            width={wp(40)}
            title={I18n.t('publish')}
          />
        </View>
      </Wrap>
    );
  }
}
