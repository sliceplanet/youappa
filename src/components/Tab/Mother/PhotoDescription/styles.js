import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    width: wp(100),
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
    paddingTop: wp(2),
    paddingBottom: wp(14),
  },
  margin: {
    marginLeft: wp(2),
    backgroundColor: 'white',
  },
  true: {
    tintColor: '#DD7B27',
    marginRight: wp(4),
  },
  false: {
    tintColor: '#C4C4C4',
    marginRight: wp(4),
  },
});

export default {base};
