import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: wp(4),
  },
  text: {
    width: wp(33),
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#A6A6A6',
    paddingLeft: wp(1),
    textAlign: 'center',
  },
});

export default {base};
