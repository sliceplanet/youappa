import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {getFeedEvent} from '../../../../store/actions/event';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getFeedEvent: item => dispatch(getFeedEvent(item)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
