import React from 'react';
import {View, Text} from 'react-native';
import {WebView} from 'react-native-webview';
import I18n from 'i18n-js';

// Components
import ButtonEvent from '../../../Base/Buttons/ButtonEvent';

// Style
import {base} from './styles';

export default class Web extends React.PureComponent {
  constructor(props) {
    super(props);

    const product = props.navigation.getParam('product', null);
    const setProduct = props.navigation.getParam('setProduct', null);
    this.state = {
      product,
      setProduct,
    };
  }

  onPressAccept = () => {
    this.web.injectJavaScript(
      'window.ReactNativeWebView.postMessage(window.location.href)',
    );
  };

  onPressDecline = () => {
    this.props.navigation.goBack();
  };

  onMessage = event => {
    this.state.setProduct({
      ...this.state.product,
      buyLink: event.nativeEvent.data,
    });
    this.props.navigation.goBack();
  };

  render() {
    const {product} = this.state;
    return (
      <View style={base.flex}>
        <View style={base.row}>
          <ButtonEvent accept onPress={this.onPressAccept} />
          <View style={base.flex} />
          <Text style={base.text}>{I18n.t('confirm_selected_product')}</Text>
          <View style={base.flex} />
          <ButtonEvent onPress={this.onPressDecline} />
        </View>

        <WebView
          ref={c => {
            this.web = c;
          }}
          javaScriptEnabled
          source={{uri: product.buyLink}}
          onMessage={this.onMessage}
        />
      </View>
    );
  }
}
