import React from 'react';
import {FlatList} from 'react-native';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarWishlist from '../../../Base/Avatars/AvatarWishlist';
import Rubric from '../../../Base/Rubric';
import Wish from '../../../Base/Wish';
import ModalProducts from '../../../Base/Modals/ModalProducts';
import ModalCreateWishlist from '../../../Base/Modals/ModalCreateWishlist';
import ModalEditeWishlist from '../../../Base/Modals/ModalEditeWishlist';

// Helpoers
import {WISH} from '../../../../helpers';

// API
import {apiPostAddWishlist} from '../../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class WishList extends React.PureComponent {
  constructor(props) {
    super(props);

    const item = props.navigation.getParam('item', null);

    this.state = {
      ...item,
      isVisible: false,
      isVisibleCreateWishlist: false,
      isVisibleWishlist: false,
      type: '',
    };
  }

  componentDidMount() {
    const {wish} = this.props;
    if (wish.length === 0) {
      this.props.wishAdd(WISH());
    }
  }

  onPressRubric = type => {
    this.setState({
      isVisible: true,
      type,
    });
  };

  onLongPressRubric = modalType => {
    this.setState({
      modalType,
      isVisibleWishlist: true,
    });
  };

  onPressItem = item => {
    const {token} = this.props.user;
    const {wishlist} = this.state;

    const patch = {
      wishlistId: wishlist._id,
    };
    const data = {
      title: item.title,
      category: this.state.type,
      imageUrl: item.image,
      buyLink: item.link,
      quantity: 1,
      currency: item.currency,
      price: item.price,
      marketType: item.market,
    };

    this.props.putAddProductWishlist({token, patch, data});
    this.setState({
      wishlist: {
        ...this.state.wishlist,
        products: [
          ...this.state.wishlist.products,
          {...item, category: this.state.type},
        ],
      },
      type: '',
      isVisible: false,
    });
  };

  onPressCreateWishlist = () => {
    this.setState({
      isVisibleCreateWishlist: true,
    });
  };

  onPressCloseWishlist = () => {
    this.setState({
      isVisibleWishlist: false,
    });
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  onPressAcceptWishlist = title => {
    this.onPressCloseWishlist();
    if (title.length > 0) {
      const {modalType} = this.state;
      this.props.wishUpdate({title, modalType});
    }
  };

  onPressAccept = item => {
    const {token} = this.props.user;
    const data = {
      title: item.name,
      description: item.description,
      type: 2,
      child: this.state.child._id,
    };
    this.props.setNetworkIndicator(true);
    apiPostAddWishlist({token, data})
      .then(result => {
        const wishlist = {
          ...result.data.data,
          child: this.state.child,
        };
        this.props.putAddWishlist(wishlist);
        this.setState({
          isVisibleCreateWishlist: false,
          isCreate: false,
          wishlist,
        });
        this.props.setNetworkIndicator(false);
      })
      .catch(e => {
        console.log(e);
        this.props.setNetworkIndicator(false);
      });
  };

  onPressCloseCreateWishlist = () => {
    this.setState({
      isVisibleCreateWishlist: false,
    });
  };

  renderGridItem = ({item, index}) => {
    return (
      <Rubric
        key={index}
        icon={item.icon}
        type={item.type}
        onPress={this.onPressRubric}
        onLongPress={this.onLongPressRubric}
        title={item.title}
      />
    );
  };

  render() {
    const {
      wishlist,
      child,
      isCreate,
      isVisible,
      isVisibleCreateWishlist,
      isVisibleWishlist,
    } = this.state;
    const {wish} = this.props;

    if (wishlist) {
      if (wishlist.type !== 2) {
        return (
          <Wrap tab type="mother" style={base.wrap}>
            <Wish wishlistId={wishlist._id} wishlist={wishlist.products} />

            <FlatList
              data={wish}
              horizontal={false}
              numColumns={3}
              renderItem={this.renderGridItem}
              keyExtractor={(item, index) => index.toString()}
            />

            <ModalProducts
              isVisible={isVisible}
              onPressItem={this.onPressItem}
              onPressClose={this.onPressClose}
            />

            <ModalEditeWishlist
              isVisible={isVisibleWishlist}
              onPressClose={this.onPressCloseWishlist}
              onPressAccept={this.onPressAcceptWishlist}
            />
          </Wrap>
        );
      }
      const {
        name,
        bornDateMs,
        usedPhotoSource,
        photoUrl,
        photoId,
      } = wishlist.child;

      const now = moment();
      const date = moment.unix(bornDateMs);
      const diffYears = now.diff(date, 'years');
      const diffMonth = now.diff(date, 'month');
      const diffDay = now.diff(date, 'day');

      let bornDate = '';
      if (diffYears > 0) {
        bornDate = `${diffYears} ${I18n.t('year')}`;
      } else if (diffMonth > 0) {
        bornDate = `${diffMonth} ${I18n.t(diffMonth > 1 ? 'months' : 'month')}`;
      } else if (diffDay > 0) {
        bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
      } else {
        bornDate = I18n.t('newborn');
      }

      const avatar = {
        usedPhotoSource,
        photoUrl,
        photoId,
      };

      return (
        <Wrap tab type="mother" style={base.wrap}>
          <AvatarWishlist
            name={name}
            bornDate={bornDate}
            avatar={avatar}
            createWishlist={false}
            countWishlist={wishlist.products.length}
          />

          <Wish wishlistId={wishlist._id} wishlist={wishlist.products} />

          <FlatList
            data={wish}
            horizontal={false}
            numColumns={3}
            renderItem={this.renderGridItem}
            keyExtractor={(item, index) => index.toString()}
          />

          <ModalProducts
            isVisible={isVisible}
            onPressItem={this.onPressItem}
            onPressClose={this.onPressClose}
          />

          <ModalEditeWishlist
            isVisible={isVisibleWishlist}
            onPressClose={this.onPressCloseWishlist}
            onPressAccept={this.onPressAcceptWishlist}
          />
        </Wrap>
      );
    }
    const {name, bornDateMs, usedPhotoSource, photoUrl, photoId} = child;

    const now = moment();
    const date = moment.unix(bornDateMs);
    const diffYears = now.diff(date, 'years');
    const diffMonth = now.diff(date, 'month');
    const diffDay = now.diff(date, 'day');

    let bornDate = '';
    if (diffYears > 0) {
      bornDate = `${diffYears} ${I18n.t('year')}`;
    } else if (diffMonth > 0) {
      bornDate = `${diffMonth} ${I18n.t(diffMonth > 1 ? 'months' : 'month')}`;
    } else if (diffDay > 0) {
      bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
    } else {
      bornDate = I18n.t('newborn');
    }

    const avatar = {
      usedPhotoSource,
      photoUrl,
      photoId,
    };

    return (
      <Wrap tab type="mother" style={base.wrap}>
        <AvatarWishlist
          name={name}
          bornDate={bornDate}
          avatar={avatar}
          createWishlist={isCreate}
          countWishlist={0}
          onPressCreateWishlist={this.onPressCreateWishlist}
        />
        <ModalCreateWishlist
          isVisible={isVisibleCreateWishlist}
          onPressAccept={this.onPressAccept}
          onPressClose={this.onPressCloseCreateWishlist}
        />
      </Wrap>
    );
  }
}
