import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator} from '../../../../store/actions';
import {
  putAddProductWishlist,
  putAddWishlist,
  wishAdd,
  wishUpdate,
} from '../../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
    wish: state.wish,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putAddProductWishlist: item => dispatch(putAddProductWishlist(item)),
    putAddWishlist: item => dispatch(putAddWishlist(item)),
    wishAdd: item => dispatch(wishAdd(item)),
    wishUpdate: item => dispatch(wishUpdate(item)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
