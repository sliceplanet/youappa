import React from 'react';
import {View, Text, Image as Img, TouchableOpacity} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Image from '../../../../Base/Image';

// Helpers
import {URI} from '../../../../../store/api';
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class FriendItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => {
    if (this.props.onPress) {
      const {
        avatar_id,
        avatar_url,
        used_avatar_source,
        _id,
        name,
        type,
      } = this.props;
      this.props.onPress({
        avatar_id,
        avatar_url,
        used_avatar_source,
        _id,
        name,
        type,
      });
    }
  };

  onPressRemove = () => {
    if (this.props.onPressRemove) {
      const {
        avatar_id,
        avatar_url,
        used_avatar_source,
        _id,
        name,
        type,
      } = this.props;
      this.props.onPressRemove({
        avatar_id,
        avatar_url,
        used_avatar_source,
        _id,
        name,
        type,
      });
    }
  };

  render() {
    const {name, avatar_url, onPressRemove, width} = this.props;

    return (
      <TouchableOpacity style={[base.row, {width}]} onPress={this.onPress}>
        {avatar_url ? (
          <Image
            uri={`${URI}${avatar_url}`}
            style={base.image}
            containerStyle={base.image}
          />
        ) : (
          <Img source={Images.profile} style={base.image} />
        )}

        <View>
          <Text style={base.name}>{name}</Text>
        </View>
        <View style={base.flex} />
        {onPressRemove && (
          <ScalableImage
            onPress={this.onPressRemove}
            source={Images.close}
            width={wp(6)}
          />
        )}
      </TouchableOpacity>
    );
  }
}
