import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    // width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: wp(2),
    paddingVertical: wp(2),
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 0.5,
  },
  icon: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(12),
    marginRight: wp(4),
  },
  name: {
    color: '#3F3F3F',
    fontSize: wp(5),
    fontFamily: 'Roboto-Regular',
  },
  city: {
    color: '#8A8A8A',
    fontSize: wp(4),
    fontFamily: 'Roboto-Regular',
    paddingLeft: wp(1),
  },
  cityRow: {
    flexDirection: 'row',
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
    margin: wp(2),
    marginRight: wp(4),
  },
});

export default {base};
