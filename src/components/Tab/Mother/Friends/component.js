import React from 'react';
import {FlatList} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Wrap from '../../../Base/Wrap';
import FriendItem from './FriendItem';

// Style
import {base} from './styles';

export default class Friends extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderItem = ({item, index}) => {
    return <FriendItem key={index} {...item} width={wp(92)} />;
  };

  render() {
    const {friends} = this.props;
    return (
      <Wrap style={base.wrap}>
        <FlatList
          data={friends}
          extraData={this.props.friends}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </Wrap>
    );
  }
}
