import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: wp(2),
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
    marginRight: wp(4),
  },
  location: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#A6A6A6',
    paddingLeft: wp(1),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'black',
    paddingVertical: wp(1),
  },
  wrapItem: {
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
});

export default {base};
