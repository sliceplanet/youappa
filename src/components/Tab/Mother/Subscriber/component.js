import React from 'react';
import {View, FlatList, Image, Text} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Wrap from '../../../Base/Wrap';
import SubscriberGroup from '../../../Base/SubscriberGroup';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

const TEST = [1, 2, 3, 4, 5, 6, 7, 8, 9];

export default class Subscriber extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderItem = ({index}) => {
    return (
      <View style={[base.row, base.wrapItem]} key={index}>
        <Image
          style={base.image}
          source={{uri: 'https://picsum.photos/id/436/200/200'}}
        />
        <View>
          <Text style={base.text}>Екатерина Мохова</Text>
          <View style={base.row}>
            <ScalableImage source={Images.location} height={wp(4)} />
            <Text style={base.location}>г. Новопроквск</Text>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <Wrap tab type="mother" style={base.wrap}>
        <SubscriberGroup />
        <FlatList
          data={TEST}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </Wrap>
    );
  }
}
