import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  calendarWrap: {
    borderTopColor: '#eeeeeeee',
    borderTopWidth: 0.7,
    height: hp(50),
  },
  icon: {
    margin: wp(4),
  },
  image: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(8),
    overflow: 'hidden',
  },
  text: {
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(4),
    color: '#3F3F3F',
  },
  itemWrap: {
    width: wp(100),
    paddingHorizontal: wp(4),
    paddingBottom: wp(2),
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  itemTime: {
    fontFamily: 'SFUIDisplay-Medium',
    fontWeight: '700',
    fontSize: wp(5),
    color: '#3F3F3F',
  },
  itemEvent: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    color: '#3F3F3F',
    marginVertical: wp(2),
  },
  itemInvite: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#3F3F3F',
  },
  width: {
    width: wp(40),
  },
  position: {
    position: 'absolute',
    top: -wp(4),
    right: wp(2),
  },
  bgc: {
    width: wp(100),
    height: wp(6),
    backgroundColor: 'white',
  },
});

export default {base};
