import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {getMyEvent} from '../../../../../store/actions/event';

function mapStateToProps(state) {
  return {
    user: state.user,
    friends: state.friends,
    events: state.events,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getMyEvent: item => dispatch(getMyEvent(item)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
