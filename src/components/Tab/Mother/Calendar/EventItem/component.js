import React from 'react';
import {View, Text} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import ButtonWhite from '../../../../Base/Buttons/ButtonWhite';

// Style
import {base} from './styles';

export default class EventItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  inviteFriend = () => {
    this.props.onPressInvite(this.props);
  };

  render() {
    const {date, title} = this.props;
    return (
      <View style={base.itemWrap}>
        <View style={base.width}>
          <Text style={base.itemTime}>{moment(date).format('h:mm a')}</Text>
          <Text style={base.itemEvent}>{title}</Text>
        </View>
        <View style={base.flex} />
        <ButtonWhite
          onPress={this.inviteFriend}
          width={wp(30)}
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="#0C8A7E"
          title={I18n.t('to_invite')}
        />
      </View>
    );
  }
}
