import {connect} from 'react-redux';
import component from './component';

import {getMyEvent} from '../../../../store/actions/event';
import {setToast, setNetworkIndicator} from '../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
    friends: state.friends,
    events: state.events,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getMyEvent: item => dispatch(getMyEvent(item)),
    showToast: data => dispatch(setToast(data)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
