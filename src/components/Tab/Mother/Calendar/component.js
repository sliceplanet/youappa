import React from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import I18n from 'i18n-js';
import {CalendarList, LocaleConfig} from 'react-native-calendars';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import ScalableImage from 'react-native-scalable-image';
import moment from 'moment';
import EventItem from './EventItem';

// Components
import EventDateView from '../../../Base/EventDateView';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ModalFriends from '../../../Base/Modals/ModalFriends';

// Helpers
import * as Images from '../../../../helpers/images';
import {locales} from '../../../../helpers';
import NavigationService from '../../../../helpers/navigation';

// Api
import {apiGetEvent, apiPutEditEvent} from '../../../../store/api/event';

// Style
import {base} from './styles';

export default class Calendar extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      current: moment().format('YYYY-MM-DD'),
      markedDates: this.getMarkedDates(),
      eventsDate: [],
      selectedDate: -1,
      isVisible: false,
      invite: null,
    };
  }

  componentDidMount() {
    LocaleConfig.locales = locales;
    LocaleConfig.defaultLocale = 'ru';

    const {token} = this.props.user;
    const {current} = this.state;
    const fromDate =
      moment(current)
        .startOf('month')
        .unix() * 1000;
    const toDate =
      moment(current)
        .endOf('month')
        .unix() * 1000;
    const patch = {
      fromDate,
      toDate,
    };

    this.props.getMyEvent({token, patch});
  }

  componentDidUpdate(previousProps) {
    if (previousProps.events !== this.props.events) {
      this.eventsUpdate();
    }
  }

  eventsUpdate = () => {
    const unique = (value, index, self) => {
      return self.indexOf(value) === index;
    };
    const eventsDate = this.props.events
      .map(e => moment(e.date).date())
      .filter(unique)
      .sort((a, b) => parseInt(a, 10) - parseInt(b, 10));
    const selectedDate = eventsDate.length > 0 ? eventsDate[0] : -1;
    this.setState({
      eventsDate,
      selectedDate,
    });
  };

  onDayPress = day => {
    this.setState({current: day.dateString});
  };

  onVisibleMonthsChange = month => {
    const current = month[0].dateString;
    const markedDates = this.getMarkedDates(moment(current));
    this.setState({markedDates, current});
  };

  onPressEvent = () => {
    NavigationService.navigate('AddEvent');
  };

  onEventDatePress = selectedDate => {
    this.setState({
      selectedDate,
    });
  };

  onPressFriendClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  onPressFriendItem = item => {
    this.setState({
      isVisible: false,
    });

    const {invite} = this.state;
    let invites = [];
    if (invite.invites) {
      const filter = invite.invites.map(e => e._id).filter(e => e !== item._id);
      invites = [...filter, item._id];
    } else {
      invites = [item._id];
    }

    const {token} = this.props.user;
    const patch = {
      eventId: invite._id,
    };
    const data = {
      invites,
    };
    this.props.setNetworkIndicator(true);
    apiPutEditEvent({token, data, patch})
      .then(result => {
        this.props.setNetworkIndicator(false);
        console.log(result);
      })
      .catch(e => {
        this.props.setNetworkIndicator(false);
        console.log(e);
      });
  };

  navigateToInvite = invite => {
    if (this.props.friends.length === 0) {
      this.props.showToast(I18n.t('add_friends'));
    }

    const {token} = this.props.user;
    const patch = {
      eventId: invite._id,
    };

    apiGetEvent({token, patch})
      .then(result => {
        this.setState({
          invite: result.data.data,
          isVisible: true,
        });
      })
      .catch(e => console.log(e));
  };

  getMarkedDates = (dates = moment()) => {
    let now = dates;
    let day = now.date();
    const lastDay = now.daysInMonth();
    const markedDates = {};

    if (day > 1) {
      let date = moment(now.format('YYYY-MM-01'));
      for (let i = 1; i < day; i++) {
        markedDates[date.format('YYYY-MM-DD')] = {
          customStyles: {
            text: {
              color: '#A8A095',
            },
          },
        };
        date = date.add(1, 'day');
      }
    }
    if (day !== lastDay) {
      now = dates;
      let date = now.add(1, 'day');
      day = date.date();
      for (let i = day; i <= lastDay; i++) {
        markedDates[date.format('YYYY-MM-DD')] = {
          customStyles: {
            text: {
              color: '#545353',
            },
          },
          dotColor: '#F15837',
          marked: true,
        };
        date = date.add(1, 'day');
      }
    }
    return markedDates;
  };

  renderEventItem = ({index, item}) => {
    return (
      <EventItem key={index} {...item} onPressInvite={this.navigateToInvite} />
    );
  };

  render() {
    const {isVisible} = this.state;
    const markedDates = {
      ...this.state.markedDates,
      [this.state.current]: {
        customStyles: {
          container: {
            alignItems: 'center',
            width: 32,
            height: 32,
            borderWidth: 0.7,
            borderColor: '#D67C2E',
          },
          text: {
            color: '#D67C2E',
          },
        },
        selected: true,
        disableTouchEvent: true,
      },
    };

    const events =
      this.state.selectedDate !== -1
        ? this.props.events.filter(
            e => moment(e.date).date() === this.state.selectedDate,
          )
        : [];

    return (
      <View style={base.wrap}>
        <View style={base.row}>
          <ScalableImage source={Images.wave} style={base.icon} width={wp(6)} />
          <Text style={base.text}>{I18n.t('calendar')}</Text>
          <View style={base.flex} />
          <Image style={base.image} />
        </View>
        <View style={base.calendarWrap}>
          <LinearGradient
            colors={['#FFF1DB', '#FDE0BE']}
            start={{x: 0.0, y: 0.0}}
            end={{x: 1.0, y: 1.0}}>
            <View style={{height: hp(45)}}>
              <CalendarList
                horizontal
                pagingEnabled
                calendarWidth={wp(100)}
                monthFormat="MMMM"
                minDate="2000-01-01"
                maxDate="2100-01-01"
                theme={{
                  calendarBackground: 'transparent',
                  selectedDayBackgroundColor: 'transparent',
                  monthTextColor: '#535353',
                  textMonthFontFamily: 'Roboto-Medium',
                  textMonthFontWeight: '600',
                  textSectionTitleColor: '#545353',
                  textDayHeaderFontFamily: 'Roboto-Regular',
                  todayTextColor: '#D67C2E',
                }}
                markingType="custom"
                markedDates={markedDates}
                onDayPress={this.onDayPress}
                // onVisibleMonthsChange={this.onVisibleMonthsChange}
              />
            </View>
            <View style={base.flex} />
            <View>
              <ScalableImage source={Images.wave} width={wp(100)} />
              <View style={base.bgc} />
              <ButtonGradient
                onPress={this.onPressEvent}
                style={base.position}
                gradientColors={['#DD7B27', '#F79C41']}
                titleColor="white"
                title={`+  ${I18n.t('create')}`}
                width={wp(40)}
              />
            </View>
          </LinearGradient>
        </View>
        <View>
          <EventDateView
            days={this.state.eventsDate}
            selectedDate={this.state.selectedDate}
            onPress={this.onEventDatePress}
          />
        </View>
        <FlatList
          data={events}
          renderItem={this.renderEventItem}
          keyExtractor={(item, index) => index.toString()}
        />
        <ModalFriends
          isVisible={isVisible}
          onPressItem={this.onPressFriendItem}
          onPressClose={this.onPressFriendClose}
        />
      </View>
    );
  }
}
