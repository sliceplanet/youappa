import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';

function mapDispatchToProps(dispatch) {
  return {
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(null, mapDispatchToProps)(component);
