import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    width: wp(100),
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
    paddingTop: wp(2),
    paddingBottom: wp(14),
  },
  image: {
    padding: wp(4),
  },
  padding: {
    margin: wp(1),
  },
  scroll: {
    height: wp(20),
    overflow: 'hidden',
  },
  height: {
    height: wp(20),
  },
  margin: {
    marginLeft: wp(2),
  },
  bottomWrap: {
    width: wp(50),
    alignItems: 'center',
    paddingHorizontal: wp(4),
  },
  imageSpace: {
    width: wp(92),
    height: wp(70),
  },
});

export default {base};
