/* eslint-disable import/no-unresolved */
import React from 'react';
import {View} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ImageResizer from 'react-native-image-resizer';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonWhite from '../../../Base/Buttons/ButtonWhite';
import Wrap from '../../../Base/Wrap';
import Filter from '../../../Base/Filter';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class PhotoFilter extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      image: '',
      resize: '',
      base64: '',
    };
  }

  openCamera = () => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        ImageResizer.createResizedImage(response.uri, 640, 480, 'JPEG', 80)
          .then(resize => {
            this.setState({
              image: `data:${response.type};base64,${response.data}`,
              resize: resize.uri,
              base64: '',
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  };

  openGallery = () => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        ImageResizer.createResizedImage(response.uri, 640, 480, 'JPEG', 80)
          .then(resize => {
            this.setState({
              image: `data:${response.type};base64,${response.data}`,
              resize: resize.uri,
              base64: '',
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  };

  onChange = base64 => {
    this.setState({base64});
  };

  navigatePhotoFilter = () => {
    const {image, base64} = this.state;
    if (image.length === 0) {
      this.props.showToast(I18n.t('upload_your_photo'));
      return;
    }

    if (base64.length === 0) {
      NavigationService.navigate('PhotoDescription', {image});
    } else {
      NavigationService.navigate('PhotoDescription', {image: base64});
    }
  };

  render() {
    const {image, resize} = this.state;
    return (
      <Wrap style={base.wrap}>
        <Filter image={image} resize={resize} onChange={this.onChange} />

        <View style={base.flex} />
        <View style={base.bottomWrap}>
          <View style={base.row}>
            <ScalableImage
              source={Images.camera}
              width={wp(8)}
              onPress={this.openCamera}
            />
            <View style={base.flex} />
            <ButtonWhite
              onPress={this.openGallery}
              width={wp(30)}
              style={base.margin}
              gradientColors={['#DD7B27', '#DD7B27']}
              titleColor="#DD7B27"
              title="Галерея"
            />
          </View>
          <ButtonGradient
            onPress={this.navigatePhotoFilter}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="white"
            title={I18n.t('next')}
          />
        </View>
      </Wrap>
    );
  }
}
