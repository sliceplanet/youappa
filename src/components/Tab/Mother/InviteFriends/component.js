import React from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Image from 'react-native-scalable-image';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonIconText from '../../../Base/Buttons/ButtonIconText';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ModalFriends from '../../../Base/Modals/ModalFriends';
import FriendItem from '../Friends/FriendItem';

// Helpers
import * as Images from '../../../../helpers/images';
import NavigationService from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class InviteFriends extends React.PureComponent {
  constructor(props) {
    super(props);

    const item = props.navigation.getParam('item', null);

    this.state = {
      photos: [],
      friends: [],
      isVisible: false,
      ...item,
    };
  }

  componentDidUpdate(previousProps) {
    if (previousProps.events !== this.props.events) {
      NavigationService.navigate('Calendar');
    }
  }

  onPressCamera = () => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          photos: [
            ...this.state.photos,
            `data:${response.type};base64,${response.data}`,
          ],
        });
      }
    });
  };

  onPressCreateEvent = () => {
    const {title, checker, dateTime, about, photos, friends, wl} = this.state;
    if (photos.length === 0) {
      this.props.showToast(I18n.t('show_photo'));
      return;
    }

    const {token} = this.props.user;
    const data = {
      title,
      text: about,
      type: checker,
      date: moment(dateTime).format(),
      photos,
      invites: friends.map(e => e._id),
      wishlists: wl.map(e => e._id),
    };

    this.props.postCreateEvent({token, data});
  };

  onPressItem = f => {
    if (this.state.friends.filter(e => e._id === f._id).length === 0) {
      this.setState({
        friends: [...this.state.friends, f],
        isVisible: false,
      });
    } else {
      this.setState({
        isVisible: false,
      });
    }
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  onPressRemove = f => {
    this.setState({
      friends: this.state.friends.filter(e => e._id !== f._id),
    });
  };

  navigateFriends = () => {
    if (this.props.friends.length > 0) {
      this.setState({
        isVisible: true,
      });
    } else {
      this.props.showToast(I18n.t('add_friends'));
    }
  };

  keyExtractor = (item, index) => index.toString();

  removeItem = item => {
    this.setState({
      photos: this.state.photos.filter(e => e !== item),
    });
  };

  renderItem = ({index, item}) => {
    return (
      <View style={base.photoWrap} key={index}>
        <Image style={base.round} height={wp(20)} source={{uri: item}} />
        <TouchableOpacity
          style={base.position}
          onPress={() => this.removeItem(item)}>
          <Image source={Images.removeGroup} width={wp(8)} />
        </TouchableOpacity>
      </View>
    );
  };

  renderFriendItem = ({index, item}) => {
    return (
      <FriendItem
        {...item}
        key={index}
        width={wp(92)}
        onPressRemove={this.onPressRemove}
      />
    );
  };

  render() {
    const {photos, friends, isVisible} = this.state;

    return (
      <Wrap>
        <View style={base.row}>
          <Image source={Images.group} width={wp(6)} />
          <Text style={base.invite_friends}>{I18n.t('invite_friends')}</Text>
          <View style={base.flex} />
          <TouchableOpacity onPress={this.navigateFriends}>
            <Text style={base.btn}>{I18n.t('to_invite')}</Text>
          </TouchableOpacity>
        </View>

        <View style={{paddingHorizontal: wp(4), maxHeight: hp(100) - wp(100)}}>
          <FlatList
            data={friends}
            extraData={friends}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderFriendItem}
          />
        </View>

        <ModalFriends
          isVisible={isVisible}
          onPressItem={this.onPressItem}
          onPressClose={this.onPressClose}
        />

        <View style={base.photo}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={photos}
            extraData={photos}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />
        </View>

        <ButtonIconText
          onPress={this.onPressCamera}
          icon={Images.camera}
          width={wp(50)}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="#D96F22"
          title={I18n.t('choose_from_gallery')}
        />

        <ButtonGradient
          onPress={this.onPressCreateEvent}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('create_event')}
        />
      </Wrap>
    );
  }
}
