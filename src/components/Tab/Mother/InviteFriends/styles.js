import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
    paddingVertical: wp(1),
    paddingHorizontal: wp(2),
    borderColor: '#949494',
    borderWidth: 0.7,
    borderRadius: wp(3),
    marginRight: wp(2),
  },
  invite_friends: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(5),
    color: '#949494',
    marginHorizontal: wp(2),
  },
  padding: {
    paddingVertical: wp(2),
  },
  photo: {
    height: wp(24),
    width: wp(100),
    paddingHorizontal: wp(4),
    alignSelf: 'flex-start',
  },
  photoWrap: {
    paddingTop: wp(4),
    paddingRight: wp(4),
  },
  position: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  round: {
    borderRadius: wp(2),
    overflow: 'hidden',
  },
});

export default {base};
