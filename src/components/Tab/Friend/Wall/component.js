import React from 'react';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarView from '../../../Base/Avatars/AvatarView';
import ListView from './ListView';

// Helpers
import {getUser} from '../../../../helpers';

// Style
import {base} from './styles';

export default class Wall extends React.PureComponent {
  constructor(props) {
    super(props);

    const posts = props.navigation.getParam('posts', null);
    const user = props.navigation.getParam('user', null);

    this.state = {
      posts,
      user,
    };
  }

  render() {
    const {posts, user} = this.state;

    if (posts === null) {
      const {name, city, avatar_url} = getUser(this.props.user);
      const {post} = this.props;

      return (
        <Wrap tab type="friend" style={base.wrap}>
          <AvatarView name={name} city={city} avatar={avatar_url} />
          <ListView post={post} />
        </Wrap>
      );
    }
    const {name, city, avatar_url} = user;
    const {data} = posts;
    console.log(data);

    return (
      <Wrap tab type="friend" style={base.wrap}>
        <AvatarView name={name} city={city} avatar={avatar_url} />
        <ListView post={data} />
      </Wrap>
    );
  }
}
