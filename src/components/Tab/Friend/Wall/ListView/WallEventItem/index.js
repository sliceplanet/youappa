import {connect} from 'react-redux';
import component from './component';

import {getCommentPost} from '../../../../../../store/actions/comment';
import {putLikePost, putUnlikePost} from '../../../../../../store/actions/like';

function mapStateToProps(state) {
  return {
    token: state.user.token,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putLikePost: data => dispatch(putLikePost(data)),
    putUnlikePost: data => dispatch(putUnlikePost(data)),
    getCommentPost: data => dispatch(getCommentPost(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
