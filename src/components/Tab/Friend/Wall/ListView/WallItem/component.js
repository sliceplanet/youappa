import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';

// Components
import Image from '../../../../../Base/Image';
import CommentList from './CommentList';

// Helpers
import * as Images from '../../../../../../helpers/images';
import NavigationService from '../../../../../../helpers/navigation';

// Api
import {apiGetCommentPost} from '../../../../../../store/api/comment';
import {URI} from '../../../../../../store/api';

// Style
import {base} from './styles';

export default class WallItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      more: false,
    };
  }

  componentDidMount() {
    const {token} = this.props;
    const postId = this.props._id;

    const patch = {
      postId,
    };
    apiGetCommentPost({token, patch})
      .then(result => {
        if (result.data.statusCode === 200) {
          this.setState({
            more: result.data.more,
            loading: false,
          });
          this.props.getCommentPost({
            more: result.data.more,
            data: result.data.data,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  onPressComment = () => {
    NavigationService.navigate('Comments', {postId: this.props._id});
  };

  onPressLike = () => {
    const {token, _id, isLike} = this.props;

    const patch = {
      postId: _id,
    };

    if (isLike) {
      this.props.putUnlikePost({token, patch});
    } else {
      this.props.putLikePost({token, patch});
    }
  };

  render() {
    const {createdAt, photos, text, likesCount, isLike, user, _id} = this.props;
    const {loading} = this.state;
    const data = moment(createdAt).format('MM/DD/YYYY');

    if (photos) {
      return (
        <View>
          {photos.length > 0 ? (
            <Image
              style={base.wall}
              containerStyle={base.wall}
              uri={URI + photos[0].path}
            />
          ) : (
            <View style={base.width} />
          )}

          <View style={base.row}>
            <ScalableImage
              source={isLike ? Images.liking : Images.like}
              height={wp(4)}
              onPress={this.onPressLike}
            />
            <Text style={base.likeText}>{likesCount}</Text>
            <ScalableImage
              source={Images.comment}
              height={wp(4)}
              onPress={this.onPressComment}
            />
            <View style={base.flex} />
            <ScalableImage source={Images.favorite} height={wp(4)} />
          </View>

          <Text style={base.text}>
            <Text style={base.bold}>{user.name}</Text>
            {'    '}
            {text}
          </Text>

          <Text style={base.date}>{data}</Text>
          {loading ? (
            <ActivityIndicator style={base.indicator} size="small" />
          ) : (
            <CommentList id={_id} />
          )}
        </View>
      );
    }
    return null;
  }
}
