import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  grid: {
    width: wp(30),
    height: wp(30),
    margin: wp(1),
  },
});

export default {base};
