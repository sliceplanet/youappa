import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  tintColor: {
    tintColor: '#FE695F',
  },
  btn: {
    width: wp(30),
    flexDirection: 'row',
    borderRadius: wp(4),
    borderColor: '#0C8A7E',
    borderWidth: 1,
    padding: wp(2),
    alignItems: 'center',
  },
  btnRed: {
    width: wp(30),
    flexDirection: 'row',
    borderRadius: wp(4),
    borderColor: '#FE695F',
    borderWidth: 1,
    padding: wp(2),
    alignItems: 'center',
  },
  btnText: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp('2.5'),
    fontWeight: '300',
    color: '#0C8A7E',
    paddingLeft: wp(2),
  },
  btnTextRed: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp('2.5'),
    fontWeight: '300',
    color: '#FE695F',
    paddingLeft: wp(2),
  },
  margin: {
    marginVertical: wp(2),
  },
  text: {
    paddingHorizontal: wp(4),
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    fontWeight: '300',
    color: '#555555',
  },
});

export default {base};
