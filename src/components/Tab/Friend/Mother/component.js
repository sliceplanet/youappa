import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarView from '../../../Base/Avatars/AvatarView';
import ModalWishlists from '../../../Base/Modals/ModalWishlists';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// API
import {apiGetUser, apiGetWishlistsUser} from '../../../../store/api/user';
import {apiGetUserPost} from '../../../../store/api/post';
import {apiGetRemoveFriend} from '../../../../store/api/friend';

// Style
import {base} from './styles';

export default class Mother extends React.PureComponent {
  constructor(props) {
    super(props);

    const item = props.navigation.getParam('item', null);

    this.state = {
      userId: item._id,
      user: null,
      wishlists: [],
      posts: null,
      isVisible: false,
    };
  }

  componentDidMount() {
    const {token} = this.props.user;
    const patch = {
      userId: this.state.userId,
    };

    this.props.setNetworkIndicator(true);
    apiGetUser({token, patch})
      .then(result => {
        this.setState({
          user: result.data.data,
        });
        this.props.setNetworkIndicator(false);
      })
      .catch(e => {
        console.log(e);
        this.props.setNetworkIndicator(false);
      });
    apiGetWishlistsUser({token, patch})
      .then(result => {
        this.setState({
          wishlists: result.data.data,
        });
      })
      .catch(e => {
        console.log(e);
      });
    apiGetUserPost({token, patch})
      .then(result => {
        console.log(result);
        this.setState({
          posts: result.data,
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  onPressFollowers = () => {};

  onPressWishlist = () => {
    this.setState({
      isVisible: true,
    });
  };

  onPressPost = () => {
    const {posts, user} = this.state;
    NavigationService.navigate('Wall', {posts, user});
  };

  onPressDecline = () => {
    const {token} = this.props.user;
    const {userId} = this.state;
    const patch = {
      userId,
    };
    apiGetRemoveFriend({token, patch})
      .then(result => {
        console.log(result);
      })
      .catch(e => {
        console.log(e);
        this.setState({
          loading: false,
        });
      });
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  render() {
    const {user, wishlists, posts} = this.state;
    if (user) {
      const {avatar_url, city, name, about, friendCount} = this.state.user;
      return (
        <Wrap tab type="friend">
          <AvatarView
            wishlist
            followers={friendCount}
            onPressFollowers={this.onPressFollowers}
            name={name}
            city={city}
            avatar={avatar_url}
          />

          <View style={[base.row, base.margin]}>
            <View style={base.flex} />

            <TouchableOpacity style={base.btn} onPress={this.onPressWishlist}>
              <View style={base.flex} />
              <ScalableImage source={Images.noteGreen} height={wp(4)} />
              <Text style={base.btnText}>
                {`${wishlists.length} ${I18n.t('wishlists')}`}
              </Text>
              <View style={base.flex} />
            </TouchableOpacity>

            <View style={base.flex} />

            <TouchableOpacity style={base.btn} onPress={this.onPressPost}>
              <View style={base.flex} />
              <ScalableImage source={Images.cameraGreen} height={wp(4)} />
              <Text style={base.btnText}>
                {posts !== null
                  ? `${posts.data.length} ${I18n.t('photo')}`
                  : `0 ${I18n.t('photo')}`}
              </Text>
              <View style={base.flex} />
            </TouchableOpacity>

            <View style={base.flex} />

            <TouchableOpacity style={base.btnRed} onPress={this.onPressDecline}>
              <View style={base.flex} />
              <ScalableImage
                source={Images.close}
                style={base.tintColor}
                height={wp(4)}
              />
              <Text style={base.btnTextRed}>Отписаться</Text>
              <View style={base.flex} />
            </TouchableOpacity>

            <View style={base.flex} />
          </View>

          <ModalWishlists
            wishlists={this.state.wishlists.filter(e => e.privateLevel === 1)}
            isVisible={this.state.isVisible}
            onPressClose={this.onPressClose}
          />

          <Text style={base.text}>{about}</Text>
        </Wrap>
      );
    }
    return <Wrap tab type="friend" />;
  }
}
