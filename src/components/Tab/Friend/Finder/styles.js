import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: wp(2),
  },
  wrap: {
    // flex: 1,
    alignItems: 'center',
    padding: wp(4),
  },
  title: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Regular',
    fontSize: wp(5),
    color: '#555555',
    paddingVertical: wp(2),
  },
  text: {
    flex: 1,
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(4),
    color: '#555555',
  },
  color: {
    tintColor: '#0C8A7E',
  },
});

export default {base};
