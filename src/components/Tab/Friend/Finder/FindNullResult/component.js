import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import ButtonGradient from '../../../../Base/Buttons/ButtonGradient';

// Helpers
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class FindNullResult extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={base.wrap}>
        <View style={base.flex} />
        <Text style={base.text}>{I18n.t('not_found')}</Text>
        <Image source={Images.cat} width={wp(60)} />
        <View style={base.flex} />
        <ButtonGradient
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="white"
          title={I18n.t('refine_search')}
        />
        <View style={base.flex} />
      </View>
    );
  }
}
