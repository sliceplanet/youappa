import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    backgroundColor: '#F4F4F6',
    alignItems: 'center',
  },
  text: {
    width: wp(60),
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    textAlign: 'center',
    color: '#949494',
    marginBottom: wp(4),
  },
});

export default {base};
