import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    width: wp(92),
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    paddingVertical: wp(2),
  },
  border: {
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.5,
  },
  image: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(2),
    overflow: 'hidden',
  },
  title: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: 'black',
    marginBottom: wp(2),
  },
  price: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp('2.5'),
    color: '#949494',
  },
  width: {
    alignSelf: 'flex-end',
    width: wp(50),
    marginHorizontal: wp(4),
  },
  btn: {
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: wp(4),
    borderWidth: 2,
    borderColor: '#069986',
  },
  btnText: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#555555',
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
});

export default {base};
