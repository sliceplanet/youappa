import React from 'react';
import {View, Text, FlatList, TouchableOpacity, Linking} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Img from '../../../../Base/Image';

// Helpers
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class FindResultProduct extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressItem = item => {
    Linking.openURL(item.link);
  };

  renderItem = ({item, index}) => {
    const {image, title, price} = item;
    return (
      <View key={index} style={[base.row, base.border]}>
        <Img style={base.image} containerStyle={base.image} uri={image} />
        <View style={base.width}>
          <Text style={base.title}>{title}</Text>
          <Text style={base.price}>{price} ₽</Text>
        </View>
        <View style={base.flex} />
        <TouchableOpacity
          onPress={() => this.onPressItem(item)}
          style={base.btn}>
          <Text style={base.btnText}>{I18n.t('buy')}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {result} = this.props;
    return (
      <View style={base.wrap}>
        <View style={base.row}>
          <Text style={base.found}>
            {I18n.t('found')} {result.length}{' '}
            {result.length > 1
              ? I18n.t('products').toLowerCase()
              : I18n.t('product').toLowerCase()}
          </Text>
          <View style={base.flex} />
          <Image source={Images.filter} width={wp(4)} />
        </View>
        <FlatList
          data={result}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
