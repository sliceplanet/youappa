import React from 'react';
import {
  View,
  Image as Img,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Image from '../../../../Base/Image';

// Helpers
import * as Images from '../../../../../helpers/images';
import {URI} from '../../../../../store/api';

// API
import {apiGetRequestFriend} from '../../../../../store/api/friend';

// Style
import {base} from './styles';

export default class FindResult extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };
  }

  onPressFriend = item => {
    const {_id} = item;
    const {token} = this.props.user;
    const patch = {
      userId: _id,
    };

    apiGetRequestFriend({token, patch})
      .then(() => {
        this.setState({
          item: this.state.filter(e => e._id !== _id),
        });
      })
      .catch(e => console.log(e));
  };

  renderItem = ({item, index}) => {
    const {avatar_url, name, city} = item;
    return (
      <View style={base.itemWrap} key={index}>
        {avatar_url ? (
          <Image
            uri={`${URI}${avatar_url}`}
            style={base.image}
            containerStyle={base.image}
          />
        ) : (
          <Img source={Images.profile} style={base.image} />
        )}

        <View>
          <Text style={base.text}>{name}</Text>
          <View style={base.row}>
            <ScalableImage source={Images.location} height={wp(4)} />
            <Text style={base.location}>
              {I18n.t('c')} {city}
            </Text>
          </View>
        </View>
        <View style={base.flex} />
        <TouchableOpacity
          style={base.btn}
          onPress={() => this.onPressFriend(item)}>
          <Text style={base.btnText}>{I18n.t('add_to_friend')}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {result} = this.props;
    return (
      <View style={base.wrap}>
        <View style={base.row}>
          <Text style={base.found}>
            {I18n.t('found')} {result.length} {I18n.t('client')}
          </Text>
          <View style={base.flex} />
          <ScalableImage source={Images.filter} width={wp(4)} />
        </View>
        <FlatList
          data={result}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
