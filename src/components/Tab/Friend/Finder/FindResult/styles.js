import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    backgroundColor: '#F4F4F6',
    alignItems: 'center',
    paddingHorizontal: wp(4),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(2),
  },
  found: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
  },
  itemWrap: {
    width: wp(92),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#F4F4F6',
    borderBottomWidth: 0.7,
  },
  image: {
    width: wp(24),
    height: wp(24),
    borderRadius: wp(24),
    overflow: 'hidden',
    margin: wp(2),
    marginRight: wp(4),
  },
  location: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#A6A6A6',
    paddingLeft: wp(1),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'black',
    paddingVertical: wp(1),
  },
  btn: {
    borderColor: '#069986',
    borderWidth: 0.7,
    borderRadius: wp(4),
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
  btnText: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#3F3F3F',
  },
});

export default {base};
