import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Helpers
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class FindEmpty extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={base.wrap}>
        <View style={base.flex} />
        <Image source={Images.searchL} height={wp(40)} />
        <Text style={base.text}>{I18n.t('you_havent_searched_yet')}</Text>
        <View style={base.flex} />
      </View>
    );
  }
}
