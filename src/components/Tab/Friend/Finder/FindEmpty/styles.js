import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    backgroundColor: '#F4F4F6',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#949494',
    marginTop: wp(4),
  },
});

export default {base};
