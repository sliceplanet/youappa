import React from 'react';
import {View, TextInput} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Helpers
import * as Images from '../../../../../helpers/images';

// Style
import {base} from './styles';

export default class FindView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
      finder: 0,
    };
  }

  onChangeText = text => {
    this.setState({text});
    if (text.length === 0) {
      this.props.onFinder(text);
    }
  };

  onSubmitEditing = () => {
    this.props.onFinder(this.state.text);
  };

  onPressUser = () => {
    this.setState({
      finder: 1,
    });
    this.props.onFinderType(1);
  };

  onPressShop = () => {
    this.setState({
      finder: 0,
    });
    this.props.onFinderType(0);
  };

  ref = c => {
    this.input = c;
  };

  clean = () => {
    this.setState({
      text: '',
    });
  };

  render() {
    const {text, finder} = this.state;
    return (
      <View style={base.wrap}>
        <View style={base.row}>
          <Image
            source={Images.shop}
            style={[base.image, finder === 0 && base.finder]}
            height={wp(4)}
            onPress={this.onPressShop}
          />
          <Image
            source={Images.people}
            style={[base.image, finder === 1 && base.finder]}
            height={wp(4)}
            onPress={this.onPressUser}
          />
          <View style={base.rowInput}>
            <TextInput
              ref={this.ref}
              style={base.input}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitEditing}
              value={text}
              placeholder={I18n.t('find')}
              placeholderTextColor="#9B9B9B"
            />
            <Image source={Images.search} style={base.search} height={wp(4)} />
          </View>
        </View>
      </View>
    );
  }
}
