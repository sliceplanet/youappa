import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F4F4F4',
    borderRadius: wp(4),
    paddingLeft: wp(4),
  },
  image: {
    marginRight: wp(2),
  },
  finder: {
    tintColor: '#069986',
  },
  rowInput: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: wp(4),
    borderColor: '#E0E0E0',
    borderWidth: 0.7,
    backgroundColor: 'white',
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
  input: {
    flex: 1,
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#9B9B9B',
  },
  search: {
    tintColor: '#E0E0E0',
  },
});

export default {base};
