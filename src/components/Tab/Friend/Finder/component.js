import React from 'react';

// Components
import Wrap from '../../../Base/Wrap';

import FindView from './FindView';
import FindResult from './FindResult';
import FindResultProduct from './FindResultProduct';
import FindEmpty from './FindEmpty';
import FindNullResult from './FindNullResult';

// Api
import {apiGetSearchFriend} from '../../../../store/api/friend';
import {
  apiPostQueryMarket,
  apiPostStatusQueryMarket,
} from '../../../../store/api/market';

export default class Finder extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      finder: '',
      finderType: 0,
      result: [],
      request: false,
    };
  }

  onFinder = finder => {
    const {token} = this.props.user;
    this.setState({finder});

    if (finder.length > 0) {
      this.setState({
        request: true,
      });
      if (this.state.finderType === 0) {
        const data = {
          text: finder,
        };

        this.props.setNetworkIndicator(true);
        apiPostQueryMarket({token, data})
          .then(result => this.f(result.data.data._id))
          .catch(e => {
            console.log(e);
            this.props.setNetworkIndicator(false);
            this.setState({
              request: false,
            });
          });
      } else {
        const patch = {
          query: finder,
        };
        this.props.setNetworkIndicator(true);
        apiGetSearchFriend({token, patch})
          .then(result => {
            this.props.setNetworkIndicator(false);
            this.setState({
              result: result.data.data,
              request: false,
            });
          })
          .catch(e => {
            console.log(e);
            this.props.setNetworkIndicator(false);
            this.setState({
              request: false,
            });
          });
      }
    }
  };

  onFinderType = finderType => {
    this.setState({
      finderType,
      finder: '',
    });
    this.finder.clean();
  };

  f = queryId => {
    const {token} = this.props.user;
    const patch = {
      queryId,
    };
    apiPostStatusQueryMarket({token, patch})
      .then(result => {
        if (result.data.data.state !== 1) {
          this.f(queryId);
        } else {
          this.setState({
            result: result.data.data.results,
            request: false,
          });

          this.props.setNetworkIndicator(false);
        }
      })
      .catch(e => {
        console.log(e);
        this.props.setNetworkIndicator(false);
        this.setState({
          request: false,
        });
      });
  };

  renderFinder = () => {
    const {finder, result, request, finderType} = this.state;
    if (finder.length > 0) {
      if (result.length > 0) {
        if (finderType === 0) {
          return <FindResultProduct result={result} />;
        }
        return <FindResult result={result} />;
      }
      if (request) {
        return <FindEmpty />;
      }
      return <FindNullResult />;
    }
    return <FindEmpty />;
  };

  ref = c => {
    this.finder = c;
  };

  render() {
    return (
      <Wrap tab type="friend">
        <FindView
          ref={this.ref}
          onFinder={this.onFinder}
          onFinderType={this.onFinderType}
        />
        {this.renderFinder()}
      </Wrap>
    );
  }
}
