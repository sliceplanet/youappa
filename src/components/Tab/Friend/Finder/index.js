import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator} from '../../../../store/actions';
import {postPrivateReport} from '../../../../store/actions/support';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postPrivateReport: data => dispatch(postPrivateReport(data)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
