import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flex: 1,
    alignItems: 'flex-start',
    padding: wp(4),
  },
  text: {
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(4),
    color: '#555555',
    paddingVertical: wp(4),
  },
  line: {
    height: 1,
    width: wp(92),
    backgroundColor: '#EEEEEE',
    marginVertical: wp(2),
  },
});

export default {base};
