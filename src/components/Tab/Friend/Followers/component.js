import React from 'react';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarView from '../../../Base/Avatars/AvatarView';
import ListView from './ListView';

export default class Followers extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const {friends} = this.props;
    if (friends.length > 0) {
      const {token} = this.props.user;
      const patch = {
        userId: this.props.friends[0]._id,
      };
      this.props.getPublicUserPost({token, patch});
    }
  }

  render() {
    const {friends} = this.props;
    if (friends.length > 0) {
      const {name, city, avatar_url} = this.props.friends[0];
      return (
        <Wrap tab type="friend">
          <AvatarView name={name} city={city} avatar={avatar_url} />
          <ListView />
        </Wrap>
      );
    }
    return <Wrap tab type="friend" />;
  }
}
