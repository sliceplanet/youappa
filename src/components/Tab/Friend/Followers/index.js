import {connect} from 'react-redux';
import component from './component';

import {getPublicUserPost} from '../../../../store/actions/post';

function mapStateToProps(state) {
  return {
    user: state.user,
    friends: state.friends,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getPublicUserPost: item => dispatch(getPublicUserPost(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
