import React from 'react';
import {View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';

// Components
import Image from '../../../../../Base/Image';

// Helpers
import * as Images from '../../../../../../helpers/images';
import NavigationService from '../../../../../../helpers/navigation';

// Api
import {URI} from '../../../../../../store/api';
import {apiGetEvent} from '../../../../../../store/api/event';

// Style
import {base} from './styles';

export default class WallEventItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      more: false,
    };
  }

  componentDidMount() {
    const {token} = this.props;
    const {_id} = this.props;
    const patch = {
      eventId: _id,
    };
    apiGetEvent({token, patch})
      .then(result => {
        this.setState({
          isVisible: true,
          ...result.data.data,
        });
      })
      .catch(e => console.log(e));
  }

  onPressComment = () => {
    NavigationService.navigate('Comments', {postId: this.props._id});
  };

  onPressLike = () => {
    const {token, _id, isLike} = this.props;

    const patch = {
      postId: _id,
    };

    if (isLike) {
      this.props.putUnlikePost({token, patch});
    } else {
      this.props.putLikePost({token, patch});
    }
  };

  render() {
    const {isVisible} = this.state;
    if (isVisible) {
      // const {
      //   createdAt,
      //   photos,
      //   text,
      //   likesCount,
      //   isLike,
      //   user,
      //   _id
      // } = this.props;
      // const { loading } = this.state;
      const {createdAt, photos, text, title} = this.state;
      const {user} = this.props;
      const data = moment(createdAt).format('MM/DD/YYYY');

      return (
        <View>
          {photos.length > 0 ? (
            <View>
              <Image
                style={base.wall}
                containerStyle={base.wall}
                uri={URI + photos[0].path}
              />
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#4B4B4B', '#7C7C7C']}
                style={base.linearGradient}>
                <View style={base.noteWrap}>
                  <View style={base.flex} />
                  <ScalableImage
                    style={base.note}
                    source={Images.note}
                    height={wp(12)}
                  />
                  <View style={base.flex} />
                </View>
                <Text style={base.title}>{title}</Text>
              </LinearGradient>
            </View>
          ) : (
            <View style={base.width} />
          )}

          {/* <View style={base.row}>
          <ScalableImage
            source={isLike ? Images.liking : Images.like}
            height={wp(4)}
            onPress={this.onPressLike}
          />
          <Text style={base.likeText}>{likesCount}</Text>
          <ScalableImage
            source={Images.comment}
            height={wp(4)}
            onPress={this.onPressComment}
          />
          <View style={base.flex} />
          <ScalableImage source={Images.favorite} height={wp(4)} />
        </View> */}

          <Text style={base.text}>
            <Text style={base.bold}>{user.name}</Text>
            {'    '}
            {text}
          </Text>

          <Text style={base.date}>{data}</Text>
          {/* {loading ? (
          <ActivityIndicator style={base.indicator} size="small" />
        ) : (
          <CommentList id={_id} />
        )} */}
        </View>
      );
    }
    return null;
  }
}
