import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import I18n from 'i18n-js';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Image from '../../../../../../Base/Image';

// Helpers
import {URI} from '../../../../../../../store/api';
import * as Images from '../../../../../../../helpers/images';
import NavigationService from '../../../../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class CommentList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressComment = () => {
    NavigationService.navigate('Comments', {postId: this.props.id});
  };

  onPressLike = comment => {
    const {_id, isLike} = comment;
    const {token} = this.props.user;

    const patch = {
      commentId: _id,
    };

    if (isLike) {
      this.props.putUnlikeComment({token, patch});
    } else {
      this.props.putLikeComment({token, patch});
    }
  };

  render() {
    let comments = [];
    this.props.comments.forEach(e => {
      if (e.data.length > 0) {
        e.data.forEach(m => {
          if (m.post === this.props.id) {
            comments = [...comments, m];
          }
        });
      } else if (e.data.post === this.props.id) {
        comments = [...comments, e.data];
      }
    });

    if (comments.length > 0) {
      console.log(comments);
      return (
        <View>
          {comments.length > 2 && (
            <TouchableOpacity onPress={this.onPressComment}>
              <Text style={base.allComments}>
                {I18n.t('view_all_comments')}
              </Text>
            </TouchableOpacity>
          )}
          {comments[0] && (
            <View style={base.row}>
              <Image
                uri={URI + comments[0].user.avatar_url}
                style={base.image}
              />
              <View>
                <Text style={base.text}>
                  <Text style={base.bold}>{comments[0].user.name}</Text>{' '}
                  {comments[0].text}
                </Text>
              </View>
              <ScalableImage
                source={comments[0].isLike ? Images.liking : Images.like}
                width={wp(4)}
                onPress={() => this.onPressLike(comments[0])}
              />
            </View>
          )}
          {comments[1] && (
            <View style={base.row}>
              <Image
                uri={URI + comments[1].user.avatar_url}
                style={base.image}
              />
              <View>
                <Text style={base.text}>
                  <Text style={base.bold}>{comments[1].user.name}</Text>{' '}
                  {comments[1].text}
                </Text>
              </View>
              <ScalableImage
                source={comments[1].isLike ? Images.liking : Images.like}
                width={wp(4)}
                onPress={() => this.onPressLike(comments[1])}
              />
            </View>
          )}
        </View>
      );
    }
    return null;
  }
}
