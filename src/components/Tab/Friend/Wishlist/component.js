import React from 'react';
import moment from 'moment';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarWishlist from '../../../Base/Avatars/AvatarWishlist';
import Rubric from '../../../Base/Rubric';
import Wish from '../../../Base/Wish';
import ModalCreateWishlist from '../../../Base/Modals/ModalCreateWishlist';

// API
import {apiPostAddWishlist} from '../../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class WishList extends React.PureComponent {
  constructor(props) {
    super(props);

    const wishlist = props.navigation.getParam('wishlist', null);

    this.state = {
      wishlist,
    };
  }

  render() {
    const {wishlist} = this.state;
    return <Wish wish={wishlist} friend />;
  }
}
