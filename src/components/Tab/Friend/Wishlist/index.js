import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator} from '../../../../store/actions';
import {
  putAddProductWishlist,
  putAddWishlist,
} from '../../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putAddProductWishlist: item => dispatch(putAddProductWishlist(item)),
    putAddWishlist: item => dispatch(putAddWishlist(item)),
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
