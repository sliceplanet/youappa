import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F4F4F6',
  },

  about: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Regular',
    fontSize: wp(5),
    color: 'black',
    paddingHorizontal: wp(4),
    paddingVertical: wp(6),
  },
  text: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
    paddingHorizontal: wp(4),
  },
});

export default {base};
