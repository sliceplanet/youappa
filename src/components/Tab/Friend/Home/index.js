import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    friends: state.friends,
    user: state.user,
  };
}

export default connect(mapStateToProps, null)(component);
