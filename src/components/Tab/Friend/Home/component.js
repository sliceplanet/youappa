import React from 'react';
import {Text} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import AvatarView from '../../../Base/Avatars/AvatarView';
import ModalFriends from '../../../Base/Modals/ModalFriends';

// Helpers
import {getUser} from '../../../../helpers';
import NavigationService from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class Home extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPressFollowers = () => {
    this.setState({
      isVisible: true,
    });
  };

  onPressItem = item => {
    NavigationService.navigate('Mother', {item});
    this.setState({
      isVisible: false,
    });
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  render() {
    const {name, city, avatar_url, about} = getUser(this.props.user);
    const {friends} = this.props;
    const {isVisible} = this.state;

    return (
      <Wrap tab type="friend" style={base.wrap}>
        <AvatarView
          friend
          followers={friends.length}
          name={name}
          city={city}
          avatar={avatar_url}
          onPressFollowers={this.onPressFollowers}
        />

        <Text style={base.about}>{I18n.t('about')}</Text>

        <Text style={base.text}>{about}</Text>

        <ModalFriends
          isVisible={isVisible}
          onPressItem={this.onPressItem}
          onPressClose={this.onPressClose}
        />
      </Wrap>
    );
  }
}
