import React from 'react';
import {Platform} from 'react-native';
import OneSignal from 'react-native-onesignal';

export default class OneSignalView extends React.PureComponent {
  constructor(properties) {
    super(properties);

    OneSignal.init('113f81d6-da61-40cc-8926-08aa087f5852');

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(0);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived = notification => {
    console.log('Notification received: ', notification);
    this.props.setPush(notification.payload);
  };

  onOpened = openResult => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  onIds = device => {
    const {token} = this.props.user;
    const data = {
      pushToken: device.pushToken,
      deviceType: Platform.OS === 'ios' ? 0 : 1,
    };
    this.props.putPushToken({token, data});
    console.log('Device info: ', device);
  };

  render() {
    return null;
  }
}
