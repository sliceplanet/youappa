import {connect} from 'react-redux';
import component from './component';

import {putPushToken, setPush} from '../../../store/actions/pushToken';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putPushToken: item => dispatch(putPushToken(item)),
    setPush: item => dispatch(setPush(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
