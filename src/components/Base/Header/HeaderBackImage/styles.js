import {StyleSheet, Platform} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  wrap: {
    marginLeft: Platform.OS === 'ios' ? wp(5) : 0,
  },
});

export default {base};
