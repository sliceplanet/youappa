import React from 'react';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class HeaderBackImage extends React.PureComponent {
  render() {
    return (
      <Image height={wp(4)} source={Images.headerBack} style={base.wrap} />
    );
  }
}
