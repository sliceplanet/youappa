import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  numbsWrap: {
    alignItems: 'center',
    // padding: wp(4),
    width: wp(100 / 7),
  },
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(6),
    color: 'white',
    padding: wp(2),
  },
  title: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(10),
    color: 'white',
  },
});

export default {base};
