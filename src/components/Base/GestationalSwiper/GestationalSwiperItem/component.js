import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Style
import {base} from './styles';

export default class GestationalSwiperItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => {
    this.props.onPress(this.props.index, this.props.title);
  };

  render() {
    const {title, index} = this.props;
    const i = 35 / 3;
    const deg = Math.round(-35 + i * index);
    let marginTop = index === 4 ? 0 : wp(8);
    if (index < 3) {
      marginTop = Math.pow((7 - 4 - index) * 2.6, 2) + wp(9);
    } else if (index > 3) {
      marginTop = Math.pow(Math.abs(7 - 4 - index) * 2.6, 2) + wp(9);
    }

    return (
      <View
        style={[
          base.numbsWrap,
          {
            transform: [{rotate: `${deg}deg`}],
            marginTop,
          },
        ]}>
        {title >= 0 && title <= 40 ? (
          <TouchableOpacity onPress={this.onPress}>
            <Text style={index === 3 ? base.title : base.text}>{title}</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}
