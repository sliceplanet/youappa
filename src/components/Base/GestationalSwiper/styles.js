import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  position: {
    position: 'absolute',
    bottom: 0,
  },
  titleWrap: {
    flexDirection: 'row',
    width: wp(100),
    position: 'absolute',
    top: wp(54),
  },
  numbsWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(100),
    position: 'absolute',
    top: wp(16),
  },
  align: {
    alignItems: 'center',
  },
  trimesterText: {
    fontFamily: 'Roboto-Bold',
    fontSize: wp(10),
    color: '#5C5C5C',
  },
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    color: '#5C5C5C',
  },
  number: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(5),
    color: '#969696',
    alignSelf: 'center',
    paddingBottom: wp(4),
  },
});

export default {base};
