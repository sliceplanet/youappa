import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import GestationalSwiperItem from './GestationalSwiperItem';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class GestationalSwiper extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      gestational: 0,
      title: 1,
    };
  }

  onPressItem = (index, title) => {
    if (index < 3) {
      const gestational = this.state.gestational - (3 - index);
      if (gestational < 0) {
        return;
      }
      this.setState({
        gestational,
        title: Math.floor(title / 17) + 1,
      });
      this.props.onChangeWeek(gestational);
    }

    if (index > 3) {
      const gestational = this.state.gestational + Math.abs(7 - 4 - index);
      if (gestational > 40) {
        return;
      }
      this.setState({
        gestational,
        title: Math.floor(title / 17) + 1,
      });
      this.props.onChangeWeek(gestational);
    }
  };

  renderNumbs = () => {
    const {gestational} = this.state;
    const numbs = [7];
    for (let i = 0; i < 7; i++) {
      numbs[i] = (
        <GestationalSwiperItem
          key={i}
          index={i}
          title={gestational - 3 + i}
          onPress={this.onPressItem}
        />
      );
    }
    return <View style={base.numbsWrap}>{numbs.map(e => e)}</View>;
  };

  render() {
    return (
      <View style={base.position}>
        <Text style={base.number}>{I18n.t('touch_number')}</Text>

        <Image source={Images.ellipse} width={wp(100)} />
        {this.renderNumbs()}

        <View style={base.titleWrap}>
          <View style={base.flex} />
          <View style={base.align}>
            <Text style={base.trimesterText}>{this.state.title}</Text>
            <Text style={base.text}>{I18n.t('trimester')}</Text>
          </View>
          <View style={base.flex} />
        </View>
      </View>
    );
  }
}
