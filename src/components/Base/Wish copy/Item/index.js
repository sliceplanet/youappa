import {connect} from 'react-redux';
import component from './component';

import {
  putAddProductWishlist,
  putProductWishlist,
  deleteProductWishlist,
} from '../../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putAddProductWishlist: item => dispatch(putAddProductWishlist(item)),
    putProductWishlist: item => dispatch(putProductWishlist(item)),
    deleteProductWishlist: item => dispatch(deleteProductWishlist(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
