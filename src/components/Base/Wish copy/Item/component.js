import React from 'react';
import {View, Text, TouchableOpacity, Linking} from 'react-native';
import I18n from 'i18n-js';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Swipeout from 'react-native-swipeout';

// Components
import Image from '../../Image';
import ModalFriends from '../../Modals/ModalFriends';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';
import {WISH} from '../../../../helpers';

// Api
import {apiPostShareWishlistIdWishlist} from '../../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class Wish extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      modal: false,
    };
  }

  onPress = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };

  onPressShare = () => {
    this.setState({
      modal: true,
    });
  };

  onPressItem = product => {
    const {friend} = this.props;
    if (friend) {
      Linking.canOpenURL(product.buyLink).then(supported => {
        if (supported) {
          Linking.openURL(product.buyLink);
        } else {
          console.log(`Don't know how to open URI: ${product.buyLink}`);
        }
      });
    } else {
      NavigationService.navigate('Web', {
        product,
        setProduct: this.setProduct,
      });
    }
  };

  onPressModalItem = item => {
    const {token} = this.props.user;
    const data = {
      users: [item._id],
      products: this.props.data.map(e => e._id),
    };
    const patch = {
      wishlistId: this.props.wishlistId,
    };
    apiPostShareWishlistIdWishlist({token, data, patch})
      .then(result => console.log(result))
      .catch(e => console.log(e));
    this.setState({
      modal: false,
    });
  };

  onPressModalClose = () => {
    this.setState({
      modal: false,
    });
  };

  onPressRemoveItem = item => {
    const {token} = this.props.user;

    const patch = {
      wishlistId: this.props.wishlistId,
      productId: item._id,
    };
    this.props.deleteProductWishlist({token, patch});
  };

  setProduct = data => {
    const {token} = this.props.user;
    const patch = {
      wishlistId: this.props.wishlistId,
      productId: data._id,
    };
    this.props.putProductWishlist({token, data, patch});
  };

  render() {
    const {type, data} = this.props;
    const {isVisible, modal} = this.state;

    return (
      <View style={base.wrap}>
        <View style={[base.row, isVisible ? base.select : base.unselect]}>
          <ScalableImage
            source={Images[type]}
            width={wp(6)}
            style={base.image}
          />
          <Text style={base.text}>
            {WISH().filter(e => e.type === type)[0].title}
          </Text>
          <Text style={base.count}>
            {data.length}{' '}
            {data.length > 1 ? I18n.t('products') : I18n.t('product')}
          </Text>

          <ScalableImage
            source={Images.share}
            width={wp(4)}
            onPress={this.onPressShare}
          />

          <ScalableImage
            source={isVisible ? Images.up : Images.down}
            width={wp(4)}
            style={base.padding}
            onPress={this.onPress}
          />
        </View>

        {isVisible &&
          data.map((e, i) => {
            const swipeoutBtns = [
              {
                text: I18n.t('delete'),
                type: 'delete',
                onPress: () => this.onPressRemoveItem(e),
              },
            ];

            console.log(e);

            return (
              <Swipeout
                key={i}
                backgroundColor="transparent"
                right={swipeoutBtns}
                autoClose>
                <View style={base.wrapItem}>
                  <Image uri={e.imageUrl} style={base.itemImage} />
                  <Text style={base.itemText}>{e.title}</Text>
                  <Text style={base.itemPrice}>{e.price} ₽</Text>
                  <TouchableOpacity
                    onPress={() => this.onPressItem(e)}
                    style={base.btn}>
                    <Text style={base.btnText}>{I18n.t('buy')}</Text>
                  </TouchableOpacity>
                </View>
              </Swipeout>
            );
          })}
        <ModalFriends
          isVisible={modal}
          onPressItem={this.onPressModalItem}
          onPressClose={this.onPressModalClose}
        />
      </View>
    );
  }
}
