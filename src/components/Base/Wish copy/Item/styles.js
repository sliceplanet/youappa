import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrapItem: {
    height: wp(16),
    flexDirection: 'row',
    alignItems: 'center',
    padding: wp(4),
  },
  wrap: {
    marginVertical: wp('0.5'),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: wp(4),
    paddingRight: 0,
  },
  image: {
    tintColor: '#DD7B27',
  },
  text: {
    flex: 1,
    marginLeft: wp(4),
    fontFamily: 'Roboto-Medium',
    fontSize: wp(4),
    fontWeight: '500',
    color: '#555555',
  },
  count: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#3F3F3F',
    marginRight: wp(4),
  },
  select: {
    backgroundColor: '#fceee1',
  },
  unselect: {
    backgroundColor: 'white',
  },
  itemImage: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(2),
    overflow: 'hidden',
  },
  itemText: {
    flex: 1,
    fontFamily: 'Roboto',
    fontSize: wp(3),
    color: '#555555',
    paddingHorizontal: wp(4),
  },
  itemPrice: {
    fontFamily: 'Roboto',
    fontSize: wp(4),
    fontWeight: '500',
    color: '#555555',
    paddingHorizontal: wp(4),
  },
  btn: {
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: wp(4),
    borderWidth: 2,
    borderColor: '#F2A737',
  },
  btnText: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: '#555555',
    paddingHorizontal: wp(4),
    paddingVertical: wp(2),
  },
  padding: {
    margin: wp(4),
  },
});

export default {base};
