import React from 'react';
import {View} from 'react-native';

// Components
import Item from './Item';

// Helpers
import {WISH} from '../../../helpers';

export default class Wish extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {wishlist, wishlistId, friend} = this.props;

    let array = [];
    WISH().forEach(w => {
      const data = wishlist.filter(e => e.category === w.type);
      if (data.length > 0) {
        array = [...array, {type: w.type, data}];
      }
    });

    return (
      <View>
        {array.map((e, i) => (
          <Item key={i} {...e} wishlistId={wishlistId} friend={friend} />
        ))}
      </View>
    );
  }
}
