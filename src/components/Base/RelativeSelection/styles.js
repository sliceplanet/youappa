import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    marginVertical: wp(4),
    paddingHorizontal: wp(2),
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    borderRadius: wp(4),
    backgroundColor: 'white',
  },
  item: {
    flex: 1,
    alignItems: 'center',
  },
  itemSelect: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#0C8A7E',
    borderRadius: wp(4),
  },
  text: {
    paddingVertical: wp(2),
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
  },
  textSelect: {
    paddingVertical: wp(2),
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'white',
  },
  title: {
    paddingVertical: wp(4),
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#969696',
  },
});

export default {base};
