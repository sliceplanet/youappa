import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import I18n from 'i18n-js';

// Style
import {base} from './styles';

export default class RelativeSelection extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      key: props.value,
    };
  }

  onPress = key => {
    this.setState({key});
    this.props.onPress(key);
  };

  renderItems = () => {
    return (
      <View style={base.row}>
        {this.props.data.map((e, i) => {
          console.log(e);
          return (
            <TouchableOpacity
              key={String(i)}
              style={this.state.key === e.id ? base.itemSelect : base.item}
              onPress={() => this.onPress(e.id)}>
              <Text
                style={this.state.key === e.id ? base.textSelect : base.text}>
                {e.text}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        <Text style={base.title}>{I18n.t('specify_user_type')}</Text>
        {this.renderItems()}
      </View>
    );
  }
}
