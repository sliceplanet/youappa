import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: wp(4),
  },
  activeWrap: {
    width: wp(12),
    height: wp(12),
    paddingTop: wp(3),
    alignItems: 'center',
  },
  position: {
    position: 'absolute',
  },
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    color: 'black',
  },
  line: {
    height: 1,
    backgroundColor: '#979797',
  },
  inactiveWrap: {
    width: wp(8),
    height: wp(8),
    borderColor: '#DEDEDE',
    borderRadius: wp(8),
    borderWidth: 4,
    alignItems: 'center',
    marginHorizontal: wp(1),
  },
  inactiveText: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(3),
    color: 'black',
  },
});

export default {base};
