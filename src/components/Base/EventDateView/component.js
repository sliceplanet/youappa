import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class EventDateView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderNumbs = () => {
    const {days, selectedDate, onPress} = this.props;

    const arr = days.map((e, i) => {
      if (e === selectedDate) {
        return (
          <View key={`d${i}`} style={base.activeWrap}>
            <Image
              source={Images.point}
              style={base.position}
              height={wp(12)}
            />
            <Text style={base.text}>{e}</Text>
          </View>
        );
      }

      return (
        <TouchableOpacity key={`d${i}`} onPress={() => onPress(e)}>
          <View style={base.inactiveWrap}>
            <View style={base.flex} />
            <Text style={base.inactiveText}>{e}</Text>
            <View style={base.flex} />
          </View>
        </TouchableOpacity>
      );
    });

    let result = [];
    for (let i = 0; i < arr.length; i++) {
      const line = (
        <View key={`l${i}`} style={[{flex: 1 / (i + 1)}, base.line]} />
      );
      if (i === arr.length - 1) {
        result = [...result, arr[i]];
      } else {
        result = [...result, arr[i], line];
      }
    }

    return result;
  };

  render() {
    return (
      <View style={base.wrap}>
        {/* <View style={base.activeWrap}>
          <Image source={Images.point} style={base.position} height={wp(12)} />
          <Text style={base.text}>18</Text>
        </View>
        <View style={[base.flex, base.line]} /> */}
        {this.renderNumbs()}
      </View>
    );
  }
}
