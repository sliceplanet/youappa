import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexItem: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  tabWrap: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: wp(4),
    justifyContent: 'space-between',
  },
  tabWrapFriend: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(4),
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  bg: {
    position: 'absolute',
    bottom: 0,
  },
  tintColor: {
    tintColor: '#484848',
  },
  tintColor2: {
    tintColor: '#969696',
  },
});

export default {base};
