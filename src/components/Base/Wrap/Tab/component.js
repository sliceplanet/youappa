import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import ModalAdd from '../../Modals/ModalAdd';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class Tab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  navigateHome = () => {
    this.props.setIndex(0);
    NavigationService.navigate('Home');
  };

  navigateCart = () => {
    this.props.setIndex(1);
    NavigationService.navigate('Subscriber');
  };

  navigatePlus = () => {
    this.setState({
      isVisible: true,
    });
  };

  navigateAlert = () => {
    this.props.setIndex(3);
    NavigationService.navigate('Events');
  };

  navigateProfile = () => {
    this.props.setIndex(2);
    NavigationService.navigate('Profile');
  };

  navigateSearch = () => {
    this.props.setIndex(1);
    NavigationService.navigate('Finder');
  };

  navigateFavorites = () => {
    this.props.setIndex(2);

    NavigationService.navigate('Followers');
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  renderMother = () => {
    const {index} = this.props;
    const {isVisible} = this.state;

    return (
      <View style={base.flex}>
        <View style={base.flex}>{this.props.children}</View>
        <View style={base.tabWrap}>
          <Image source={Images.tabs} style={base.bg} width={wp(100)} />

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateHome}>
              <Image
                source={Images.home}
                height={wp(6)}
                style={index === 0 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateCart}>
              <Image
                source={Images.cart}
                height={wp(6)}
                style={index === 1 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.alignSelf}>
            <TouchableOpacity onPress={this.navigatePlus}>
              <Image source={Images.plus} height={wp(16)} />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateAlert}>
              <Image
                source={Images.alert}
                height={wp(6)}
                style={index === 3 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateProfile}>
              <Image
                source={Images.profile}
                height={wp(6)}
                style={index === 2 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>
        </View>
        <ModalAdd isVisible={isVisible} onPressClose={this.onPressClose} />
      </View>
    );
  };

  renderFriend = () => {
    const {index} = this.props;
    return (
      <View style={base.flex}>
        <View style={base.flex}>{this.props.children}</View>
        <View style={base.tabWrapFriend}>
          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateHome}>
              <Image
                source={Images.home}
                height={wp(6)}
                style={index === 0 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateSearch}>
              <Image
                source={Images.search}
                height={wp(6)}
                style={index === 1 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateFavorites}>
              <Image
                source={Images.favorites}
                height={wp(6)}
                style={index === 2 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>

          <View style={base.flexItem}>
            <TouchableOpacity onPress={this.navigateAlert}>
              <Image
                source={Images.alert}
                height={wp(6)}
                style={index === 3 ? base.tintColor : base.tintColor2}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return this.props.type === 'mother'
      ? this.renderMother()
      : this.renderFriend();
  }
}
