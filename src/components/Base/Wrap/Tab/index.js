import {connect} from 'react-redux';
import component from './component';

import {setIndex} from '../../../../store/actions';

function mapStateToProps(state) {
  return {
    index: state.tab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setIndex: item => dispatch(setIndex(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
