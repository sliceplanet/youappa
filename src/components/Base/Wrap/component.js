import React from 'react';
import {View, RefreshControl} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

// Components
import Tab from './Tab';

// Style
import {base} from './styles';

export default class Wrap extends React.PureComponent {
  render() {
    const {style, onRefresh, tab, children, type, noScroll} = this.props;
    if (!noScroll) {
      return (
        <View style={base.flex}>
          <KeyboardAwareScrollView
            contentContainerStyle={[base.flex, style]}
            enableOnAndroid
            enableAutomaticScroll
            showsVerticalScrollIndicator={false}
            refreshControl={
              onRefresh ? (
                <RefreshControl refreshing={false} onRefresh={onRefresh} />
              ) : null
            }>
            {tab ? <Tab type={type}>{children}</Tab> : children}
          </KeyboardAwareScrollView>
        </View>
      );
    }
    return (
      <View style={base.flex}>
        {tab ? <Tab type={type}>{children}</Tab> : children}
      </View>
    );
  }
}
