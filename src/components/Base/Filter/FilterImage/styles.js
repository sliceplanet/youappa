import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  position: {
    position: 'absolute',
    alignSelf: 'center',
    top: wp(35),
  },
});

export default {base};
