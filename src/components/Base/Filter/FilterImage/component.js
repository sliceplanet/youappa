import React from 'react';
import {View, ActivityIndicator, Image} from 'react-native';
import * as FilterKit from 'react-native-image-filter-kit';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Style
import {base} from './styles';

export default class FilterImage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      indication: true,
    };
  }

  onFilteringStart = () => {
    this.setState({indication: true});
  };

  onFilteringFinish = () => {
    this.setState({indication: false});
    this.props.onLoading();
  };

  onFilteringError = () => {
    this.setState({indication: false});
  };

  render() {
    const {base64, filter} = this.props;
    const {indication} = this.state;

    if (base64.length > 0) {
      switch (filter) {
        case 0: {
          return (
            <View>
              <FilterKit._1977
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 1: {
          return (
            <View>
              <FilterKit.Aden
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 2: {
          return (
            <View>
              <FilterKit.Brannan
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 3: {
          return (
            <View>
              <FilterKit.Brooklyn
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 4: {
          return (
            <View>
              <FilterKit.Clarendon
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 5: {
          return (
            <View>
              <FilterKit.Earlybird
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 6: {
          return (
            <View>
              <FilterKit.Gingham
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 7: {
          return (
            <View>
              <FilterKit.Hudson
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 8: {
          return (
            <View>
              <FilterKit.Inkwell
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 9: {
          return (
            <View>
              <FilterKit.Kelvin
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 10: {
          return (
            <View>
              <FilterKit.Lark
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 11: {
          return (
            <View>
              <FilterKit.Lofi
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 12: {
          return (
            <View>
              <FilterKit.Maven
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 13: {
          return (
            <View>
              <FilterKit.Mayfair
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 14: {
          return (
            <View>
              <FilterKit.Moon
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 15: {
          return (
            <View>
              <FilterKit.Nashville
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 16: {
          return (
            <View>
              <FilterKit.Perpetua
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 17: {
          return (
            <View>
              <FilterKit.Reyes
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 18: {
          return (
            <View>
              <FilterKit.Rise
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 19: {
          return (
            <View>
              <FilterKit.Slumber
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 20: {
          return (
            <View>
              <FilterKit.Stinson
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 21: {
          return (
            <View>
              <FilterKit.Toaster
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 22: {
          return (
            <View>
              <FilterKit.Valencia
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 23: {
          return (
            <View>
              <FilterKit.Walden
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 24: {
          return (
            <View>
              <FilterKit.Willow
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        case 25: {
          return (
            <View>
              <FilterKit.Xpro2
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={
                  <Image
                    source={{uri: base64}}
                    style={{width: wp(92), height: wp(92)}}
                  />
                }
              />
              {indication && (
                <ActivityIndicator style={base.position} size="large" />
              )}
            </View>
          );
        }
        default: {
          return (
            <Image
              source={{uri: base64}}
              style={{width: wp(92), height: wp(92)}}
            />
          );
        }
      }
    }
    return null;
  }
}
