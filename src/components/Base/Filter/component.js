import React from 'react';
import {View} from 'react-native';
import ViewShot from 'react-native-view-shot';

// Components
import FilterImage from './FilterImage';
import Filters from './Filters';

// Style
import {base} from './styles';

export default class Filter extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      filter: -1,
    };
  }

  onPressFilters = filter => {
    this.setState({filter});
  };

  onLoading = () => {
    this.viewShot.capture().then(base64 => {
      this.props.onChange(`data:image/jpg;base64,${base64}`);
    });
  };

  ref = c => {
    this.viewShot = c;
  };

  render() {
    return (
      <View style={base.wrap}>
        <ViewShot
          ref={this.ref}
          options={{format: 'jpg', quality: 1, result: 'base64'}}>
          <FilterImage
            filter={this.state.filter}
            base64={this.props.image}
            onLoading={this.onLoading}
          />
        </ViewShot>
        <Filters image={this.props.resize} onPress={this.onPressFilters} />
      </View>
    );
  }
}
