import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    alignItems: 'center',
  },
});

export default {base};
