import React from 'react';
import {View, ActivityIndicator, TouchableOpacity} from 'react-native';
import * as FilterKit from 'react-native-image-filter-kit';

// Style
import {base} from './styles';

export default class FilterItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      indication: false,
    };
  }

  onPress = () => {
    const {onPress, id} = this.props;
    onPress(id);
  };

  onFilteringStart = () => {
    this.setState({indication: true});
  };

  onFilteringFinish = () => {
    this.setState({indication: false});
  };

  onFilteringError = () => {
    this.setState({indication: false});
  };

  render() {
    const {type, image} = this.props;
    const {indication} = this.state;

    switch (type) {
      case '_1977': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit._1977
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Aden': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Aden
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Brannan': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Brannan
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Brooklyn': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Clarendon
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Clarendon': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Clarendon
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Earlybird': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Earlybird
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Gingham': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Gingham
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Hudson': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Hudson
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Inkwell': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Inkwell
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Kelvin': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Kelvin
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Lark': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Lark
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Lofi': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Lofi
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Maven': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Maven
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Mayfair': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Mayfair
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Moon': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Moon
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Nashville': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Nashville
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Perpetua': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Perpetua
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Reyes': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Reyes
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Rise': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Rise
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Slumber': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Slumber
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Stinson': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Stinson
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Toaster': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Toaster
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Valencia': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Valencia
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Walden': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Walden
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Willow': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Willow
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      case 'Xpro2': {
        return (
          <View style={base.item}>
            <TouchableOpacity onPress={this.onPress}>
              <FilterKit.Xpro2
                onFilteringStart={this.onFilteringStart}
                onFilteringFinish={this.onFilteringFinish}
                onFilteringError={this.onFilteringError}
                image={image}
              />
            </TouchableOpacity>
            {indication && (
              <ActivityIndicator style={base.position} size="small" />
            )}
          </View>
        );
      }
      default: {
        return null;
      }
    }
  }
}
