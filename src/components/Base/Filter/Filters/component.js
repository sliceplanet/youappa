import React from 'react';
import {View, ScrollView, Image} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import FilterItem from './FilterItem';

// Helpers
import {FILTERS} from '../../../../helpers';

// Style
import {base} from './styles';

export default class Filters extends React.PureComponent {
  render() {
    const {image, onPress} = this.props;
    if (image.length > 0) {
      return (
        <View style={base.wrap}>
          <View style={base.height}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {FILTERS.map((e, i) => {
                return (
                  <FilterItem
                    key={i}
                    id={i}
                    onPress={onPress}
                    type={e}
                    image={
                      <Image
                        source={{uri: image}}
                        style={{width: wp(20), height: wp(20)}}
                      />
                    }
                  />
                );
              })}
            </ScrollView>
          </View>
        </View>
      );
    }
    return null;
  }
}
