import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  item: {
    margin: wp(1),
  },
  height: {
    height: wp(20),
  },
});

export default {base};
