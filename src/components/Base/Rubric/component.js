import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helperss
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class Rubric extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => {
    this.props.onPress(this.props.category);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View style={base.wrap}>
          <View style={base.radius}>
            <Image
              source={Images[`wish_category_${this.props.category}`]}
              height={wp(8)}
            />
            <Text style={base.text}>{this.props.title}</Text>
          </View>
          <Image style={base.icon} source={Images.roundPlus} width={wp(8)} />
        </View>
      </TouchableOpacity>
    );
  }
}
