import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(33),
    marginVertical: wp(2),
    alignItems: 'center',
  },
  radius: {
    width: wp(30),
    height: wp(30),
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: wp(2),
    paddingVertical: wp(4),
    paddingHorizontal: wp(2),
    marginBottom: wp(4),
  },
  icon: {
    position: 'absolute',
    bottom: 0,
  },
  text: {
    fontFamily: 'Roboto-Medium',
    fontSize: wp(3),
    textAlign: 'center',
    color: '#969696',
    paddingVertical: wp(2),
  },
});

export default {base};
