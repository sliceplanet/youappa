import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.7,
    marginBottom: wp(4),
    padding: wp(2),
  },
  wrapMultiline: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#E0E0E0',
    borderWidth: 0.7,
    borderRadius: wp(2),
    marginVertical: wp(4),
    padding: wp(2),
  },
  input: {
    flex: 1,
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
    paddingTop: 0,
  },
  text: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
  },
  placeholder: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#9B9B9B',
  },
  padding: {
    paddingHorizontal: wp(2),
  },
});

export default {base};
