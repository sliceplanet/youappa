import React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class Input extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      secure: true,
      date: false,
    };
  }

  onSubmitEditing = () => {
    this.props.onSubmitEditing();
  };

  onPressIn = () => {
    this.setState({secure: false});
  };

  onPressOut = () => {
    this.setState({secure: true});
  };

  focus = () => {
    this.input.focus();
  };

  showDate = () => {
    this.setState({date: true});
  };

  hideDate = () => {
    this.setState({date: false});
  };

  handleDate = date => {
    this.hideDate();
    this.props.onChangeText(date);
  };

  ref = ref => {
    this.input = ref;
  };

  renderIcon = () => {
    if (this.props.secureTextEntry) {
      return (
        <TouchableOpacity
          style={base.padding}
          onPressIn={this.onPressIn}
          onPressOut={this.onPressOut}>
          <Image width={wp(4)} source={Images.eye} />
        </TouchableOpacity>
      );
    }
    if (this.props.email) {
      return (
        <TouchableOpacity style={base.padding} onPress={this.props.onPressUser}>
          <Image width={wp(4)} source={Images.user} />
        </TouchableOpacity>
      );
    }
    if (this.props.icon) {
      if (this.props.onPressIcon) {
        return (
          <Image
            height={wp(4)}
            source={this.props.icon}
            style={base.padding}
            onPress={this.props.onPressIcon}
          />
        );
      }
      return (
        <Image height={wp(4)} source={this.props.icon} style={base.padding} />
      );
    }

    return null;
  };

  renderMultiline = () => {
    const {style, onChangeText, value, placeholder} = this.props;
    return (
      <View style={[base.wrapMultiline, style]}>
        <TextInput
          ref={this.ref}
          style={base.input}
          onChangeText={onChangeText}
          value={value}
          placeholder={placeholder}
          placeholderTextColor="#9B9B9B"
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          multiline
        />
      </View>
    );
  };

  renderDate = () => {
    const {style, value, placeholder, maximumDate, date, time} = this.props;
    let mode = 'date';
    if (time) {
      mode = 'time';
    }
    if (date && time) {
      mode = 'datetime';
    }

    return (
      <TouchableOpacity onPress={this.showDate} style={[base.wrap, style]}>
        <DateTimePicker
          isVisible={this.state.date}
          onConfirm={this.handleDate}
          onCancel={this.hideDate}
          maximumDate={maximumDate}
          mode={mode}
        />

        <Text style={value === null ? base.placeholder : base.text}>
          {value === null ? placeholder : moment(value).format('ll')}
        </Text>
        <View style={base.flex} />
        {this.renderIcon()}
      </TouchableOpacity>
    );
  };

  renderInput = () => {
    const {
      style,
      onChangeText,
      value,
      placeholder,
      keyboardType,
      returnKeyType,
      secureTextEntry,
      autoCapitalize,
    } = this.props;
    return (
      <View style={[base.wrap, style]}>
        <TextInput
          ref={this.ref}
          style={base.input}
          onChangeText={onChangeText}
          value={value}
          placeholder={placeholder}
          placeholderTextColor="#9B9B9B"
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          onSubmitEditing={this.onSubmitEditing}
          secureTextEntry={secureTextEntry ? this.state.secure : false}
          underlineColorAndroid="transparent"
          autoCapitalize={autoCapitalize || 'none'}
        />
        {this.renderIcon()}
      </View>
    );
  };

  render() {
    const {multiline, date, time} = this.props;
    if (multiline) {
      return this.renderMultiline();
    }
    if (date || time) {
      return this.renderDate();
    }
    return this.renderInput();
  }
}
