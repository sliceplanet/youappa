import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class AvatarSelection extends React.PureComponent {
  constructor(props) {
    super(props);

    const {value} = props;

    this.state = {
      avatar: value || '',
    };
  }

  onPress = () => {
    const options = {
      mediaType: 'photo',
      quality: 0.1,
      storageOptions: {skipBackup: true, cameraRoll: false},
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          avatar: `data:${response.type};base64,${response.data}`,
        });
        this.props.onChange(`data:${response.type};base64,${response.data}`);
      }
    });
  };

  renderButton = () => {
    return (
      <View style={this.props.mother ? base.border : base.borderFriend}>
        <View style={this.props.mother ? base.radius : base.radiusFriend}>
          <View style={base.image}>
            <View style={base.flex} />
            <ScalableImage
              style={base.tintColor}
              width={wp(8)}
              source={Images.camera}
            />
            <Text style={base.text}>{I18n.t('upload_your_photo')}</Text>
            <View style={base.flex} />
          </View>
        </View>
      </View>
    );
  };

  renderAvatar = () => {
    const {avatar} = this.state;
    return (
      <View style={this.props.mother ? base.border : base.borderFriend}>
        <View style={this.props.mother ? base.radius : base.radiusFriend}>
          <Image source={{uri: avatar}} style={base.avatar} />
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        <TouchableOpacity onPress={this.onPress}>
          {this.state.avatar.length > 0
            ? this.renderAvatar()
            : this.renderButton()}
        </TouchableOpacity>
      </View>
    );
  }
}
