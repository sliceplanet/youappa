import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    marginVertical: wp(4),
    paddingHorizontal: wp(2),
    alignItems: 'center',
  },
  image: {
    width: wp(70),
    height: wp(70),
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: wp(70),
  },
  radius: {
    borderRadius: wp(70),
    borderWidth: wp(3),
    borderColor: '#FE922E66',
  },
  radiusFriend: {
    alignItems: 'center',
    borderRadius: wp(70),
    borderWidth: wp(3),
    borderColor: '#37A89B66',
  },
  border: {
    borderWidth: wp(3),
    borderColor: '#FE922E33',
    borderRadius: wp(70),
  },
  borderFriend: {
    borderWidth: wp(3),
    borderColor: '#37A89B33',
    borderRadius: wp(70),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#A3A3A3',
    paddingVertical: wp(2),
  },
  avatar: {
    width: wp(70),
    height: wp(70),
    overflow: 'hidden',
    borderRadius: wp(70),
  },
  tintColor: {
    tintColor: '#CFCFCF',
  },
});

export default {base};
