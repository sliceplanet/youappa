import React from 'react';
import {View, Text, Image} from 'react-native';
import I18n from 'i18n-js';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import ButtonIconText from '../../Buttons/ButtonIconText';
import Img from '../../Image';

// Helpers
import * as Images from '../../../../helpers/images';
import {AVATARS} from '../../../../helpers';
import {URI} from '../../../../store/api';

// Style
import {base} from './styles';

export default class AvatarWishlist extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {name, bornDate, createWishlist, countWishlist} = this.props;
    const {usedPhotoSource, photoUrl, photoId} = this.props.avatar;
    return (
      <View style={base.wrap}>
        {usedPhotoSource === 'url' ? (
          <Img
            style={base.image}
            containerStyle={base.image}
            uri={URI + photoUrl}
          />
        ) : (
          <Image style={base.image} source={AVATARS[photoId]} />
        )}
        <View>
          <Text style={base.name}>{name}</Text>
          {bornDate && <Text style={base.year}>{bornDate}</Text>}

          {createWishlist && (
            <ButtonIconText
              style={base.margin}
              onPress={this.props.onPressCreateWishlist}
              width={wp(50)}
              icon={Images.add}
              gradientColors={['#F1A737', '#F2A737']}
              titleColor="#DD7B27"
              title={I18n.t('create_desire')}
            />
          )}
        </View>
        {!createWishlist && (
          <>
            <View style={base.flex} />

            <View style={base.rubricWrap}>
              <Text style={base.rubricText}>
                {countWishlist} {I18n.t('rubric')}
              </Text>
            </View>
          </>
        )}
      </View>
    );
  }
}
