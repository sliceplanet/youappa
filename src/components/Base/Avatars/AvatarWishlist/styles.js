import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(1),
  },
  wrap: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingBottom: wp(2),
    paddingRight: wp(4),
  },
  image: {
    width: wp(24),
    height: wp(24),
    borderRadius: wp(24),
    overflow: 'hidden',
    margin: wp(2),
    marginRight: wp(4),
  },
  name: {
    fontFamily: 'Roboto-Regular',
    fontSize: wp(4),
    fontWeight: '800',
    color: 'black',
  },
  year: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#969696',
    paddingBottom: wp(4),
  },
  rubricWrap: {
    paddingHorizontal: wp(4),
    paddingVertical: wp(1),
    borderColor: '#E0E0E0',
    borderWidth: 0.7,
    borderRadius: wp(4),
  },
  rubricText: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#969696',
  },
  margin: {
    marginTop: wp(4),
  },
});

export default {base};
