import React from 'react';
import {View, Text, TouchableOpacity, Image as Img} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Components
import Image from '../../Image';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

import {URI} from '../../../../store/api';

// Style
import {base} from './styles';

export default class AvatarView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  navigationFollowers = () => {
    NavigationService.navigate('Followers');
  };

  renderFollowers = () => {
    const {followers, onPressFollowers} = this.props;
    if (followers) {
      if (followers > 0) {
        return (
          <TouchableOpacity
            onPress={onPressFollowers || this.navigationFollowers}>
            <Text style={base.followers}>
              {followers} {I18n.t('followers')}
            </Text>
          </TouchableOpacity>
        );
      }
    }
    return null;
  };

  render() {
    const {city, name, friend, wishlist, avatar} = this.props;
    return (
      <View style={base.wrap}>
        {avatar ? (
          <Image
            uri={URI + avatar}
            style={base.image}
            containerStyle={base.image}
          />
        ) : (
          <Img source={Images.profile} style={base.image} />
        )}

        <View style={base.row}>
          <View>
            <View style={base.row}>
              <ScalableImage source={Images.location} height={wp(4)} />
              <Text style={base.location}>
                {I18n.t('c')} {city}
              </Text>
            </View>
            <Text style={base.text}>{name}</Text>
            <View style={base.row}>
              {friend && <Text style={base.followers}>Коллега</Text>}
              {this.renderFollowers()}
            </View>
          </View>
        </View>
        <View style={base.flex} />
        {wishlist && (
          <View style={base.align}>
            {/* <Text style={base.location}>В сети 25 мин. назад</Text> */}
            {/* <View style={base.row}>
              <ScalableImage source={Images.checked} width={wp(2)} />
              <ScalableImage source={Images.user} width={wp(4)} />
              <ScalableImage
                source={Images.note}
                width={wp(4)}
                style={base.margin}
              />
              <ScalableImage source={Images.info} width={wp(4)} />
            </View> */}
            <ScalableImage
              source={Images.close}
              width={wp(4)}
              onPress={() => NavigationService.goBack()}
            />
          </View>
        )}
      </View>
    );
  }
}
