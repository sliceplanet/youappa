import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(1),
  },
  wrap: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: wp(5),
    paddingVertical: wp(2),
    borderBottomColor: '#F4F4F6',
    borderBottomWidth: 4,
  },
  image: {
    width: wp(18),
    height: wp(18),
    borderRadius: wp(18),
    overflow: 'hidden',
    marginRight: wp(4),
  },
  location: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#A6A6A6',
    paddingLeft: wp(1),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'black',
    paddingVertical: wp(1),
  },
  followers: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
    paddingVertical: wp(1),
    paddingHorizontal: wp(2),
    borderColor: '#949494',
    borderWidth: 0.7,
    borderRadius: wp(3),
    marginRight: wp(2),
  },
  align: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    paddingVertical: wp(2),
  },
  margin: {
    marginHorizontal: wp(4),
    marginVertical: wp(1),
  },
});

export default {base};
