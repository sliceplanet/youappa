import {connect} from 'react-redux';
import component from './component';

import {setI18n} from '../../../store/actions';

function mapStateToProps(state) {
  return {
    i18n: state.i18n,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setI18n: item => dispatch(setI18n(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
