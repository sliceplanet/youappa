import React from 'react';
import {View, Text} from 'react-native';
import I18n from 'i18n-js';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import ModalDropdown from '../Modals/ModalDropdown';

// Helpers
import * as Images from '../../../helpers/images';
import {LANG} from '../../../helpers';

// Style
import {base} from './styles';

export default class DropdownLanguage extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      idx: -1,
      lang: props.i18n,
    };
  }

  onSelect = (idx, lang) => {
    this.setState({idx, lang});
    this.props.setI18n(lang);
  };

  render() {
    const {lang} = this.state;
    return (
      <View style={this.props.style}>
        <ModalDropdown
          style={base.ddWrap}
          dropdownStyle={base.width}
          options={LANG}
          onSelect={this.onSelect}
          showsVerticalScrollIndicator={false}>
          <View style={base.wrap}>
            <Text style={base.text}>{I18n.t(`lang_${lang}`)}</Text>
            <Image source={Images.dropDown} width={wp(2)} />
          </View>
        </ModalDropdown>
      </View>
    );
  }
}
