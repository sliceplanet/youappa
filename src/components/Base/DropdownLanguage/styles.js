import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  wrap: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.7,
    paddingRight: wp(2),
  },
  ddWrap: {
    width: wp(90),
    paddingVertical: wp(4),
  },
  width: {
    width: wp(90),
    height: 'auto',
  },
  text: {
    flex: 1,
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#3F3F3F',
    padding: wp(2),
  },
  hint: {
    flex: 1,
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    padding: wp(2),
    color: '#969696',
  },
});

export default {base};
