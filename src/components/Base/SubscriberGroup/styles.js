import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(100),
    backgroundColor: '#F4F4F6',
    paddingTop: wp(4),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
  },
  sentText: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
    marginRight: wp(2),
  },
  round: {
    borderColor: '#B4B4B4',
    borderWidth: 0.7,
    borderRadius: wp(3),
    paddingHorizontal: wp(2),
    paddingVertical: wp(1),
  },
  padding: {
    padding: wp(2),
  },
  height: {
    height: wp(28),
    paddingBottom: wp(4),
  },
  image: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(12),
    overflow: 'hidden',
  },
  imagePosition: {
    position: 'absolute',
    right: 0,
    bottom: wp(7),
    borderColor: '#F4F4F6',
    borderWidth: 3,
  },
  addPosition: {
    position: 'absolute',
    right: wp(3),
    bottom: wp(4),
  },
  itemWrap: {
    width: wp(18),
    marginHorizontal: wp(2),
    alignItems: 'center',
  },
  textGroup: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
    paddingTop: wp(8),
  },
});

export default {base};
