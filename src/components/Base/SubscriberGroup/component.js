import React from 'react';
import {View, Image, FlatList, Text, TouchableOpacity} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

const TEST = [
  {
    group: 'Семья',
    image: [
      {uri: 'https://picsum.photos/id/436/200/200'},
      {uri: 'https://picsum.photos/id/437/200/200'},
    ],
  },
  {
    group: 'Друзья',
    image: [
      {uri: 'https://picsum.photos/id/436/200/200'},
      {uri: 'https://picsum.photos/id/437/200/200'},
    ],
  },
  {
    group: 'Коллеги',
    image: [
      {uri: 'https://picsum.photos/id/436/200/200'},
      {uri: 'https://picsum.photos/id/437/200/200'},
    ],
  },
  {
    group: 'Кумы',
    image: [
      {uri: 'https://picsum.photos/id/436/200/200'},
      {uri: 'https://picsum.photos/id/437/200/200'},
    ],
  },
  {
    group: 'Дальние',
    image: [
      {uri: 'https://picsum.photos/id/436/200/200'},
      {uri: 'https://picsum.photos/id/437/200/200'},
    ],
  },
];

export default class SubscriberGroup extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectGroup: 2,
    };
  }

  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity>
        <View style={base.itemWrap} key={index}>
          <Image style={base.image} source={item.image[0]} />
          <Image
            style={[base.image, base.imagePosition]}
            source={item.image[1]}
          />
          <ScalableImage
            style={base.addPosition}
            source={Images.addGroup}
            width={wp(6)}
          />
          <Text style={base.textGroup}>{item.group}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {selectGroup} = this.state;
    return (
      <View style={base.wrap}>
        <View style={[base.row, base.padding]}>
          {selectGroup > 0 && (
            <Text style={base.text}>Выбрана {selectGroup} группа</Text>
          )}
          <View style={base.flex} />
          <TouchableOpacity>
            <View style={[base.row, base.round]}>
              <Text style={base.sentText}>Отправить всем</Text>
              <ScalableImage source={Images.send} height={wp(4)} />
            </View>
          </TouchableOpacity>
        </View>

        <View style={base.height}>
          <FlatList
            data={TEST}
            showsHorizontalScrollIndicator={false}
            horizontal
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <ScalableImage source={Images.wave} width={wp(100)} />
      </View>
    );
  }
}
