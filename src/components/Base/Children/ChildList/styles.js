import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    marginVertical: wp(1),
  },
  wrapItem: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: wp(10),
    marginVertical: wp(1),
    padding: wp(2),
    paddingRight: wp(5),
  },
  itemImage: {
    width: wp(10),
    height: wp(10),
    borderRadius: wp(10),
    overflow: 'hidden',
    marginRight: wp(2),
  },
  name: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: 'black',
  },
  date: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#949494',
  },
});

export default {base};
