import React from 'react';
import {View, Image, Text} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';
import I18n from 'i18n-js';

// Helpers
import * as Images from '../../../../helpers/images';
import {AVATARS} from '../../../../helpers';
import {URI} from '../../../../store/api';

import {base} from './styles';

export default class ChildList extends React.PureComponent {
  renderItem = (item, index) => {
    const {usedPhotoSource, _id, name, photoUrl, photoId, bornDateMs} = item;

    const image =
      usedPhotoSource === 'url' ? {uri: URI + photoUrl} : AVATARS[photoId];

    const now = moment();
    const date = moment.unix(bornDateMs);
    const diffYears = now.diff(date, 'years');
    const diffMonth = now.diff(date, 'month');
    const diffDay = now.diff(date, 'day');

    let bornDate = '';
    if (diffYears > 0) {
      bornDate = `${diffYears} ${I18n.t('year')}`;
    } else if (diffMonth > 0) {
      bornDate = `${diffMonth} ${I18n.t(diffMonth > 1 ? 'months' : 'month')}`;
    } else if (diffDay > 0) {
      bornDate = `${diffDay} ${I18n.t(diffDay > 1 ? 'days' : 'day')}`;
    } else {
      bornDate = I18n.t('newborn');
    }

    const {token} = this.props.user;
    const patch = {
      childId: _id,
    };

    return (
      <View style={base.wrapItem} key={index}>
        <Image source={image} style={base.itemImage} />
        <View>
          <Text style={base.name}>{name}</Text>
          <Text style={base.date}>{bornDate}</Text>
        </View>
        <View style={base.flex} />
        <ScalableImage
          width={wp(4)}
          source={Images.close}
          onPress={() => this.props.deleteChild({token, patch})}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        {this.props.babies.map((item, index) => this.renderItem(item, index))}
      </View>
    );
  }
}
