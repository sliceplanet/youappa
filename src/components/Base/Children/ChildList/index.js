import {connect} from 'react-redux';
import component from './component';

import {deleteChild} from '../../../../store/actions/child';

function mapStateToProps(state) {
  return {
    babies: state.babies,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    deleteChild: data => dispatch(deleteChild(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
