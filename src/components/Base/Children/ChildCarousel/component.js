import React from 'react';
import {View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helpers
import {AVATARS} from '../../../../helpers';

// Style
import {base} from './styles';

export default class ChildCarousel extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      activeSlide: -1,
    };
  }

  onSnapToItem = activeSlide => {
    this.setState({activeSlide});
    this.props.onSnapToItem(activeSlide);
  };

  renderItem = ({item, index}) => {
    return (
      <View key={index} style={base.itemWrap}>
        <Image height={wp(50)} width={wp(50)} source={item} />
      </View>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        <Carousel
          data={AVATARS}
          renderItem={this.renderItem}
          sliderWidth={wp(100)}
          itemWidth={wp(50)}
          onSnapToItem={this.onSnapToItem}
        />
      </View>
    );
  }
}
