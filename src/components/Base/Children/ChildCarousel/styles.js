import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  itemWrap: {
    alignItems: 'center',
  },
  wrap: {
    height: wp(50),
  },
});

export default {base};
