import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    marginVertical: wp(4),
    paddingHorizontal: wp(2),
  },
  rowWrap: {
    flexDirection: 'row',
  },
  title: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#555555',
    marginBottom: wp(2),
  },
  button: {
    width: wp(40),
  },
  buttonWrap: {
    alignItems: 'center',
  },
  buttonTitle: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(3),
    color: '#8C8C8C',
    marginTop: wp(1),
  },
});

export default {base};
