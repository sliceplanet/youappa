import React from 'react';
import {View, Text} from 'react-native';
import I18n from 'i18n-js';

// Components
import ButtonIcon from '../../Buttons/ButtonIcon';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ChildSex extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      select: -1,
    };
  }

  onPressBoy = () => {
    this.setState({
      select: 0,
    });
    this.props.onPress('boy');
  };

  onPressGirl = () => {
    this.setState({
      select: 1,
    });
    this.props.onPress('girl');
  };

  render() {
    const {select} = this.state;
    return (
      <View style={base.wrap}>
        <Text style={base.title}>{I18n.t('specify_the_sex_of_the_child')}</Text>
        <View style={base.rowWrap}>
          <View style={base.buttonWrap}>
            <ButtonIcon
              style={base.button}
              icon={Images.girl}
              gradientColors={
                select === 1 ? ['#969696', '#5E5E5E'] : ['#FF515E', '#FB786B']
              }
              onPress={this.onPressGirl}
            />
            <Text style={base.buttonTitle}>{I18n.t('girl')}</Text>
          </View>

          <View style={base.flex} />

          <View style={base.buttonWrap}>
            <ButtonIcon
              style={base.button}
              icon={Images.boy}
              gradientColors={
                select === 0 ? ['#969696', '#5E5E5E'] : ['#4D92FC', '#7DB6FB']
              }
              onPress={this.onPressBoy}
            />
            <Text style={base.buttonTitle}>{I18n.t('boy')}</Text>
          </View>
        </View>
      </View>
    );
  }
}
