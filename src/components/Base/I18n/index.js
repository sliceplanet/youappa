import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    i18n: state.i18n,
  };
}

export default connect(mapStateToProps, null)(component);
