import React from 'react';
import i18njs from 'i18n-js';
import memoize from 'lodash.memoize';

const translationGetters = {
  en: () => require('./translations/en.json'),
  es: () => require('./translations/es.json'),
  ru: () => require('./translations/ru.json'),
};

const translate = memoize(
  (key, config) => i18njs.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

const setI18nConfig = lang => {
  translate.cache.clear();
  i18njs.translations = {[lang]: translationGetters[lang]()};
  i18njs.locale = lang;
};

export default class I18n extends React.PureComponent {
  constructor(props) {
    super(props);
    const {i18n} = props;
    setI18nConfig(i18n);
  }

  componentDidUpdate(prevProps) {
    if (this.props.i18n !== prevProps.i18n) {
      const {i18n} = this.props;
      setI18nConfig(i18n);
      this.forceUpdate();
    }
  }

  render() {
    const {children} = this.props;
    return <>{children}</>;
  }
}
