import {connect} from 'react-redux';
import component from './component';

import {
  putAddProductWishlist,
  putProductWishlist,
  putUpdateWishlist,
  deleteProductWishlist,
  deleteWishlist,
} from '../../../store/actions/wishlist';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    putAddProductWishlist: item => dispatch(putAddProductWishlist(item)),
    putProductWishlist: item => dispatch(putProductWishlist(item)),
    putUpdateWishlist: item => dispatch(putUpdateWishlist(item)),
    deleteProductWishlist: item => dispatch(deleteProductWishlist(item)),
    deleteWishlist: item => dispatch(deleteWishlist(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
