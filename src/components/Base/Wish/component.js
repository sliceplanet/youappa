import React from 'react';
import {View, Text, TouchableOpacity, Linking} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';
import Swipeout from 'react-native-swipeout';

import Image from '../Image';
import ModalFriends from '../Modals/ModalFriends';
import ModalEditeWishlist from '../Modals/ModalEditeWishlist';

// Helpers
import NavigationService from '../../../helpers/navigation';
import * as Images from '../../../helpers/images';

// Api
import {apiPostShareWishlistIdWishlist} from '../../../store/api/wishlist';

// Style
import {base} from './styles';

export default class Wish extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      modal: false,
      isVisibleWishlist: false,
    };
  }

  onPressItem = product => {
    const {friend} = this.props;
    if (friend) {
      Linking.canOpenURL(product.buyLink).then(supported => {
        if (supported) {
          Linking.openURL(product.buyLink);
        } else {
          console.log(`Don't know how to open URI: ${product.buyLink}`);
        }
      });
    } else {
      NavigationService.navigate('Web', {
        product,
        setProduct: this.setProduct,
      });
    }
  };

  onPress = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };

  onPressRemoveWishlist = () => {
    const {token} = this.props.user;

    const patch = {
      wishlistId: this.props.wish._id,
    };
    this.props.deleteWishlist({token, patch});
    NavigationService.goBack();
  };

  onPressRemoveItem = item => {
    const {token} = this.props.user;

    const patch = {
      wishlistId: this.props.wish._id,
      productId: item._id,
    };
    this.props.deleteProductWishlist({token, patch});
  };

  onPressShare = () => {
    this.setState({
      modal: true,
    });
  };

  onPressLock = () => {
    this.props.onChangeWishlistLock(this.props.wish);
  };

  onPressModalItem = item => {
    const {token} = this.props.user;
    const data = {
      users: [item._id],
      products: this.props.wish.products.map(e => e._id),
    };
    const patch = {
      wishlistId: this.props.wish._id,
    };
    apiPostShareWishlistIdWishlist({token, data, patch})
      .then(result => console.log(result))
      .catch(e => console.log(e));
    this.setState({
      modal: false,
    });
  };

  onPressModalClose = () => {
    this.setState({
      modal: false,
    });
  };

  onPressEditWishlist = () => {
    this.setState({
      isVisibleWishlist: true,
    });
  };

  onPressCloseWishlist = () => {
    this.setState({
      isVisibleWishlist: false,
    });
  };

  onPressAcceptWishlist = title => {
    if (title.length > 0) {
      this.onPressCloseWishlist();

      const {token} = this.props.user;
      const patch = {
        wishlistId: this.props.wish._id,
      };
      const data = {
        title,
      };

      this.props.putUpdateWishlist({token, patch, data});
    }
  };

  setProduct = data => {
    const {token} = this.props.user;
    const patch = {
      wishlistId: this.props.wish._id,
      productId: data._id,
    };
    this.props.putProductWishlist({token, data, patch});
  };

  renderSwipe = () => {
    const {wish, friend} = this.props;
    const {isVisible, modal} = this.state;

    return (
      <View style={base.wrap}>
        <View style={[base.row, isVisible ? base.select : base.unselect]}>
          <ScalableImage
            source={
              wish.category >= 0
                ? Images[`wish_category_${wish.category}`]
                : Images.wish_category_0
            }
            width={wp(6)}
            style={base.image}
          />
          <Text style={base.text}>{wish.title}</Text>
          <Text style={base.count}>
            {wish.products.length}{' '}
            {wish.products.length > 1 ? I18n.t('products') : I18n.t('product')}
          </Text>

          {!friend && (
            <ScalableImage
              source={Images.lock}
              width={wp(4)}
              style={wish.privateLevel === 0 ? base.unlock : base.lock}
              onPress={this.onPressLock}
            />
          )}

          {!friend && (
            <ScalableImage
              source={Images.share}
              width={wp(5)}
              onPress={this.onPressShare}
            />
          )}

          <ScalableImage
            source={isVisible ? Images.up : Images.down}
            width={wp(4)}
            style={base.padding}
            onPress={this.onPress}
          />
        </View>

        {isVisible &&
          wish.products.map(e => {
            const swipeoutBtns = [
              {
                text: I18n.t('delete'),
                type: 'delete',
                onPress: () => this.onPressRemoveItem(e),
              },
            ];

            return (
              <Swipeout
                key={e._id}
                disabled={friend}
                backgroundColor="transparent"
                right={swipeoutBtns}
                autoClose>
                <View style={base.wrapItem}>
                  <Image uri={e.imageUrl} style={base.itemImage} />
                  <Text style={base.itemText}>{e.title}</Text>
                  <Text style={base.itemPrice}>{e.price} ₽</Text>
                  <TouchableOpacity
                    onPress={() => this.onPressItem(e)}
                    style={base.btn}>
                    <Text style={base.btnText}>{I18n.t('buy')}</Text>
                  </TouchableOpacity>
                </View>
              </Swipeout>
            );
          })}

        <ModalFriends
          isVisible={modal}
          onPressItem={this.onPressModalItem}
          onPressClose={this.onPressModalClose}
        />
      </View>
    );
  };

  render() {
    const {friend} = this.props;

    const {isVisible, isVisibleWishlist} = this.state;

    if (isVisible) {
      return this.renderSwipe();
    }
    const swipeoutBtns = [
      {
        text: I18n.t('edit'),
        type: 'primary',
        onPress: () => this.onPressEditWishlist(),
      },
      {
        text: I18n.t('delete'),
        type: 'delete',
        onPress: () => this.onPressRemoveWishlist(),
      },
    ];
    return (
      <Swipeout
        disabled={friend}
        backgroundColor="transparent"
        right={swipeoutBtns}
        autoClose>
        {this.renderSwipe()}

        <ModalEditeWishlist
          isVisible={isVisibleWishlist}
          onPressClose={this.onPressCloseWishlist}
          onPressAccept={this.onPressAcceptWishlist}
        />
      </Swipeout>
    );
  }
}
