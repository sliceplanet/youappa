import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {base} from './styles';

export default class ButtonGradient extends React.PureComponent {
  render() {
    const {
      width,
      style,
      onPress,
      gradientColors,
      titleColor,
      title,
    } = this.props;
    return (
      <View style={style}>
        <TouchableOpacity onPress={onPress}>
          <LinearGradient
            colors={gradientColors}
            start={{x: 0.0, y: 1.0}}
            end={{x: 1.0, y: 1.0}}
            style={[base.gradient, width && {width}]}>
            <Text style={[base.text, {color: titleColor}]}>{title}</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
