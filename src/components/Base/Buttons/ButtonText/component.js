import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {base} from './styles';

export default class ButtonText extends React.PureComponent {
  render() {
    const {style, onPress, title, titleColor} = this.props;
    return (
      <View style={[base.wrap, style]}>
        <TouchableOpacity onPress={onPress}>
          <Text style={[base.text, {color: titleColor}]}>{title}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
