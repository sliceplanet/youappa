import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  gradient: {
    borderRadius: wp(6),
  },
  wrap: {
    width: wp(50),
    borderRadius: wp(6),
    alignItems: 'center',
    backgroundColor: 'white',
    margin: 0.7,
  },
  text: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    paddingVertical: wp(3),
  },
});

export default {base};
