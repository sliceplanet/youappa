import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  wrap: {
    alignSelf: 'flex-start',
    paddingHorizontal: wp(2),
  },
  text: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#969696',
  },
  row: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
  },
  flex: {
    flex: 1,
  },
});

export default {base};
