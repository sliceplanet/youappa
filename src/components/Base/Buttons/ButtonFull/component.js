import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

import {base} from './styles';

export default class ButtonFull extends React.PureComponent {
  render() {
    return (
      <View style={[base.wrap, this.props.style]}>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={base.row}>
            <Text style={base.text}>{this.props.title}</Text>

            <View style={base.flex} />

            <Image height={this.props.height} source={this.props.icon} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
