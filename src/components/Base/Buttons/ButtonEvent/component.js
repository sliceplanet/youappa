import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import I18n from 'i18n-js';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Helpers
import * as Images from '../../../../helpers/images';

import {base} from './styles';

export default class ButtonEvent extends React.PureComponent {
  render() {
    if ((this, this.props.accept)) {
      return (
        <View style={[base.roundAccept, this.props.style]}>
          <TouchableOpacity onPress={this.props.onPress}>
            <View style={base.row}>
              <Image height={wp(3)} source={Images.accept} />

              <Text style={base.textAccept}>{I18n.t('accept')}</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={[base.roundReject, this.props.style]}>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={base.row}>
            <Image height={wp(3)} source={Images.reject} />

            <Text style={base.textReject}>{I18n.t('reject')}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
