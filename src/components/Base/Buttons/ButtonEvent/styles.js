import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  textReject: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#FE695F',
    marginLeft: wp(2),
  },
  textAccept: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#0FB36E',
    marginLeft: wp(2),
  },
  row: {
    // width: wp(25),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp(2),
    paddingVertical: wp(1),
  },
  roundReject: {
    borderColor: '#FE695F',
    borderWidth: 0.7,
    borderRadius: wp(4),
    marginLeft: wp(2),
    marginTop: wp(2),
  },
  roundAccept: {
    borderColor: '#0FB36E',
    borderWidth: 0.7,
    borderRadius: wp(4),
    marginTop: wp(2),
  },
});

export default {base};
