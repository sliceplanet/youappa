import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  gradient: {
    borderRadius: wp(6),
  },
  wrap: {
    flexDirection: 'row',
    width: wp(86),
    borderRadius: wp(6),
    alignItems: 'center',
    backgroundColor: 'white',
    margin: 0.7,
  },
  text: {
    fontFamily: 'SFUIDisplay-Regular',
    fontSize: wp(4),
    paddingVertical: wp(4),
    paddingLeft: wp(2),
  },
});

export default {base};
