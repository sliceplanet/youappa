import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {base} from './styles';

export default class ButtonIconText extends React.PureComponent {
  render() {
    return (
      <View style={this.props.style}>
        <TouchableOpacity onPress={this.props.onPress}>
          <LinearGradient
            colors={this.props.gradientColors}
            start={{x: 0.0, y: 1.0}}
            end={{x: 1.0, y: 1.0}}
            style={base.gradient}>
            <View
              style={[
                base.wrap,
                this.props.width && {width: this.props.width},
              ]}>
              <View style={base.flex} />
              <Image
                width={wp(4)}
                style={this.props.iconStyle}
                source={this.props.icon}
              />
              <Text style={[base.text, {color: this.props.titleColor}]}>
                {this.props.title}
              </Text>
              <View style={base.flex} />
            </View>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  }
}
