import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  gradient: {
    alignItems: 'center',
    padding: wp(4),
    borderRadius: wp(2),
  },
});

export default {base};
