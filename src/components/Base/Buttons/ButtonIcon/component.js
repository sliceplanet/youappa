import React from 'react';
import {TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Style
import {base} from './styles';

export default class ButtonIcon extends React.PureComponent {
  renderBtn = () => {
    const Icon = this.props.icon;
    return <Icon />;
  };

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <LinearGradient
          colors={this.props.gradientColors}
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          style={[base.gradient, this.props.style]}>
          <Image source={this.props.icon} height={wp(5)} width={wp(5)} />
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
