import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    width: wp(90),
    marginVertical: wp(4),
    paddingHorizontal: wp(2),
  },
  icon: {
    borderRadius: wp(3),
    overflow: 'hidden',
  },
  iconWrap: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteWrap: {
    width: wp(16),
    height: wp(16),
    backgroundColor: 'white',
    borderRadius: wp(3),
  },
  done: {
    position: 'absolute',
  },
  text: {
    fontFamily: 'SFUIDisplay-Light',
    fontSize: wp(4),
    color: '#555555',
    paddingVertical: wp(2),
  },
  margin: {
    margin: wp('0.5'),
  },
});

export default {base};
