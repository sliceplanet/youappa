import React from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

// Helpers
import {COVERS} from '../../../helpers';
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class CoverSelection extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      key: 0,
    };
  }

  onPress = key => {
    this.setState({key});
    this.props.onPress(key);
  };

  keyExtractor = (item, index) => index.toString();

  renderItem = ({index, item}) => {
    const {key} = this.state;
    if (item === null) {
      return (
        <TouchableOpacity
          style={base.margin}
          key={index}
          onPress={() => this.onPress(index)}>
          <View style={base.iconWrap}>
            <View style={base.whiteWrap} />
            {index === key ? (
              <Image width={wp(6)} source={Images.done} style={base.done} />
            ) : null}
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity
        style={base.margin}
        key={index}
        onPress={() => this.onPress(index)}>
        <View style={base.iconWrap}>
          <Image
            source={item}
            style={base.icon}
            width={wp(16)}
            height={wp(16)}
          />
          {index === key ? (
            <Image width={wp(6)} source={Images.done} style={base.done} />
          ) : null}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        <Text style={base.text}>{I18n.t('choose_a_cover')}</Text>
        <FlatList
          horizontal
          data={COVERS}
          extraData={this.state}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}
