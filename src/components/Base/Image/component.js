import React from 'react';
import {View, ActivityIndicator, Platform, Image as Img} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

// Style
import {base} from './styles';

export default class Image extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      path: '',
      uri: '',
    };
  }

  componentDidMount() {
    const {uri, cache} = this.props;
    const filter = cache.filter(e => e.uri === uri);
    if (filter.length > 0) {
      RNFetchBlob.fs
        .exists(filter[0].path)
        .then(exist => {
          if (exist) {
            this.setState({
              uri,
              path: filter[0].path,
            });
          } else {
            this.props.removeCache({uri});
            this.fetch(uri);
          }
        })
        .catch(() => {});
    } else {
      this.fetch(uri);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.cache !== prevProps.cache) {
      const {uri, cache} = this.props;
      const filter = cache.filter(e => e.uri === uri);
      if (filter.length > 0) {
        RNFetchBlob.fs
          .exists(filter[0].path)
          .then(exist => {
            if (exist) {
              this.setState({
                uri,
                path: filter[0].path,
              });
            } else {
              this.props.removeCache({uri});
              this.fetch(uri);
            }
          })
          .catch(e => {
            console.log(e);
          });
      }
    }
  }

  fetch = uri => {
    RNFetchBlob.config({
      fileCache: true,
    })
      .fetch('GET', uri)
      .then(resp => {
        const path =
          Platform.OS === 'android'
            ? `file://${resp.path()}`
            : `${resp.path()}`;
        this.setState({
          path,
          uri,
        });
        this.props.setCache({path, uri});
      });
  };

  render() {
    const {path} = this.state;
    const {style, containerStyle} = this.props;
    if (path.length > 0) {
      return <Img source={{uri: path}} style={style} />;
    }
    return (
      <View style={[base.wrap, containerStyle]}>
        <View style={base.flex} />
        <ActivityIndicator size="small" />
        <View style={base.flex} />
      </View>
    );
  }
}
