import {connect} from 'react-redux';
import component from './component';

import {setCache, removeCache} from '../../../store/actions';

function mapStateToProps(state) {
  return {
    cache: state.cache,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setCache: data => dispatch(setCache(data)),
    removeCache: data => dispatch(removeCache(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
