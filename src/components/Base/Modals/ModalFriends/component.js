import React from 'react';
import {View, ScrollView} from 'react-native';
import Modal from 'react-native-modal';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import FriendItem from '../../../Tab/Mother/Friends/FriendItem';

// Style
import {base} from './styles';

export default class ModalFriends extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = e => {
    this.props.onPressItem(e);
  };

  render() {
    const {isVisible, friends} = this.props;

    if (friends.length > 0) {
      return (
        <Modal
          style={base.center}
          isVisible={isVisible}
          onBackButtonPress={this.props.onPressClose}
          onBackdropPress={this.props.onPressClose}>
          <View style={base.wrap}>
            <View style={base.height}>
              <ScrollView>
                {friends.map((e, i) => {
                  return (
                    <FriendItem
                      {...e}
                      width={wp(92)}
                      key={i}
                      onPress={this.onPress}
                    />
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </Modal>
      );
    }
    this.props.onPressClose();
    return null;
  }
}
