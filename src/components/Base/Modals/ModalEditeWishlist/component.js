import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Input from '../../Input';
import ButtonIconText from '../../Buttons/ButtonIconText';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ModalEditeWishlist extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
    };
  }

  onChangeName = name => {
    this.setState({
      name,
    });
  };

  done = () => {
    this.props.onPressAccept(this.state.name);
  };

  refName = c => {
    this.name = c;
  };

  render() {
    const {isVisible} = this.props;
    const {name} = this.state;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <Input
            ref={this.refName}
            style={base.input}
            onChangeText={this.onChangeName}
            value={name}
            placeholder={I18n.t('name')}
            keyboardType="default"
            returnKeyType="next"
            onSubmitEditing={this.done}
          />

          <ButtonIconText
            onPress={this.done}
            width={wp(70)}
            icon={Images.add}
            gradientColors={['#F1A737', '#F2A737']}
            titleColor="#DD7B27"
            title={I18n.t('change_wishlist_name')}
          />
        </View>
      </Modal>
    );
  }
}
