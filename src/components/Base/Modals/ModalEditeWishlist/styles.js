import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    width: wp(90),
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp(4),
  },
  input: {
    width: wp(82),
  },
  image: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(2),
    overflow: 'hidden',
  },
  wrapItem: {
    height: wp(16),
    flexDirection: 'row',
    alignItems: 'center',
  },
  height: {
    maxHeight: wp(100),
  },
  text: {
    flex: 1,
    fontFamily: 'Roboto',
    fontSize: wp(3),
    color: '#555555',
    paddingHorizontal: wp(4),
  },
  price: {
    fontFamily: 'Roboto',
    fontSize: wp(4),
    fontWeight: '500',
    color: '#555555',
    paddingHorizontal: wp(4),
  },
});

export default {base};
