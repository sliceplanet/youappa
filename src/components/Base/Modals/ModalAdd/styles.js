import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  wrap: {
    width: wp(60),
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp(4),
    alignItems: 'center',
  },
  text: {
    fontFamily: 'Roboto',
    fontSize: wp(4),
    color: '#555555',
  },
  textWrap: {
    paddingVertical: wp(2),
  },
});

export default {base};
