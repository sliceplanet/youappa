import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';

// Helpers
import NavigationService from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class ModalAdd extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressChild = () => {
    NavigationService.navigate('GestationalAge');
    this.props.onPressClose();
  };

  onPressEvent = () => {
    NavigationService.navigate('AddEvent');
    this.props.onPressClose();
  };

  onPressPost = () => {
    NavigationService.navigate('PhotoFilter');
    this.props.onPressClose();
  };

  render() {
    const {isVisible} = this.props;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <TouchableOpacity style={base.textWrap} onPress={this.onPressChild}>
            <Text style={base.text}>{I18n.t('add_child')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.textWrap} onPress={this.onPressEvent}>
            <Text style={base.text}>{I18n.t('add_event')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.textWrap} onPress={this.onPressPost}>
            <Text style={base.text}>{I18n.t('add_post')}</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
