import React from 'react';
import {View, ScrollView} from 'react-native';
import Modal from 'react-native-modal';

// Components
import ModalUserItem from './ModalUserItem';

// Style
import {base} from './styles';

export default class ModalUser extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {isVisible, user} = this.props;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <View style={base.height}>
            <ScrollView>
              {user.users.map((e, i) => {
                return (
                  <ModalUserItem
                    key={i}
                    {...e}
                    onPress={this.props.onPressItem}
                  />
                );
              })}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}
