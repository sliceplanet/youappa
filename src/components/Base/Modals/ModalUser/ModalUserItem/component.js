import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

// Components
import Image from '../../../Image';

// Api
import {URI} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class ModalUser extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => {
    this.props.onPress(this.props._id);
  };

  render() {
    const {avatar_url, name} = this.props;
    return (
      <TouchableOpacity style={base.row} onPress={this.onPress}>
        <Image uri={URI + avatar_url} style={base.image} />
        <Text style={base.text}>{name}</Text>
      </TouchableOpacity>
    );
  }
}
