import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    padding: wp(2),
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
  },
  wrap: {
    width: wp(90),
    backgroundColor: 'white',
    borderRadius: wp(4),
    padding: wp(4),
  },
  image: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(2),
    overflow: 'hidden',
  },
  height: {
    maxHeight: wp(100),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: '#A3A3A3',
    paddingHorizontal: wp(2),
  },
});

export default {base};
