import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import Modal from 'react-native-modal';

// Helpers
import NavigationSevice from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class ModalWishlists extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = wishlist => {
    NavigationSevice.navigate('Wishlist', {wishlist});
    this.props.onPressClose();
  };

  render() {
    const {isVisible, wishlists} = this.props;

    if (wishlists.length > 0) {
      return (
        <Modal
          style={base.center}
          isVisible={isVisible}
          onBackButtonPress={this.props.onPressClose}
          onBackdropPress={this.props.onPressClose}>
          <View style={base.wrap}>
            <View style={base.height}>
              <ScrollView>
                {wishlists.map(e => {
                  return (
                    <TouchableOpacity
                      style={base.row}
                      key={e._id}
                      onPress={() => this.onPress(e)}>
                      <Text style={base.text}>{e.title}</Text>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </Modal>
      );
    }
    return null;
  }
}
