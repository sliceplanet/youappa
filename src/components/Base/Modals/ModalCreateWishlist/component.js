import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Input from '../../Input';
import ButtonIconText from '../../Buttons/ButtonIconText';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ModalCreateWishlist extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      description: '',
    };
  }

  onChangeName = name => {
    this.setState({
      name,
    });
  };

  onChangeDescription = description => {
    this.setState({
      description,
    });
  };

  nextDescription = () => {
    this.description.focus();
  };

  done = () => {
    if (this.state.name.length === 0) {
      this.props.showToast(I18n.t('show_name_wishlist'));
      return;
    }
    this.props.onPressAccept(this.state);
  };

  refName = c => {
    this.name = c;
  };

  refDescription = c => {
    this.description = c;
  };

  render() {
    const {isVisible} = this.props;
    const {name, description} = this.state;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <Input
            ref={this.refName}
            style={base.input}
            onChangeText={this.onChangeName}
            value={name}
            placeholder={I18n.t('name')}
            keyboardType="default"
            returnKeyType="next"
            onSubmitEditing={this.nextDescription}
          />
          <Input
            ref={this.refDescription}
            style={base.input}
            multiline
            onChangeText={this.onChangeDescription}
            value={description}
            placeholder={I18n.t('description')}
            keyboardType="default"
            onSubmitEditing={this.done}
          />

          <ButtonIconText
            onPress={this.done}
            width={wp(50)}
            icon={Images.add}
            gradientColors={['#F1A737', '#F2A737']}
            titleColor="#DD7B27"
            title={I18n.t('create_desire')}
          />
        </View>
      </Modal>
    );
  }
}
