import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modal';
import I18n from 'i18n-js';

// Components
import Input from '../../Input';
import Image from '../../Image';

// API
import {
  apiPostQueryMarket,
  apiPostStatusQueryMarket,
} from '../../../../store/api/market';

// Style
import {base} from './styles';

export default class ModalProducts extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      input: '',
      result: [],
      loading: false,
    };
  }

  onChangeText = input => {
    this.setState({
      input,
    });
  };

  f = queryId => {
    const {token} = this.props.user;
    const patch = {
      queryId,
    };
    apiPostStatusQueryMarket({token, patch})
      .then(result => {
        if (result.data.data.state !== 1) {
          this.f(queryId);
        } else {
          this.setState({
            result: result.data.data.results,
          });

          this.setState({
            loading: false,
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({
          loading: false,
        });
      });
  };

  search = () => {
    const {token} = this.props.user;
    const {input} = this.state;

    const data = {
      text: input,
    };

    if (input.length > 0) {
      this.setState(
        {
          loading: true,
        },
        () => {
          apiPostQueryMarket({token, data})
            .then(result => this.f(result.data.data._id))
            .catch(e => {
              console.log(e);
              this.setState({
                loading: false,
              });
            });
        },
      );
    }
  };

  render() {
    const {isVisible} = this.props;
    const {input, result, loading} = this.state;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={this.props.onPressClose}
        onBackdropPress={this.props.onPressClose}>
        <View style={base.wrap}>
          <Input
            style={base.input}
            onChangeText={this.onChangeText}
            value={input}
            placeholder={I18n.t('refine_search')}
            keyboardType="default"
            returnKeyType="search"
            onSubmitEditing={this.search}
          />

          {loading ? (
            <ActivityIndicator size="small" />
          ) : (
            <View style={base.height}>
              <ScrollView>
                {result.map((e, i) => {
                  return (
                    <TouchableOpacity
                      key={i}
                      style={base.wrapItem}
                      onPress={() => this.props.onPressItem(e)}>
                      <Image uri={e.image} style={base.image} />
                      <Text style={base.text}>{e.title}</Text>
                      <Text style={base.price}>{e.price} ₽</Text>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>
          )}
        </View>
      </Modal>
    );
  }
}
