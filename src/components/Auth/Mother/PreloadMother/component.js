import React from 'react';
import {View, Text} from 'react-native';
import I18n from 'i18n-js';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonWhite from '../../../Base/Buttons/ButtonWhite';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class PreloadMother extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  resetPreloadFriend = () => {
    NavigationService.reset('PreloadFriend');
  };

  navigateSignInMother = () => {
    NavigationService.navigate('SignInMother');
  };

  render() {
    return (
      <Wrap>
        <Image
          width={wp(100)}
          source={Images.background}
          style={base.absolute}
        />
        <Image width={wp(100)} source={Images.backgroundMother} />

        <View style={base.flex} />

        <View style={base.wrap}>
          <Text style={base.text}>{I18n.t('i_am_a_mother')}</Text>
          <ButtonWhite
            onPress={this.resetPreloadFriend}
            style={base.padding}
            gradientColors={['#0C8A7E', '#069986']}
            titleColor="#0C8A7E"
            title={I18n.t('i_am_a_friend')}
          />
          <ButtonGradient
            onPress={this.navigateSignInMother}
            style={base.padding}
            gradientColors={['#DD7B27', '#F79C41']}
            titleColor="white"
            title={I18n.t('next')}
          />
        </View>
      </Wrap>
    );
  }
}
