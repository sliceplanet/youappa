import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  padding: {
    paddingVertical: wp(4),
  },
  wrap: {
    width: wp(100),
    alignItems: 'center',
    paddingVertical: wp(4),
  },
  absolute: {
    position: 'absolute',
    bottom: 0,
  },
  text: {
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(7),
    color: '#3F3F3F',
  },
});

export default {base};
