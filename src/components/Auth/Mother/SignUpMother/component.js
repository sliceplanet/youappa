import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import DropdownLanguage from '../../../Base/DropdownLanguage';
import Input from '../../../Base/Input';

// Helpers
import {checkEmail} from '../../../../helpers';

// Style
import {base} from './styles';

export default class SignUpMother extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      rePassword: '',
    };
  }

  onChangeTextEmail = email => {
    this.setState({email});
  };

  onChangeTextPassword = password => {
    this.setState({password});
  };

  onChangeTextRePassword = rePassword => {
    this.setState({rePassword});
  };

  nextPassword = () => {
    this.password.focus();
  };

  nextRePassword = () => {
    this.rePassword.focus();
  };

  check = () => {
    const {email, password, rePassword} = this.state;
    if (!checkEmail(email)) {
      this.props.showToast(I18n.t('invalid_email_address'));
      return;
    }
    if (password !== rePassword) {
      this.props.showToast(I18n.t('passwords_do_not_match'));
      return;
    }
    if (password.length < 6) {
      this.props.showToast(I18n.t('short_password'));
      return;
    }
    this.props.postSignUp({
      email,
      password,
      type: 1,
    });
  };

  refPassword = ref => {
    this.password = ref;
  };

  refRePassword = ref => {
    this.rePassword = ref;
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <DropdownLanguage />

        <Input
          onChangeText={this.onChangeTextEmail}
          value={this.state.email}
          placeholder={I18n.t('enter_your_email')}
          keyboardType="email-address"
          returnKeyType="next"
          onSubmitEditing={this.nextPassword}
        />

        <Input
          ref={this.refPassword}
          onChangeText={this.onChangeTextPassword}
          value={this.state.password}
          placeholder={I18n.t('enter_your_password')}
          keyboardType="default"
          returnKeyType="next"
          onSubmitEditing={this.nextRePassword}
          secureTextEntry
        />

        <Input
          ref={this.refRePassword}
          onChangeText={this.onChangeTextRePassword}
          value={this.state.rePassword}
          placeholder={I18n.t('confirm_the_password')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          secureTextEntry
        />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('to_register')}
        />
      </Wrap>
    );
  }
}
