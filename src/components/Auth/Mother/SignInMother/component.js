import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonWhite from '../../../Base/Buttons/ButtonWhite';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import ButtonText from '../../../Base/Buttons/ButtonText';
import DropdownLanguage from '../../../Base/DropdownLanguage';
import Input from '../../../Base/Input';
import ModalUser from '../../../Base/Modals/ModalUser';

// Helpers
import NavigationService from '../../../../helpers/navigation';
import {checkEmail} from '../../../../helpers';

// Style
import {base} from './styles';

export default class SignInMother extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isVisible: false,
    };
  }

  onPressUser = () => {
    this.setState({
      isVisible: true,
    });
  };

  onPressClose = () => {
    this.setState({
      isVisible: false,
    });
  };

  onPressItem = _id => {
    const {user} = this.props;
    this.setState({
      isVisible: false,
    });

    const u = user.users.filter(e => e._id === _id);
    if (u.length > 0) {
      this.props.signInUser(u[0]);
    }
  };

  onChangeTextEmail = email => {
    this.setState({email});
  };

  onChangeTextPassword = password => {
    this.setState({password});
  };

  navigateForgotPassword = () => {
    NavigationService.navigate('ForgotPasswordMother');
  };

  navigateSignUpMother = () => {
    NavigationService.navigate('SignUpMother');
  };

  nextPassword = () => {
    this.password.focus();
  };

  check = () => {
    const {email, password} = this.state;
    if (!checkEmail(email)) {
      this.props.showToast(I18n.t('invalid_email_address'));
      return;
    }
    if (password.length < 6) {
      this.props.showToast(I18n.t('short_password'));
      return;
    }
    this.props.postSignIn({
      email,
      password,
      type: 1,
    });
  };

  ref = ref => {
    this.password = ref;
  };

  render() {
    const {user} = this.props;
    const {isVisible} = this.state;

    return (
      <Wrap style={base.wrap}>
        <DropdownLanguage />

        <Input
          onChangeText={this.onChangeTextEmail}
          value={this.state.email}
          placeholder={I18n.t('enter_your_email')}
          keyboardType="email-address"
          returnKeyType="next"
          email={user.users.length > 0}
          onPressUser={this.onPressUser}
          onSubmitEditing={this.nextPassword}
        />

        <Input
          ref={this.ref}
          onChangeText={this.onChangeTextPassword}
          value={this.state.password}
          placeholder={I18n.t('enter_your_password')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          secureTextEntry
        />

        <ButtonText
          onPress={this.navigateForgotPassword}
          titleColor="#9B9B9B"
          title={I18n.t('forgot_password')}
        />

        <View style={base.flex} />

        <ButtonWhite
          onPress={this.navigateSignUpMother}
          style={base.padding}
          gradientColors={['#DD7B27', '#DD7B27']}
          titleColor="#DD7B27"
          title={I18n.t('sign_up')}
        />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#DD7B27', '#F79C41']}
          titleColor="white"
          title={I18n.t('sign_in')}
        />
        <ModalUser
          user={user}
          isVisible={isVisible}
          onPressItem={this.onPressItem}
          onPressClose={this.onPressClose}
        />
      </Wrap>
    );
  }
}
