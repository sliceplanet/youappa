import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {postSignIn, signInUser} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postSignIn: data => dispatch(postSignIn(data)),
    signInUser: data => dispatch(signInUser(data)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
