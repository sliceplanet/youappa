import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator, setToast} from '../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
