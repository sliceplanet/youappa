import React from 'react';
import {View} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import Input from '../../../Base/Input';

// Helpers
import NavigationService from '../../../../helpers/navigation';

// API
import {apiPostPasswordReset} from '../../../../store/api/user';

// Style
import {base} from './styles';

export default class SavePasswordFriend extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      rePassword: '',
      resetcode: props.navigation.getParam('resetCode', ''),
    };
  }

  onChangeTextPassword = password => {
    this.setState({password});
  };

  onChangeTextRePassword = rePassword => {
    this.setState({rePassword});
  };

  nextRePassword = () => {
    this.rePassword.focus();
  };

  check = () => {
    const {password, rePassword, resetcode} = this.state;
    if (password !== rePassword) {
      this.props.showToast(I18n.t('passwords_do_not_match'));
      return;
    }
    if (password.length < 6) {
      this.props.showToast(I18n.t('short_password'));
      return;
    }
    this.props.setNetworkIndicator(true);
    apiPostPasswordReset({password, resetcode})
      .then(response => {
        console.log('apiPostPasswordReset -> ', response);
        this.props.setNetworkIndicator(false);
        NavigationService.reset('SignInFriend');
      })
      .catch(error => {
        console.log('apiPostPasswordReset -> ', error.response);
        this.props.showToast(error.response.data);
        this.props.setNetworkIndicator(false);
      });
  };

  refPassword = ref => {
    this.password = ref;
  };

  refRePassword = ref => {
    this.rePassword = ref;
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <View style={base.flex} />

        <Input
          ref={this.refPassword}
          onChangeText={this.onChangeTextPassword}
          value={this.state.password}
          placeholder={I18n.t('enter_your_password')}
          keyboardType="default"
          returnKeyType="next"
          onSubmitEditing={this.nextRePassword}
          secureTextEntry
        />

        <Input
          ref={this.refRePassword}
          onChangeText={this.onChangeTextRePassword}
          value={this.state.rePassword}
          placeholder={I18n.t('confirm_the_password')}
          keyboardType="default"
          returnKeyType="done"
          onSubmitEditing={this.check}
          secureTextEntry
        />

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="white"
          title={I18n.t('save')}
        />
      </Wrap>
    );
  }
}
