import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator, setToast} from '../../../../store/actions';

function mapDispatchToProps(dispatch) {
  return {
    setNetworkIndicator: data => dispatch(setNetworkIndicator(data)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(null, mapDispatchToProps)(component);
