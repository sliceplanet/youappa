import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {postSignUp} from '../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postSignUp: data => dispatch(postSignUp(data)),
    showToast: data => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
