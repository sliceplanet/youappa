import React from 'react';
import {View, Text} from 'react-native';
import I18n from 'i18n-js';

// Components
import Wrap from '../../../Base/Wrap';
import ButtonGradient from '../../../Base/Buttons/ButtonGradient';
import Input from '../../../Base/Input';

// Helpers
import {checkEmail} from '../../../../helpers';
import NavigationService from '../../../../helpers/navigation';

// API
import {apiGetForgotPassword} from '../../../../store/api/user';

// Style
import {base} from './styles';

export default class ForgotPasswordFriend extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
    };
  }

  onChangeTextEmail = email => {
    this.setState({email});
  };

  navigateSavePasswordFriend = () => {
    NavigationService.navigate('SavePasswordFriend');
  };

  check = () => {
    const {email} = this.state;
    if (!checkEmail(email)) {
      this.props.showToast(I18n.t('invalid_email_address'));
      return;
    }
    this.props.setNetworkIndicator(true);
    apiGetForgotPassword({email})
      .then(response => {
        console.log('apiGetForgotPassword -> ', response);
        this.props.setNetworkIndicator(false);
        NavigationService.navigate('SignInMother');
        this.props.showToast(I18n.t('deep_link'));
      })
      .catch(error => {
        console.log('apiGetForgotPassword -> ', error.response);
        this.props.showToast(error.response.data);
        this.props.setNetworkIndicator(false);
      });
  };

  render() {
    return (
      <Wrap style={base.wrap}>
        <View style={base.flex} />

        <Input
          style={base.margin}
          onChangeText={this.onChangeTextEmail}
          value={this.state.email}
          placeholder={I18n.t('enter_your_email')}
          keyboardType="email-address"
          returnKeyType="done"
          onSubmitEditing={this.check}
        />

        <Text style={base.text}>
          {I18n.t('a_message_will_be_sent_to_your_email')}
        </Text>

        <View style={base.flex} />

        <ButtonGradient
          onPress={this.check}
          style={base.padding}
          gradientColors={['#0C8A7E', '#069986']}
          titleColor="white"
          title={I18n.t('restore')}
        />
      </Wrap>
    );
  }
}
