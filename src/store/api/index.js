export const URL = 'http://95.216.198.112/api';
export const URI = 'http://95.216.198.112';

export const axiosConfig = {
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'x-bbox-app': 'v1',
  },
};

export const axiosConfigToken = token => {
  const headers = {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-bbox-app': 'v1',
      'x-tbbox': token,
    },
  };
  return headers;
};

export const getParams = patch => {
  let params = [];

  if (patch) {
    Object.keys(patch).forEach(prop => {
      params = [...params, `${prop}=${patch[prop]}`];
    });
  }

  if (params.length > 0) {
    return `?${params.join('&')}`;
  }

  return '';
};
