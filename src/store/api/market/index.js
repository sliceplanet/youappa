import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostQueryMarket(body) {
  return axios.post(
    `${URL}/market/query`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPostStatusQueryMarket(body) {
  return axios.get(
    `${URL}/market/query/${body.patch.queryId}/status`,
    axiosConfigToken(body.token),
  );
}
