import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPutUpdatePregnancy(body) {
  return axios.put(
    `${URL}/pregnancy/update`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiGetCurrentPregnancy(body) {
  return axios.get(`${URL}/pregnancy/current`, axiosConfigToken(body.token));
}

export function apiGetPreviousPregnancy(body) {
  return axios.get(`${URL}/pregnancy/previous`, axiosConfigToken(body.token));
}

export function apiGetAllPregnancy(body) {
  return axios.get(`${URL}/pregnancy/all`, axiosConfigToken(body.token));
}
