import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPutPushToken(body) {
  return axios.put(
    `${URL}/push_token`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeletePushToken(body) {
  return axios.delete(`${URL}/push_token`, axiosConfigToken(body.token));
}
