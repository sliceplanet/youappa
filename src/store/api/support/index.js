import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostPrivateReport(body) {
  return axios.post(
    `${URL}/report/private`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export default {apiPostPrivateReport};
