import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiGetAddSubscriber(body) {
  return axios.get(
    `${URL}/subscriber/add/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetRemoveSubscriber(body) {
  return axios.get(
    `${URL}/subscriber/remove/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetSearchSubscriber(body) {
  return axios.get(`${URL}/subscriber/search`, axiosConfigToken(body.token));
}
