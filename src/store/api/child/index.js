import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiGetMyChild(body) {
  return axios.get(`${URL}/child/my`, axiosConfigToken(body.token));
}

export function apiPostAddChild(body) {
  return axios.post(
    `${URL}/child/add`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutUpdateChild(body) {
  return axios.put(
    `${URL}/child/update/${body.patch.childId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteChild(body) {
  return axios.delete(
    `${URL}/child/delete/${body.patch.childId}`,
    axiosConfigToken(body.token),
  );
}
