import axios from 'axios';

import {URL, axiosConfigToken, getParams} from '../index';

export function apiGetMyEvent(body) {
  return axios.get(
    `${URL}/event/my${getParams(body.patch)}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetFeedEvent(body) {
  return axios.get(
    `${URL}/event/feed${getParams(body.patch)}`,
    axiosConfigToken(body.token),
  );
}

export function apiPostCreateEvent(body) {
  return axios.post(
    `${URL}/event/create`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutEditEvent(body) {
  return axios.put(
    `${URL}/event/edit/${body.patch.eventId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteEvent(body) {
  return axios.delete(
    `${URL}/event/delete/${body.patch.eventId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetEvent(body) {
  return axios.get(
    `${URL}/event/get/${body.patch.eventId}`,
    axiosConfigToken(body.token),
  );
}
