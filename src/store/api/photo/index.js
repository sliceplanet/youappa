import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostUploadPhoto(body) {
  return axios.post(
    `${URL}/photo/upload`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPostUploadPhotoMulty(body) {
  let result = [];
  body.data.forEach(e => {
    const promise = new Promise((resolve, reject) => {
      axios
        .post(`${URL}/photo/upload`, e, axiosConfigToken(body.token))
        .then(require => {
          resolve(require.data.data._id);
        })
        .catch(ex => reject(ex));
    });
    result = [...result, promise];
  });

  return Promise.all(result);
}

export function apiDeletePhoto(body) {
  return axios.delete(
    `${URL}/photo/delete/${body.patch.photoId}`,
    axiosConfigToken(body.token),
  );
}
