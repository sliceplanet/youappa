import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPutLikeComment(body) {
  return axios.put(
    `${URL}/comment/like/${body.patch.commentId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutUnlikeComment(body) {
  return axios.put(
    `${URL}/comment/unlike/${body.patch.commentId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutLikePost(body) {
  return axios.put(
    `${URL}/post/like/${body.patch.postId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutUnlikePost(body) {
  return axios.put(
    `${URL}/post/unlike/${body.patch.postId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}
