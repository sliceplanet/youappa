import axios from 'axios';

import {URL, axiosConfigToken, axiosConfig} from '../index';

export function apiGetForgotPassword(body) {
  return axios.get(
    `${URL}/user/password/resetcode?email=${body.email}`,
    axiosConfig,
  );
}

export function apiPushTest(body) {
  return axios.get(`${URL}/push_token/test`, axiosConfigToken(body.token));
}

export function apiPostSignUp(body) {
  return axios.post(`${URL}/user/register`, body, axiosConfig);
}

export function apiPostSignIn(body) {
  return axios.post(`${URL}/user/login`, body, axiosConfig);
}

export function apiPostPasswordReset(body) {
  return axios.post(`${URL}/user/password/reset`, body, axiosConfig);
}

export function apiGetMe(body) {
  return axios.get(`${URL}/user/me`, axiosConfigToken(body.token));
}

export function apiLogout(body) {
  return axios.get(`${URL}/user/logout`, axiosConfigToken(body.token));
}

export function apiPostUpdate(body) {
  return axios.post(
    `${URL}/user/update`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiGetUser(body) {
  return axios.get(
    `${URL}/user/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetWishlistsUser(body) {
  return axios.get(
    `${URL}/user/${body.patch.userId}/wishlists`,
    axiosConfigToken(body.token),
  );
}
