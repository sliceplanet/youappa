import axios from 'axios';

import {URL, axiosConfigToken, getParams} from '../index';

export function apiPostCreatePost(body) {
  return axios.post(
    `${URL}/post/create`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeletePost(body) {
  return axios.delete(
    `${URL}/post/delete/${body.patch.postId}`,
    axiosConfigToken(body.token),
  );
}

export function apiDeletePhotoDeletePost(body) {
  return axios.delete(
    `${URL}/post/delete/${body.patch.postId}/photo`,
    axiosConfigToken(body.token),
  );
}

export function apiPutEditPost(body) {
  return axios.put(
    `${URL}/post/edit/${body.patch.postId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutPhotoEditPost(body) {
  return axios.put(
    `${URL}/post/edit/${body.patch.postId}/photo`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiGetPrivatePost(body) {
  return axios.get(`${URL}/post/private`, axiosConfigToken(body.token));
}

export function apiGetDiaryPost(body) {
  return axios.get(`${URL}/post/diary`, axiosConfigToken(body.token));
}

export function apiGetFeedPost(body) {
  return axios.get(`${URL}/post/feed`, axiosConfigToken(body.token));
}

export function apiGetMyPost(body) {
  return axios.get(
    `${URL}/post/my${getParams(body.patch)}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetUserPost(body) {
  return axios.get(
    `${URL}/post/user/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetPublicUserPost(body) {
  return axios.get(
    `${URL}/post/user/${body.patch.userId}/public`,
    axiosConfigToken(body.token),
  );
}

export function apiGetPost(body) {
  return axios.get(
    `${URL}/post/post/${body.patch.postId}`,
    axiosConfigToken(body.token),
  );
}
