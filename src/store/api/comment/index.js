import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostCreateComment(body) {
  return axios.post(
    `${URL}/post/${body.patch.postId}/comment/create`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPostEditComment(body) {
  return axios.post(
    `${URL}/comment/${body.patch.commentId}/edit`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteComment(body) {
  return axios.delete(
    `${URL}/comment/${body.patch.commentId}/delete`,
    axiosConfigToken(body.token),
  );
}

export function apiGetCommentPost(body) {
  return axios.get(
    `${URL}/post/${body.patch.postId}/comment`,
    axiosConfigToken(body.token),
  );
}
