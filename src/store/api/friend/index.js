import axios from 'axios';

import {URL, axiosConfigToken, getParams} from '../index';

export function apiGetFriend(body) {
  return axios.get(`${URL}/friend/get`, axiosConfigToken(body.token));
}

export function apiGetRequestFriend(body) {
  return axios.get(
    `${URL}/friend/request/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetAcceptFriend(body) {
  return axios.get(
    `${URL}/friend/accept/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetRemoveFriend(body) {
  return axios.get(
    `${URL}/friend/remove/${body.patch.userId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetIncomingFriend(body) {
  return axios.get(`${URL}/friend/incoming`, axiosConfigToken(body.token));
}

export function apiGetOutgoingFriend(body) {
  return axios.get(`${URL}/friend/outgoing`, axiosConfigToken(body.token));
}

export function apiGetSearchFriend(body) {
  return axios.get(
    `${URL}/friend/search${getParams(body.patch)}`,
    axiosConfigToken(body.token),
  );
}
