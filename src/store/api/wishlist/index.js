import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostAddWishlist(body) {
  return axios.post(
    `${URL}/wishlist/add`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutUpdateWishlist(body) {
  return axios.put(
    `${URL}/wishlist/update/${body.patch.wishlistId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutAddProductWishlist(body) {
  return axios.put(
    `${URL}/wishlist/${body.patch.wishlistId}/product/add`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutProductWishlist(body) {
  return axios.put(
    `${URL}/wishlist/${body.patch.wishlistId}/product/${body.patch.productId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteProductWishlist(body) {
  return axios.delete(
    `${URL}/wishlist/${body.patch.wishlistId}/product/${body.patch.productId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetMyWishlist(body) {
  return axios.get(`${URL}/wishlist/my`, axiosConfigToken(body.token));
}

export function apiDeleteWishlist(body) {
  return axios.delete(
    `${URL}/wishlist/delete/${body.patch.wishlistId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetwishlistShareIdsharesWishlist(body) {
  return axios.get(
    `${URL}/wishlist/shares/${body.patch.wishlistShareId}`,
    axiosConfigToken(body.token),
  );
}

export function apiGetSharesWishlistIdWishlist(body) {
  return axios.get(
    `${URL}/wishlist/${body.patch.wishlistId}/shares`,
    axiosConfigToken(body.token),
  );
}

export function apiPostShareWishlistIdWishlist(body) {
  return axios.post(
    `${URL}/wishlist/${body.patch.wishlistId}/share`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutWishlistShareIdSharesWishlistIdWishlist(body) {
  return axios.put(
    `${URL}/wishlist/${body.patch.wishlistId}}/shares/${body.patch.wishlistShareId}`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteWishlistShareIdSharesWishlistIdWishlist(body) {
  return axios.delete(
    `${URL}/wishlist/${body.patch.wishlistId}}/shares/${body.patch.wishlistShareId}`,
    axiosConfigToken(body.token),
  );
}
