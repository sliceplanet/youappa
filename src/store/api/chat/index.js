import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostCreateChat(body) {
  return axios.post(
    `${URL}/chat/create`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiDeleteChat(body) {
  return axios.delete(
    `${URL}/chat/${body.patch.chatId}/delete`,
    axiosConfigToken(body.token),
  );
}

export function apiPostSendChat(body) {
  return axios.post(
    `${URL}/chat/${body.patch.chatId}/send`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiPutEditChat(body) {
  return axios.put(
    `${URL}/chat/${body.patch.chatId}/edit`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiGetMessagesChat(body) {
  return axios.get(
    `${URL}/chat/${body.patch.chatId}/messages`,
    axiosConfigToken(body.token),
  );
}
