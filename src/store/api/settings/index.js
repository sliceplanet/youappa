import axios from 'axios';

import {URL, axiosConfigToken} from '../index';

export function apiPostUpdateSettings(body) {
  return axios.post(
    `${URL}/settings/update`,
    body.data,
    axiosConfigToken(body.token),
  );
}

export function apiGetSettings(body) {
  return axios.get(`${URL}/settings`, axiosConfigToken(body.token));
}
