export function getFriend(data) {
  return {
    type: 'getFriend',
    data,
  };
}

export function getRequestFriend(data) {
  return {
    type: 'getRequestFriend',
    data,
  };
}

export function getAcceptFriend(data) {
  return {
    type: 'getAcceptFriend',
    data,
  };
}

export function getRemoveFriend(data) {
  return {
    type: 'getRemoveFriend',
    data,
  };
}

export function getIncomingFriend(data) {
  return {
    type: 'getIncomingFriend',
    data,
  };
}

export function getOutgoingFriend(data) {
  return {
    type: 'getOutgoingFriend',
    data,
  };
}

export function getSearchFriend(data) {
  return {
    type: 'getSearchFriend',
    data,
  };
}
