export function postUploadPhoto(data) {
  return {
    type: 'postUploadPhoto',
    data,
  };
}

export function deletePhoto(data) {
  return {
    type: 'deletePhoto',
    data,
  };
}
