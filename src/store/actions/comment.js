export function postCreateComment(data) {
  return {
    type: 'postCreateComment',
    data,
  };
}

export function postEditComment(data) {
  return {
    type: 'postEditComment',
    data,
  };
}

export function deleteComment(data) {
  return {
    type: 'deleteComment',
    data,
  };
}

export function getCommentPost(data) {
  return {
    type: 'getCommentPost',
    data,
  };
}
