export function postCreatePost(data) {
  return {
    type: 'postCreatePost',
    data,
  };
}

export function deletePost(data) {
  return {
    type: 'deletePost',
    data,
  };
}

export function deletePhotoDeletePost(data) {
  return {
    type: 'deletePhotoDeletePost',
    data,
  };
}

export function putEditPost(data) {
  return {
    type: 'putEditPost',
    data,
  };
}

export function putPhotoEditPost(data) {
  return {
    type: 'putPhotoEditPost',
    data,
  };
}

export function getPrivatePost(data) {
  return {
    type: 'getPrivatePost',
    data,
  };
}

export function getDiaryPost(data) {
  return {
    type: 'getDiaryPost',
    data,
  };
}

export function getFeedPost(data) {
  return {
    type: 'getFeedPost',
    data,
  };
}

export function getMyPost(data) {
  return {
    type: 'getMyPost',
    data,
  };
}

export function getUserPost(data) {
  return {
    type: 'getUserPost',
    data,
  };
}

export function getPublicUserPost(data) {
  return {
    type: 'getPublicUserPost',
    data,
  };
}

export function getPost(data) {
  return {
    type: 'getPost',
    data,
  };
}
