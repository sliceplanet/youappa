export function getSettings(data) {
  return {
    type: 'getSettings',
    data,
  };
}

export function postUpdateSettings(data) {
  return {
    type: 'postUpdateSettings',
    data,
  };
}
