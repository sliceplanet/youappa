export function getMyEvent(data) {
  return {
    type: 'getMyEvent',
    data,
  };
}

export function getFeedEvent(data) {
  return {
    type: 'getFeedEvent',
    data,
  };
}

export function postCreateEvent(data) {
  return {
    type: 'postCreateEvent',
    data,
  };
}

export function putEditEvent(data) {
  return {
    type: 'putEditEvent',
    data,
  };
}

export function deleteEvent(data) {
  return {
    type: 'deleteEvent',
    data,
  };
}

export function getEvent(data) {
  return {
    type: 'getEvent',
    data,
  };
}
