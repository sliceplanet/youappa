export function setNetworkIndicator(data) {
  return {
    type: 'networkIndicator',
    data,
  };
}

export function setI18n(data) {
  return {
    type: 'i18n',
    data,
  };
}

export function setToast(data) {
  return {
    type: 'toast',
    data,
  };
}

export function setIndex(data) {
  return {
    type: 'setIndex',
    data,
  };
}

export function setCache(data) {
  return {
    type: 'cache',
    data,
  };
}

export function removeCache(data) {
  return {
    type: 'removeCache',
    data,
  };
}

export function mainMother(data) {
  return {
    type: 'mainMother',
    data,
  };
}
