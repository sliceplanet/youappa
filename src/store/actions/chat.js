export function postCreateChat(data) {
  return {
    type: 'postCreateChat',
    data,
  };
}

export function deleteChat(data) {
  return {
    type: 'deleteChat',
    data,
  };
}

export function postSendChat(data) {
  return {
    type: 'postSendChat',
    data,
  };
}

export function putEditChat(data) {
  return {
    type: 'putEditChat',
    data,
  };
}

export function getMessagesChat(data) {
  return {
    type: 'getMessagesChat',
    data,
  };
}
