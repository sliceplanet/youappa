export function setPush(data) {
  return {
    type: 'push',
    data,
  };
}

export function putPushToken(data) {
  return {
    type: 'putPushToken',
    data,
  };
}

export function deletePushToken(data) {
  return {
    type: 'deletePushToken',
    data,
  };
}
