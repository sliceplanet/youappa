export function postPrivateReport(data) {
  return {
    type: 'postPrivateReport',
    data,
  };
}

export default {postPrivateReport};
