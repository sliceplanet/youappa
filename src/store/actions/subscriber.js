export function getAddSubscriber(data) {
  return {
    type: 'getAddSubscriber',
    data,
  };
}

export function getRemoveSubscriber(data) {
  return {
    type: 'getRemoveSubscriber',
    data,
  };
}

export function getSearchSubscriber(data) {
  return {
    type: 'getSearchSubscriber',
    data,
  };
}
