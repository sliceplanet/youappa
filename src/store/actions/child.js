export function getMyChild(data) {
  return {
    type: 'getMyChild',
    data,
  };
}

export function postAddChild(data) {
  return {
    type: 'postAddChild',
    data,
  };
}

export function deleteChild(data) {
  return {
    type: 'deleteChild',
    data,
  };
}

export function putUpdateChild(data) {
  return {
    type: 'putUpdateChild',
    data,
  };
}
