export function postAddWishlist(data) {
  return {
    type: 'postAddWishlist',
    data,
  };
}

export function reducerAddWishlist(data) {
  return {
    type: 'putAddWishlist',
    data,
  };
}

export function putUpdateWishlist(data) {
  return {
    type: 'putUpdateWishlist',
    data,
  };
}

export function putAddProductWishlist(data) {
  return {
    type: 'putAddProductWishlist',
    data,
  };
}

export function putProductWishlist(data) {
  return {
    type: 'putProductWishlist',
    data,
  };
}

export function putAddWishlist(data) {
  return {
    type: 'putAddWishlist',
    data,
  };
}

export function deleteProductWishlist(data) {
  return {
    type: 'deleteProductWishlist',
    data,
  };
}

export function getMyWishlist(data) {
  return {
    type: 'getMyWishlist',
    data,
  };
}

export function deleteWishlist(data) {
  return {
    type: 'deleteWishlist',
    data,
  };
}

export function wishAdd(data) {
  return {
    type: 'wishAdd',
    data,
  };
}

export function wishUpdate(data) {
  return {
    type: 'wishUpdate',
    data,
  };
}
