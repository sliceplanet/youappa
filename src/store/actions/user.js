export function postSignUp(data) {
  return {
    type: 'postSignUp',
    data,
  };
}

export function postSignIn(data) {
  return {
    type: 'postSignIn',
    data,
  };
}

export function signInUser(data) {
  return {
    type: 'signInUser',
    data,
  };
}

export function logout(data) {
  return {
    type: 'logout',
    data,
  };
}

export function postUpdate(data) {
  return {
    type: 'postUpdate',
    data,
  };
}

export function getMe(data) {
  return {
    type: 'getMe',
    data,
  };
}
