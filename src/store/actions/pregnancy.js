export function putUpdatePregnancy(data) {
  return {
    type: 'putUpdatePregnancy',
    data,
  };
}

export function getCurrentPregnancy(data) {
  return {
    type: 'getCurrentPregnancy',
    data,
  };
}

export function getPreviousPregnancy(data) {
  return {
    type: 'getPreviousPregnancy',
    data,
  };
}

export function getAllPregnancy(data) {
  return {
    type: 'getAllPregnancy',
    data,
  };
}
