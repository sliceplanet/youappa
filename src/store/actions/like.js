export function putLikeComment(data) {
  return {
    type: 'putLikeComment',
    data,
  };
}

export function putUnlikeComment(data) {
  return {
    type: 'putUnlikeComment',
    data,
  };
}

export function putLikePost(data) {
  return {
    type: 'putLikePost',
    data,
  };
}

export function putUnlikePost(data) {
  return {
    type: 'putUnlikePost',
    data,
  };
}
