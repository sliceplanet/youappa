const initialState = {};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'updatePregnancy':
    case 'currentPregnancy': {
      return action.data;
    }
    case 'previousPregnancy': {
      return state;
    }
    case 'allPregnancy': {
      return state;
    }
    default:
      return state;
  }
}
