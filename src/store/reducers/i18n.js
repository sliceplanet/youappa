import * as RNLocalize from 'react-native-localize';

const fallback = {languageTag: 'en', isRTL: false};
const {languageTag} =
  RNLocalize.findBestAvailableLanguage(['en', 'es', 'ru']) || fallback;
const initialState = languageTag;

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'i18n': {
      return action.data;
    }
    default:
      return state;
  }
}
