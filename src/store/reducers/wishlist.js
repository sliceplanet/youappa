const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'myWishlist': {
      return action.data;
    }
    case 'addProductWishlist': {
      const {wishlistId} = action.data;
      let array = state.filter(e => e._id !== wishlistId);
      const item = state.filter(e => e._id === wishlistId)[0];

      item.products = [...item.products, action.data];
      array = [...array, item];
      return array;
    }
    case 'productWishlist': {
      const wishlists = state.filter(e => e._id !== action.data.wishlistId);
      const wishlist = state.filter(e => e._id === action.data.wishlistId);

      if (wishlist.length > 0) {
        const products = wishlist[0].products.filter(
          e => e._id !== action.data._id,
        );
        const product = wishlist[0].products.filter(
          e => e._id === action.data._id,
        );

        if (product.length > 0) {
          return [
            ...wishlists,
            {
              ...wishlist,
              products: [...products, action.data],
            },
          ];
        }
      }
      return state;
    }
    case 'putAddWishlist': {
      console.log(action.data);
      return [...state, action.data];
    }
    case 'delProductWishlist': {
      const s = state.filter(e => e._id !== action.data.wishlist);
      const w = state.filter(e => e._id === action.data.wishlist)[0];
      w.products = w.products.filter(e => e._id !== action.data._id);

      return [...s, w];
    }
    case 'updateWishlist': {
      const {data} = action;
      delete data.child;
      const s = state.filter(e => e._id !== action.data._id);
      let w = state.filter(e => e._id === action.data._id)[0];
      w = {
        ...w,
        ...data,
      };

      return [...s, w];
    }
    case 'delWishlist': {
      return [...state.filter(e => e._id !== action.data.patch.wishlistId)];
    }

    default:
      return state;
  }
}
