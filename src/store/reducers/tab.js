const initialState = 0;

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'setIndex': {
      return action.data;
    }
    case 'logoutUser': {
      return 0;
    }
    default:
      return state;
  }
}
