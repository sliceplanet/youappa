import moment from 'moment';

const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'getCommentPost': {
      return [...state, action.data];
    }
    case 'createComment': {
      const comments = state.filter(
        e => e.data.filter(k => k.post === action.data.post).length === 0,
      );
      const comment = state.filter(
        e => e.data.filter(k => k.post === action.data.post).length > 0,
      );

      if (comment.length > 0) {
        comment[0].data = [...comment[0].data, action.data].sort(
          (a, b) =>
            moment(b.createdAt).valueOf() - moment(a.createdAt).valueOf(),
        );
        return [...comment, ...comments];
      }
      return [{more: false, data: action.data}, ...comments].sort(
        (a, b) => moment(b.createdAt).valueOf() - moment(a.createdAt).valueOf(),
      );
    }
    case 'likeComment':
    case 'unlikeComment': {
      const s = state;
      const {_id} = action.data;
      const index = s.findIndex(
        e => e.data.filter(k => k._id === _id).length > 0,
      );
      if (index !== -1) {
        const item = s[index];
        const i = item.data.findIndex(e => e._id === _id);
        item.data[i] = action.data;
        s[index] = item;
      }
      return [...s];
    }
    default:
      return state;
  }
}
