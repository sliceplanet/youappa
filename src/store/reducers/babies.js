const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'baby': {
      return [...state, action.data];
    }
    case 'babies': {
      return action.data;
    }
    case 'updateBaby': {
      const babies = state.filter(e => e._id !== action.data._id);
      return [...babies, action.data];
    }
    case 'removeBaby': {
      return state.filter(e => e._id !== action.data.childId);
    }
    default:
      return state;
  }
}
