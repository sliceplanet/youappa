import {combineReducers} from 'redux';

import networkIndicator from './networkIndicator';
import toast from './toast';
import i18n from './i18n';
import cache from './cache';

import babies from './babies';
import comments from './comments';
import friends from './friends';
import events from './events';
import followers from './followers';
import post from './post';
import pregnancy from './pregnancy';
import push from './push';
import settings from './settings';
import user from './user';
import wish from './wish';
import wishlist from './wishlist';

export default combineReducers({
  networkIndicator,
  toast,
  i18n,

  babies,
  cache,
  comments,
  friends,
  events,
  followers,
  post,
  pregnancy,
  push,
  settings,
  user,
  wish,
  wishlist,
});
