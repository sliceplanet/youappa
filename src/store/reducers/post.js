const initialState = {
  data: [],
  more: false,
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'createPost': {
      return {
        ...state,
        data: [...state.data, action.data],
      };
    }
    case 'post': {
      const {data, more} = action.data;
      if (state.data.length > 0)
        return {
          data: [...state.data, ...data],
          more,
        };
      return {
        data,
        more,
      };
    }
    default:
      return state;
  }
}
