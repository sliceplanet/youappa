const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'push': {
      return [...state, action.data];
    }
    default:
      return state;
  }
}
