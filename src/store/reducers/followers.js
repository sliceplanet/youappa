const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'follower': {
      return [...state, action.data];
    }
    case 'followers': {
      return state;
    }
    default: {
      return state;
    }
  }
}
