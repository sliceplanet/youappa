const initialState = {
  notify: false,
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'settings': {
      return {
        ...state,
        ...action.data.data,
      };
    }
    case 'updateSettings': {
      return {
        ...state,
        ...action.data,
      };
    }
    default:
      return state;
  }
}
