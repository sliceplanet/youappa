const initialState = {
  token: '',
  users: [],
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'signUpUser':
    case 'signInUser': {
      const u = state.users.filter(e => e.email !== action.data.email);
      const {token} = action.data;

      return {
        users: [...u, action.data],
        token,
      };
    }
    case 'updateUser': {
      const u = state.users.filter(e => e.email !== action.data.data.email);

      const {token} = action.data;
      return {
        users: [...u, {token, ...action.data.data}],
        token,
      };
    }
    case 'meUser': {
      const u = state.users.filter(e => e.email !== action.data.email);
      const user = state.users.filter(e => e.email === action.data.email)[0];

      return {
        ...state,
        users: [...u, {...user, ...action.data}],
      };
    }
    case 'logoutUser': {
      return {
        ...state,
        token: '',
      };
    }
    default:
      return state;
  }
}
