const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'myEvent': {
      return action.data;
    }
    case 'createEvent': {
      return [...state, action.data];
    }
    default:
      return state;
  }
}
