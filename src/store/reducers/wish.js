const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'wishAdd': {
      return action.data;
    }
    case 'wishUpdate': {
      const {modalType, title} = action.data;
      const s = state;
      for (let i = 0; i < state.length; i++) {
        if (state[i].type === modalType) {
          s[i].title = title;
        }
      }
      return s;
    }
    default:
      return state;
  }
}
