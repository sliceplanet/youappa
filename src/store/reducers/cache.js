const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'cache': {
      return [action.data, ...state];
    }
    case 'removeCache': {
      return state.filter(e => e.uri !== action.data.uri);
    }
    default:
      return state;
  }
}
