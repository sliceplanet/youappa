import {put} from 'redux-saga/effects';

import * as API from '../../api/wishlist';

export function* postAddWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostAddWishlist(action.data);
    console.log('apiPostAddWishlist -> ', response);

    yield put({
      type: 'putAddWishlist',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostAddWishlist -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putUpdateWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutUpdateWishlist(action.data);
    console.log('apiPutUpdateWishlist -> ', response);

    yield put({
      type: 'updateWishlist',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutUpdateWishlist -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putAddProductWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutAddProductWishlist(action.data);
    console.log('apiPutAddProductWishlist -> ', response);

    yield put({
      type: 'addProductWishlist',
      data: {...response.data.data, ...action.data.patch},
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    // const { message } = error.response.data;
    console.log('apiPutAddProductWishlist -> ', error);
    // yield put({ type: "toast", data: { message } });
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putProductWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutProductWishlist(action.data);
    console.log('apiPutProductWishlist -> ', response);

    yield put({
      type: 'productWishlist',
      data: {...response.data.data, ...action.data.patch},
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    // const { message } = error.response.data;
    console.log('apiPutProductWishlist -> ', error);
    // yield put({ type: "toast", data: { message } });
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteProductWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteProductWishlist(action.data);
    console.log('apiDeleteProductWishlist -> ', response);

    yield put({
      type: 'delProductWishlist',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    // const {message} = error.response.data;
    console.log('apiDeleteProductWishlist -> ', error);
    // yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getMyWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetMyWishlist(action.data);
    console.log('start apiGetMyWishlist');
    console.log('apiGetMyWishlist -> ', response);
    console.log('stop apiGetMyWishlist');

    yield put({
      type: 'myWishlist',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetMyWishlist -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteWishlist(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteWishlist(action.data);
    console.log('apiDeleteWishlist -> ', response);

    yield put({
      type: 'delWishlist',
      data: action.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeleteWishlist -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
