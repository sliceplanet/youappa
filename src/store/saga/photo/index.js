import {put} from 'redux-saga/effects';

import * as API from '../../api/photo';

export function* postUploadPhoto(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostUploadPhoto(action.data);
    console.log('apiPostUploadPhoto -> ', response);

    yield put({
      type: 'uploadPhoto',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostUploadPhoto -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deletePhoto(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeletePhoto(action.data);
    console.log('apiDeletePhoto -> ', response);

    yield put({
      type: 'deletePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeletePhoto -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
