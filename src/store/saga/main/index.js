import {put} from 'redux-saga/effects';

import * as UserApi from '../../api/user';
import * as ChildApi from '../../api/child';
import * as SettingsApi from '../../api/settings';
import * as WishlistApi from '../../api/wishlist';
import * as FriendApi from '../../api/friend';
import * as PostApi from '../../api/post';
import * as EventApi from '../../api/event';

export function* mother(action) {
  try {
    yield put({type: 'networkIndicator', data: true});

    const {token} = action.data;

    const me = yield UserApi.apiGetMe({token});
    yield put({
      type: 'meUser',
      data: me.data.data,
    });
    console.log('Me -> ', me);

    const child = yield ChildApi.apiGetMyChild(action.data);
    yield put({
      type: 'babies',
      data: child.data.data,
    });
    console.log('Child -> ', child);

    const settings = yield SettingsApi.apiGetSettings(action.data);
    yield put({
      type: 'settings',
      data: {
        token,
        data: settings.data.data,
      },
    });
    console.log('Settings -> ', settings);

    const wishlist = yield WishlistApi.apiGetMyWishlist(action.data);
    yield put({
      type: 'myWishlist',
      data: wishlist.data.data,
    });
    console.log('Wishlist -> ', wishlist);

    const friend = yield FriendApi.apiGetFriend(action.data);
    yield put({
      type: 'friends',
      data: friend.data.data,
    });
    console.log('Friend -> ', friend);

    const post = yield PostApi.apiGetMyPost(action.data);
    const event = yield EventApi.apiGetMyEvent(action.data);
    yield put({
      type: 'post',
      data: post.data,
    });

    yield put({
      type: 'myEvent',
      data: event.data.data,
    });
    console.log('Post -> ', post);
    console.log('Event -> ', event);

    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    console.log(error);
    console.log('main -> ', error.response);
    // yield put({
    //   type: 'toast',
    //   data: error.response.data.message,
    // });
    yield put({type: 'networkIndicator', data: false});
  }
}

export default {mother};
