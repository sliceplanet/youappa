import {put} from 'redux-saga/effects';

import * as API from '../../api/chat';

export function* postCreateChat(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostCreateChat(action.data);
    console.log('apiPostCreateChat -> ', response);

    yield put({
      type: 'createChat',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostCreateChat -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteChat(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteChat(action.data);
    console.log('apiDeleteChat -> ', response);

    yield put({
      type: 'deleteChat',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeleteChat -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postSendChat(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostSendChat(action.data);
    console.log('apiPostSendChat -> ', response);

    yield put({
      type: 'sendChat',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostSendChat -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putEditChat(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutEditChat(action.data);
    console.log('apiPutEditChat -> ', response);

    yield put({
      type: 'editChat',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutEditChat -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getMessagesChat(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetMessagesChat(action.data);
    console.log('apiGetMessagesChat -> ', response);

    yield put({
      type: 'messagesChat',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetMessagesChat -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
