import {put} from 'redux-saga/effects';

import * as API from '../../api/support';

export function* postPrivateReport(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostPrivateReport(action.data);
    console.log('apiPostPrivateReport -> ', response);

    yield put({
      type: 'privateReport',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostPrivateReport -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export default {postPrivateReport};
