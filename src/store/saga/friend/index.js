import {put} from 'redux-saga/effects';

import * as API from '../../api/friend';

export function* getFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetFriend(action.data);
    console.log('apiGetFriend -> ', response);
    yield put({
      type: 'friends',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getRequestFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetRequestFriend(action.data);
    console.log('apiGetRequestFriend -> ', response);
    yield put({
      type: 'requestFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetRequestFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getAcceptFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetAcceptFriend(action.data);
    console.log('apiGetAcceptFriend -> ', response);
    yield put({
      type: 'acceptFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetAcceptFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getRemoveFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetRemoveFriend(action.data);
    console.log('getRemoveFriend -> ', response);
    yield put({
      type: 'removeFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('getRemoveFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getIncomingFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetIncomingFriend(action.data);
    console.log('getIncomingFriend -> ', response);
    yield put({
      type: 'incomingFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('getIncomingFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getOutgoingFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetOutgoingFriend(action.data);
    console.log('getOutgoingFriend -> ', response);
    yield put({
      type: 'outgoingFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('getOutgoingFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getSearchFriend(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetSearchFriend(action.data);
    console.log('getSearchFriend -> ', response);
    yield put({
      type: 'searchFriend',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('getSearchFriend -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}
