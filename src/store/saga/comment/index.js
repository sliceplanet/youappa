import {put} from 'redux-saga/effects';

import * as API from '../../api/comment';

export function* postCreateComment(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostCreateComment(action.data);
    console.log('apiPostCreateComment -> ', response);

    yield put({
      type: 'createComment',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostCreateComment -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postEditComment(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostEditComment(action.data);
    console.log('apiPostEditComment -> ', response);

    yield put({
      type: 'editComment',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostEditComment -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteComment(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteComment(action.data);
    console.log('apiDeleteComment -> ', response);

    yield put({
      type: 'deleteComment',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeleteComment -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getCommentPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetCommentPost(action.data);
    console.log('apiGetCommentPost -> ', response);

    yield put({
      type: 'commentPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetCommentPost -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}
