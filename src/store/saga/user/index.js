import {put} from 'redux-saga/effects';

import * as API from '../../api/user';

export function* postSignUp(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostSignUp(action.data);
    console.log('apiPostSignUp -> ', response);

    const {token} = response.data.data;
    const response2 = yield API.apiGetMe({token});
    console.log('apiGetMe -> ', response2);

    yield put({
      type: 'signUpUser',
      data: {
        token,
        ...response2.data.data,
      },
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostSignUp -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postSignIn(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const {email, password, type} = action.data;
    const response = yield API.apiPostSignIn({email, password});
    console.log('apiPostSignIn -> ', response);

    const {token} = response.data.data;
    const response2 = yield API.apiGetMe({token});

    console.log('apiGetMe -> ', response2);

    yield put({
      type: 'signInUser',
      data: {
        token,
        ...response2.data.data,
        type,
      },
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    console.log(error);
    const {message} = error.response.data;
    console.log('apiPostSignIn -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postUpdate(action) {
  try {
    console.log(action.data);
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostUpdate(action.data);
    console.log('apiPostUpdate -> ', response);

    const {token} = action.data;
    const response2 = yield API.apiGetMe({token});
    console.log('apiGetMe -> ', response2);

    yield put({
      type: 'updateUser',
      data: {
        token,
        data: response2.data.data,
      },
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostUpdate -> ', error);
    console.log('apiPostUpdate -> ', message);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getMe(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const {token} = action.data;
    const response = yield API.apiGetMe({token});
    console.log('apiGetMe -> ', response);

    yield put({
      type: 'updateUser',
      data: {
        token,
        data: response.data.data,
      },
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiLogout -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* logout() {
  yield put({type: 'networkIndicator', data: true});
  yield put({type: 'logoutUser'});
  yield put({type: 'networkIndicator', data: false});
}
