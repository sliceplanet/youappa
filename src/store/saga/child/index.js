import {put} from 'redux-saga/effects';

import * as API from '../../api/child';
import {URI} from '../../api';

export function* getMyChild(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetMyChild(action.data);
    console.log('apiGetMyChild -> ', response);

    yield put({
      type: 'babies',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetMyChild -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postAddChild(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostAddChild(action.data);
    console.log('apiPostAddChild -> ', response);

    yield put({
      type: 'baby',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPostAddChild -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putUpdateChild(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutUpdateChild(action.data);
    console.log('apiPutUpdateChild -> ', response);

    if (response.data.data.usedPhotoSource === 'url') {
      const data = {
        uri: `${URI}${response.data.data.photoUrl}`,
      };
      yield put({
        type: 'removeCache',
        data,
      });
    }
    yield put({
      type: 'updateBaby',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutUpdateChild -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteChild(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteChild(action.data);
    console.log('apiDeleteChild -> ', response);

    yield put({
      type: 'removeBaby',
      data: action.data.patch,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeleteChild -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}
