import {put} from 'redux-saga/effects';

import * as API from '../../api/post';
import {apiGetFeedEvent} from '../../api/event';

export function* postCreatePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostCreatePost(action.data);
    console.log('apiPostCreatePost -> ', response);

    yield put({
      type: 'createPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    console.log('apiPostCreatePost -> ', error);
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deletePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeletePost(action.data);
    console.log('apiDeletePost -> ', response);

    yield put({
      type: 'deletePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeletePost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deletePhotoDeletePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeletePhotoDeletePost(action.data);
    console.log('apiDeletePhotoDeletePost -> ', response);

    yield put({
      type: 'photoDeletePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeletePhotoDeletePost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putEditPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutEditPost(action.data);
    console.log('apiPutEditPost -> ', response);

    yield put({
      type: 'editPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutEditPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putPhotoEditPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutPhotoEditPost(action.data);
    console.log('apiPutPhotoEditPost -> ', response);

    yield put({
      type: 'photoEditPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutPhotoEditPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getPrivatePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetPrivatePost(action.data);
    console.log('apiGetPrivatePost -> ', response);

    yield put({
      type: 'privatePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetPrivatePost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getDiaryPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetDiaryPost(action.data);
    console.log('apiGetDiaryPost -> ', response);

    yield put({
      type: 'diaryPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetDiaryPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getFeedPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetFeedPost(action.data);
    console.log('apiGetFeedPost -> ', response);

    yield put({
      type: 'feedPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetFeedPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getMyPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetMyPost(action.data);
    console.log('apiGetMyPost -> ', response);

    yield put({
      type: 'myPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetMyPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getUserPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetUserPost(action.data);
    console.log('apiGetUserPost -> ', response);

    yield put({
      type: 'userPost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetUserPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getPublicUserPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetPublicUserPost(action.data);
    const post = response.data.data;

    console.log('apiGetPublicUserPost -> ', post);

    const response2 = yield apiGetFeedEvent({
      token: action.data.token,
    });
    const event = response2.data.data;
    console.log('apiGetFeedEvent -> ', event);

    yield put({
      type: 'publicUserPost',
      data: post,
    });

    yield put({
      type: 'myEvent',
      data: event,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    console.log('apiGetPublicUserPost -> ', error);
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getPost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetPost(action.data);
    console.log('apiGetPost -> ', response);

    yield put({
      type: 'post',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetPost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
