import {put} from 'redux-saga/effects';

import * as API from '../../api/pushToken';

export function* putPushToken(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutPushToken(action.data);
    console.log('apiPutPushToken -> ', response);

    // yield put({
    //   type: 'pushToken',
    //   data: response.data.data
    // });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutPushToken -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deletePushToken(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeletePushToken(action.data);
    console.log('apiDeletePushToken -> ', response);

    yield put({
      type: 'deletePushToken',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeletePushToken -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
