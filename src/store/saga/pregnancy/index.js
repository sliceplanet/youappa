import {put} from 'redux-saga/effects';

import * as API from '../../api/pregnancy';

export function* putUpdatePregnancy(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutUpdatePregnancy(action.data);
    console.log('apiPutUpdatePregnancy -> ', response);

    yield put({
      type: 'updatePregnancy',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutUpdatePregnancy -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getCurrentPregnancy(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetCurrentPregnancy(action.data);
    console.log('apiGetCurrentPregnancy -> ', response);

    yield put({
      type: 'currentPregnancy',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetCurrentPregnancy -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getPreviousPregnancy(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetPreviousPregnancy(action.data);
    console.log('apiGetPreviousPregnancy -> ', response);

    yield put({
      type: 'previousPregnancy',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetPreviousPregnancy -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getAllPregnancy(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetAllPregnancy(action.data);
    console.log('apiGetAllPregnancy -> ', response);

    yield put({
      type: 'allPregnancy',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetAllPregnancy -> ', error);
    yield put({type: 'toast', data: message});
    yield put({type: 'networkIndicator', data: false});
  }
}
