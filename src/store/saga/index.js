import {takeLatest} from 'redux-saga/effects';

import * as Main from './main';
import * as User from './user';
import * as Child from './child';
import * as Pregnancy from './pregnancy';
import * as Friend from './friend';
import * as Post from './post';
import * as Chat from './chat';
import * as Photo from './photo';
import * as Comment from './comment';
import * as Like from './like';
import * as Setting from './settings';
import * as PushToken from './pushToken';
import * as Subscriber from './subscriber';
import * as Wishlist from './wishlist';
import * as Event from './event';
import * as Support from './support';

function* dataSaga() {
  yield takeLatest('mainMother', Main.mother);

  yield takeLatest('postSignUp', User.postSignUp);
  yield takeLatest('postSignIn', User.postSignIn);
  yield takeLatest('postUpdate', User.postUpdate);
  yield takeLatest('getMe', User.getMe);
  yield takeLatest('logout', User.logout);

  yield takeLatest('getMyChild', Child.getMyChild);
  yield takeLatest('postAddChild', Child.postAddChild);
  yield takeLatest('putUpdateChild', Child.putUpdateChild);
  yield takeLatest('deleteChild', Child.deleteChild);

  yield takeLatest('putUpdatePregnancy', Pregnancy.putUpdatePregnancy);
  yield takeLatest('getCurrentPregnancy', Pregnancy.getCurrentPregnancy);
  yield takeLatest('getPreviousPregnancy', Pregnancy.getPreviousPregnancy);
  yield takeLatest('getAllPregnancy', Pregnancy.getAllPregnancy);

  yield takeLatest('getFriend', Friend.getFriend);
  yield takeLatest('getRequestFriend', Friend.getRequestFriend);
  yield takeLatest('getAcceptFriend', Friend.getAcceptFriend);
  yield takeLatest('getRemoveFriend', Friend.getRemoveFriend);
  yield takeLatest('getIncomingFriend', Friend.getIncomingFriend);
  yield takeLatest('getOutgoingFriend', Friend.getOutgoingFriend);
  yield takeLatest('getSearchFriend', Friend.getSearchFriend);

  yield takeLatest('postCreatePost', Post.postCreatePost);
  yield takeLatest('deletePost', Post.deletePost);
  yield takeLatest('deletePhotoDeletePost', Post.deletePhotoDeletePost);
  yield takeLatest('putEditPost', Post.putEditPost);
  yield takeLatest('putPhotoEditPost', Post.putPhotoEditPost);
  yield takeLatest('getPrivatePost', Post.getPrivatePost);
  yield takeLatest('getDiaryPost', Post.getDiaryPost);
  yield takeLatest('getFeedPost', Post.getFeedPost);
  yield takeLatest('getMyPost', Post.getMyPost);
  yield takeLatest('getUserPost', Post.getUserPost);
  yield takeLatest('getPublicUserPost', Post.getPublicUserPost);
  yield takeLatest('getPost', Post.getPost);

  yield takeLatest('postCreateChat', Chat.postCreateChat);
  yield takeLatest('deleteChat', Chat.deleteChat);
  yield takeLatest('postSendChat', Chat.postSendChat);
  yield takeLatest('putEditChat', Chat.putEditChat);
  yield takeLatest('getMessagesChat', Chat.getMessagesChat);

  yield takeLatest('postUploadPhoto', Photo.postUploadPhoto);
  yield takeLatest('deletePhoto', Photo.deletePhoto);

  yield takeLatest('postCreateComment', Comment.postCreateComment);
  yield takeLatest('postEditComment', Comment.postEditComment);
  yield takeLatest('deleteComment', Comment.deleteComment);
  // yield takeLatest('getCommentPost', Comment.getCommentPost);

  yield takeLatest('putLikeComment', Like.putLikeComment);
  yield takeLatest('putUnlikeComment', Like.putUnlikeComment);
  yield takeLatest('putLikePost', Like.putLikePost);
  yield takeLatest('putUnlikePost', Like.putUnlikePost);

  yield takeLatest('getSettings', Setting.getSettings);
  yield takeLatest('postUpdateSettings', Setting.postUpdateSettings);

  yield takeLatest('putPushToken', PushToken.putPushToken);
  yield takeLatest('deletePushToken', PushToken.deletePushToken);

  yield takeLatest('getAddSubscriber', Subscriber.getAddSubscriber);
  yield takeLatest('getRemoveSubscriber', Subscriber.getRemoveSubscriber);
  yield takeLatest('getSearchSubscriber', Subscriber.getSearchSubscriber);

  yield takeLatest('postAddWishlist', Wishlist.postAddWishlist);
  yield takeLatest('putUpdateWishlist', Wishlist.putUpdateWishlist);
  yield takeLatest('putAddProductWishlist', Wishlist.putAddProductWishlist);
  yield takeLatest('putProductWishlist', Wishlist.putProductWishlist);
  yield takeLatest('deleteProductWishlist', Wishlist.deleteProductWishlist);
  yield takeLatest('getMyWishlist', Wishlist.getMyWishlist);
  yield takeLatest('deleteWishlist', Wishlist.deleteWishlist);

  yield takeLatest('getMyEvent', Event.getMyEvent);
  yield takeLatest('getFeedEvent', Event.getFeedEvent);
  yield takeLatest('postCreateEvent', Event.postCreateEvent);
  yield takeLatest('putEditEvent', Event.putEditEvent);
  yield takeLatest('deleteEvent', Event.deleteEvent);
  yield takeLatest('getEvent', Event.getEvent);

  yield takeLatest('postPrivateReport', Support.postPrivateReport);
}

export default dataSaga;
