import {put} from 'redux-saga/effects';

import * as API from '../../api/event';
import * as PHOTO from '../../api/photo';

export function* getMyEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetMyEvent(action.data);
    console.log('apiGetMyEvent -> ', response);

    yield put({
      type: 'myEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetMyEvent -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getFeedEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetFeedEvent(action.data);
    console.log('apiGetFeedEvent -> ', response);

    yield put({
      type: 'feedEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetFeedEvent -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* postCreateEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const {token} = action.data;

    let promises = [];

    action.data.data.photos.forEach(element => {
      const promise = new Promise((resolve, reject) => {
        try {
          const data = {
            image: element,
          };
          resolve(PHOTO.apiPostUploadPhoto({token, data}));
        } catch (e) {
          reject(e);
        }
      });
      promises = [...promises, promise];
    });

    let photos = yield Promise.all(promises);
    photos = photos.filter(e => e.status === 200).map(e => e.data.data._id);

    const {date, text, title, type, wishlists, invites} = action.data.data;
    const data = {date, text, title, type, wishlists, photos, invites};

    const response = yield API.apiPostCreateEvent({token, data});
    console.log('apiPostCreateEvent -> ', response);

    yield put({
      type: 'createEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    console.log('apiPostCreateEvent -> ', error);
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putEditEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutEditEvent(action.data);
    console.log('apiPutEditEvent -> ', response);

    yield put({
      type: 'putEditEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutEditEvent -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* deleteEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiDeleteEvent(action.data);
    console.log('apiDeleteEvent -> ', response);

    yield put({
      type: 'deleteEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiDeleteEvent -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getEvent(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetEvent(action.data);
    console.log('apiGetEvent -> ', response);

    yield put({
      type: 'getEvent',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetEvent -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
