import {put} from 'redux-saga/effects';

import * as API from '../../api/subscriber';

export function* getAddSubscriber(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetAddSubscriber(action.data);
    console.log('apiGetAddSubscriber -> ', response);

    yield put({
      type: 'addSubscriber',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetAddSubscriber -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getRemoveSubscriber(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetRemoveSubscriber(action.data);
    console.log('apiGetRemoveSubscriber -> ', response);

    yield put({
      type: 'removeSubscriber',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetRemoveSubscriber -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getSearchSubscriber(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiGetSearchSubscriber(action.data);
    console.log('apiGetSearchSubscriber -> ', response);

    yield put({
      type: 'searchSubscriber',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetSearchSubscriber -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
