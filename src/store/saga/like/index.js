import {put} from 'redux-saga/effects';

import * as API from '../../api/like';

export function* putLikeComment(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutLikeComment(action.data);
    console.log('apiPutLikeComment -> ', response);

    yield put({
      type: 'likeComment',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutLikeComment -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putUnlikeComment(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutUnlikeComment(action.data);
    console.log('apiPutUnlikeComment -> ', response);

    yield put({
      type: 'unlikeComment',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutUnlikeComment -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putLikePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutLikePost(action.data);
    console.log('apiPutLikePost -> ', response);

    yield put({
      type: 'likePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutLikePost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* putUnlikePost(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPutUnlikePost(action.data);
    console.log('apiPutUnlikePost -> ', response);

    yield put({
      type: 'unlikePost',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiPutUnlikePost -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
