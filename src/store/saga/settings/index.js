import {put} from 'redux-saga/effects';

import * as API from '../../api/settings';

export function* postUpdateSettings(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const response = yield API.apiPostUpdateSettings(action.data);
    console.log('apiPostUpdateSettings -> ', response);

    yield put({
      type: 'updateSettings',
      data: response.data.data,
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;

    console.log('apiPostUpdateSettings -> ', error.response);

    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}

export function* getSettings(action) {
  try {
    yield put({type: 'networkIndicator', data: true});
    const {token} = action.data;
    const response = yield API.apiGetSettings({token});
    console.log('apiGetSettings -> ', response);

    yield put({
      type: 'settings',
      data: {
        token,
        data: response.data.data,
      },
    });
    yield put({type: 'networkIndicator', data: false});
  } catch (error) {
    const {message} = error.response.data;
    console.log('apiGetSettings -> ', error.response);
    yield put({type: 'toast', data: {message}});
    yield put({type: 'networkIndicator', data: false});
  }
}
