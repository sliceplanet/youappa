import React from 'react';
import {Platform, Linking} from 'react-native';

// Screens
import Auth from './directions/Auth';
import StepMother from './directions/Step/Mother';
import StepFriend from './directions/Step/Friend';
import TabMother from './directions/Tab/Mother';
import TabFriend from './directions/Tab/Friend';

// Helpers
import NavigationService from '../helpers/navigation';

export default class Route extends React.PureComponent {
  componentDidMount() {
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        if (url !== null) {
          this.navigate(url);
        }
      });
    } else {
      Linking.addEventListener('url', this.handleOpenURL);
    }
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
  }

  handleOpenURL = event => {
    this.navigate(event.url);
  };

  navigate = deepLink => {
    console.log(deepLink);
    const url = deepLink.split('://')[0];
    if (url === 'bbox') {
      const link = deepLink.split('://')[1];
      const links = link.split('/');
      const params = links[links.length - 1].split('&');
      const resetCode = params[0].split('=')[1];
      const type = params[1].split('=')[1];
      if (type === '1') {
        NavigationService.navigate('SavePasswordMother', {resetCode});
      } else {
        NavigationService.navigate('SavePasswordFriend', {resetCode});
      }
    }
  };

  render() {
    const {user} = this.props;
    console.log('Route Direction -> ', user);
    if (user.token.length > 0) {
      const u = user.users.filter(e => e.token === user.token);

      if (u.length > 0) {
        if (u[0].type === 1) {
          if (u[0].registrationStep === 0) {
            return <StepMother />;
          }
          return (
            <>
              <TabMother />
              {/* <OneSignalView /> */}
            </>
          );
        }
        if (u[0].registrationStep === 0) {
          return <StepFriend />;
        }
        return (
          <>
            <TabFriend />
            {/* <OneSignalView /> */}
          </>
        );
      }
    }
    return <Auth />;
  }
}
