import React from 'react';
import Navigation from './Navigation';
import NavigationService from '../../../../helpers/navigation';

export default class StepMother extends React.PureComponent {
  render() {
    return (
      <Navigation
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
