import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// Screens
import GestationalAge from '../../../../components/Step/Mother/GestationalAge';
import AddChild from '../../../../components/Step/Mother/AddChild';
import ChildPhoto from '../../../../components/Step/Mother/ChildPhoto';
import InfoMother from '../../../../components/Step/Mother/InfoMother';
import LoadAvatar from '../../../../components/Step/Mother/LoadAvatar';

// Components
// import headerBackImage from '../../../../components/Base/Header/HeaderBackImage';

const navigationOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    backgroundColor: '#F4F4F6',
  },
};

const defaultNavigationOptions = {
  // headerBackImage,
  headerBackTitle: null,
  gesturesEnabled: true,
};

const Route = createStackNavigator(
  {
    GestationalAge: {
      screen: GestationalAge,
      navigationOptions,
    },
    InfoMother: {
      screen: InfoMother,
      navigationOptions,
    },
    AddChild: {
      screen: AddChild,
      navigationOptions,
    },
    ChildPhoto: {
      screen: ChildPhoto,
      navigationOptions,
    },
    LoadAvatar: {
      screen: LoadAvatar,
      navigationOptions,
    },
  },
  {
    initialRouteName: 'GestationalAge',
    mode: 'modal',
    headerMode: 'screen',
    defaultNavigationOptions,
  },
);

const Navigation = createAppContainer(Route);
export default Navigation;
