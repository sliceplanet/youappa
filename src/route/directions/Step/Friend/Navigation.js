import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import AvatarFriend from '../../../../components/Step/Friend/AvatarFriend';
import InfoFriend from '../../../../components/Step/Friend/InfoFriend';

// import headerBackImage from '../../../../components/Base/Header/HeaderBackImage';

const navigationOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    backgroundColor: '#F4F4F6',
  },
};

const defaultNavigationOptions = {
  // headerBackImage,
  headerBackTitle: null,
  gesturesEnabled: true,
};

const Route = createStackNavigator(
  {
    AvatarFriend: {
      screen: AvatarFriend,
      navigationOptions,
    },
    InfoFriend: {
      screen: InfoFriend,
      navigationOptions,
    },
  },
  {
    initialRouteName: 'AvatarFriend',
    mode: 'modal',
    headerMode: 'screen',
    defaultNavigationOptions,
  },
);

const Navigation = createAppContainer(Route);
export default Navigation;
