import React from 'react';
import Navigation from './Navigation';
import NavigationService from '../../../../helpers/navigation';

export default class StepFriend extends React.PureComponent {
  render() {
    return (
      <Navigation
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
