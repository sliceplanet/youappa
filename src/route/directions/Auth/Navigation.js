import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// Screens
import PreloadMother from '../../../components/Auth/Mother/PreloadMother';
import SignInMother from '../../../components/Auth/Mother/SignInMother';
import SignUpMother from '../../../components/Auth/Mother/SignUpMother';
import ForgotPasswordMother from '../../../components/Auth/Mother/ForgotPasswordMother';
import SavePasswordMother from '../../../components/Auth/Mother/SavePasswordMother';

import PreloadFriend from '../../../components/Auth/Friend/PreloadFriend';
import SignInFriend from '../../../components/Auth/Friend/SignInFriend';
import SignUpFriend from '../../../components/Auth/Friend/SignUpFriend';
import ForgotPasswordFriend from '../../../components/Auth/Friend/ForgotPasswordFriend';
import SavePasswordFriend from '../../../components/Auth/Friend/SavePasswordFriend';

// Components
// import headerBackImage from '../../../components/Base/Header/HeaderBackImage';

const navigationOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    backgroundColor: '#F4F4F6',
  },
};

const defaultNavigationOptions = {
  // headerBackImage,
  headerBackTitle: null,
  gesturesEnabled: true,
};

const Route = createStackNavigator(
  {
    PreloadMother: {
      screen: PreloadMother,
      navigationOptions: {
        header: null,
      },
    },
    PreloadFriend: {
      screen: PreloadFriend,
      navigationOptions: {
        header: null,
      },
    },
    SignInMother: {
      screen: SignInMother,
      navigationOptions,
    },
    SignInFriend: {
      screen: SignInFriend,
      navigationOptions,
    },
    SignUpMother: {
      screen: SignUpMother,
      navigationOptions,
    },
    SignUpFriend: {
      screen: SignUpFriend,
      navigationOptions,
    },
    ForgotPasswordMother: {
      screen: ForgotPasswordMother,
      navigationOptions,
    },
    ForgotPasswordFriend: {
      screen: ForgotPasswordFriend,
      navigationOptions,
    },
    SavePasswordMother: {
      screen: SavePasswordMother,
      navigationOptions,
    },
    SavePasswordFriend: {
      screen: SavePasswordFriend,
      navigationOptions,
    },
  },
  {
    initialRouteName: 'PreloadMother',
    mode: 'modal',
    headerMode: 'screen',
    defaultNavigationOptions,
  },
);
const Navigation = createAppContainer(Route);
export default Navigation;
