import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: wp(1),
  },
  wrap: {
    padding: wp(4),
    alignItems: 'flex-start',
  },
  image: {
    width: wp(16),
    height: wp(16),
    borderRadius: wp(16),
    overflow: 'hidden',
    marginRight: wp(2),
  },
  location: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#A6A6A6',
    paddingLeft: wp(1),
  },
  text: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(4),
    color: 'black',
    paddingVertical: wp(1),
  },
  followers: {
    fontFamily: 'Roboto-Light',
    fontSize: wp(3),
    color: '#949494',
    paddingVertical: wp(1),
    paddingHorizontal: wp(2),
    borderColor: '#949494',
    borderWidth: 0.7,
    borderRadius: wp(3),
    marginRight: wp(2),
  },
  align: {
    alignSelf: 'flex-start',
    alignItems: 'center',
  },
  nav: {
    fontFamily: 'SFUIDisplay-Medium',
    fontSize: wp(4),
    color: '#3F3F3F',
    marginVertical: wp(2),
  },
  line: {
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 0.7,
    marginBottom: wp(4),
    paddingBottom: wp(4),
  },
});

export default {base};
