import {connect} from 'react-redux';
import component from './component';

import {logout} from '../../../../../store/actions/user';

function mapStateToProps(state) {
  return {
    user: state.user,
    babies: state.babies,
    friends: state.friends,
    wishlist: state.wishlist,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: data => dispatch(logout(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
