import React from 'react';
import {
  View,
  SafeAreaView,
  Image as Img,
  Text,
  TouchableOpacity,
} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import I18n from 'i18n-js';

import Image from '../../../../../components/Base/Image';

// Helpers
import {getUser} from '../../../../../helpers';
import NavigationService from '../../../../../helpers/navigation';
import * as Images from '../../../../../helpers/images';
import {URI} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class DrawerMenu extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  navigateSettings = () => {
    NavigationService.navigate('Settings');
  };

  navigateEvent = () => {
    NavigationService.navigate('Events');
  };

  navigateCalendar = () => {
    NavigationService.navigate('Calendar');
  };

  navigateInviteFriends = () => {};

  navigateFriends = () => {
    NavigationService.navigate('Friends');
  };

  navigateChild = () => {
    NavigationService.navigate('Profile');
  };

  navigateWishlist = () => {
    NavigationService.navigate('Wishlist', {
      type: 1,
    });
  };

  logout = () => {
    this.props.logout();
  };

  render() {
    const {city, name, avatar_url} = getUser(this.props.user);
    return (
      <SafeAreaView style={base.flex}>
        <View style={base.wrap}>
          <View style={[base.row, base.line]}>
            {avatar_url ? (
              <Image
                uri={`${URI}${avatar_url}`}
                style={base.image}
                containerStyle={base.image}
              />
            ) : (
              <Img source={Images.profile} style={base.image} />
            )}

            <View style={base.flex}>
              <View style={base.row}>
                <ScalableImage source={Images.location} height={wp(4)} />
                <Text style={base.location}>
                  {I18n.t('c')} {city}
                </Text>
              </View>
              <View style={base.row}>
                <Text style={base.text}>{name}</Text>
                <View style={base.flex} />
                <ScalableImage
                  width={wp(6)}
                  source={Images.settings}
                  onPress={this.navigateSettings}
                />
              </View>
              <View style={base.row}>
                <TouchableOpacity
                  onPress={this.navigateChild}
                  style={base.align}>
                  <Text style={base.followers}>
                    {this.props.babies.length} {I18n.t('child')}
                  </Text>
                </TouchableOpacity>
                <View style={base.flex} />
                <TouchableOpacity onPress={this.navigateFriends}>
                  <View style={base.align}>
                    <Text style={base.followers}>
                      {this.props.friends.length} {I18n.t('followers')}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          {/* <TouchableOpacity onPress={this.navigateEvent}>
            <Text style={base.nav}>{I18n.t('event')}</Text>
          </TouchableOpacity> */}

          <TouchableOpacity onPress={this.navigateCalendar}>
            <Text style={base.nav}>{I18n.t('calendar')}</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity onPress={this.navigateInviteFriends}>
            <Text style={base.nav}>{I18n.t('invite_friends')}</Text>
          </TouchableOpacity> */}

          <TouchableOpacity onPress={this.navigateWishlist}>
            <Text style={base.nav}>{I18n.t('wishlist_mother')}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.logout}>
            <Text style={base.nav}>{I18n.t('logout')}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
