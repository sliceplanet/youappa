import React from 'react';
import Navigation from './Navigation';
import NavigationService from '../../../../helpers/navigation';

export default class TabMother extends React.PureComponent {
  componentDidMount() {
    const {token} = this.props.user;
    this.props.main({token});
  }

  render() {
    return (
      <Navigation
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
