import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';

// Screens
import Home from '../../../../components/Tab/Mother/Home';
import Profile from '../../../../components/Tab/Mother/Profile';
import Wishlist from '../../../../components/Tab/Mother/Wishlist';
import Child from '../../../../components/Tab/Mother/Child';
import Calendar from '../../../../components/Tab/Mother/Calendar';
import AddEvent from '../../../../components/Tab/Mother/AddEvent';
import Events from '../../../../components/Tab/Mother/Events';
import PhotoFilter from '../../../../components/Tab/Mother/PhotoFilter';
import PhotoDescription from '../../../../components/Tab/Mother/PhotoDescription';
import Subscriber from '../../../../components/Tab/Mother/Subscriber';
import Settings from '../../../../components/Tab/Mother/Settings';
import Support from '../../../../components/Tab/Mother/Support';
import Friends from '../../../../components/Tab/Mother/Friends';
import Comments from '../../../../components/Tab/Mother/Comments';
import InviteFriends from '../../../../components/Tab/Mother/InviteFriends';
import Web from '../../../../components/Tab/Mother/Web';
import AddChild from '../../../../components/Step/Mother/AddChild';
import ChildPhoto from '../../../../components/Step/Mother/ChildPhoto';
import GestationalAge from '../../../../components/Step/Mother/GestationalAge';
import InfoMother from '../../../../components/Tab/Mother/InfoMother';

// Components
// import headerBackImage from '../../../../components/Header/HeaderBackImage';
import DrawerMenu from './DrawerMenu';

const navigationOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
  },
};

const defaultNavigationOptions = {
  // headerBackImage,
  headerBackTitle: null,
  gesturesEnabled: true,
};

const Route = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions,
    },
    Profile: {
      screen: Profile,
      navigationOptions,
    },
    Wishlist: {
      screen: Wishlist,
      navigationOptions,
    },
    Child: {
      screen: Child,
      navigationOptions,
    },
    Calendar: {
      screen: Calendar,
      navigationOptions,
    },
    AddEvent: {
      screen: AddEvent,
      navigationOptions,
    },
    Events: {
      screen: Events,
      navigationOptions,
    },
    PhotoFilter: {
      screen: PhotoFilter,
      navigationOptions,
    },
    PhotoDescription: {
      screen: PhotoDescription,
      navigationOptions,
    },
    Subscriber: {
      screen: Subscriber,
      navigationOptions,
    },
    Settings: {
      screen: Settings,
      navigationOptions,
    },
    Support: {
      screen: Support,
      navigationOptions,
    },
    Friends: {
      screen: Friends,
      navigationOptions,
    },
    Comments: {
      screen: Comments,
      navigationOptions,
    },
    InviteFriends: {
      screen: InviteFriends,
      navigationOptions,
    },
    Web: {
      screen: Web,
      navigationOptions,
    },
    AddChild: {
      screen: AddChild,
      navigationOptions,
    },
    ChildPhoto: {
      screen: ChildPhoto,
      navigationOptions,
    },
    GestationalAge: {
      screen: GestationalAge,
      navigationOptions,
    },
    InfoMother: {
      screen: InfoMother,
      navigationOptions,
    },
  },
  {
    initialRouteName: 'Home',
    mode: 'modal',
    headerMode: 'screen',
    defaultNavigationOptions,
  },
);

const Drawer = createDrawerNavigator(
  {
    Drawer: {
      screen: Route,
    },
  },
  {
    contentComponent: DrawerMenu,
  },
);

const Navigation = createAppContainer(Drawer);
export default Navigation;
