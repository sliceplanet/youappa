import {connect} from 'react-redux';
import component from './component';

import {mainMother} from '../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    main: item => dispatch(mainMother(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
