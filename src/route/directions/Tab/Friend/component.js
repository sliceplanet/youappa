import React from 'react';
import Navigation from './Navigation';
import NavigationService from '../../../../helpers/navigation';

export default class StepMother extends React.PureComponent {
  componentDidMount() {
    const {token} = this.props.user;

    this.props.getSettings({token});
    this.props.getFriend({token});
  }

  render() {
    return (
      <Navigation
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
