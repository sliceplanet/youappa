import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';

// Screens
import Home from '../../../../components/Tab/Friend/Home';
import Wall from '../../../../components/Tab/Friend/Wall';
import Settings from '../../../../components/Tab/Friend/Settings';
import Support from '../../../../components/Tab/Friend/Support';
import Finder from '../../../../components/Tab/Friend/Finder';
import Mother from '../../../../components/Tab/Friend/Mother';
import Followers from '../../../../components/Tab/Friend/Followers';
import Wishlist from '../../../../components/Tab/Friend/Wishlist';

// Components
// import headerBackImage from '../../../../components/Header/HeaderBackImage';
import DrawerMenu from './DrawerMenu';

const navigationOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
  },
};

const defaultNavigationOptions = {
  // headerBackImage,
  headerBackTitle: null,
  gesturesEnabled: true,
};

const Route = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions,
    },
    Finder: {
      screen: Finder,
      navigationOptions,
    },
    Wall: {
      screen: Wall,
      navigationOptions,
    },
    Settings: {
      screen: Settings,
      navigationOptions,
    },
    Support: {
      screen: Support,
      navigationOptions,
    },
    Mother: {
      screen: Mother,
      navigationOptions: {
        headerStyle: {
          borderBottomWidth: 0,
          elevation: 0,
          height: 0,
        },
      },
    },
    Followers: {
      screen: Followers,
      navigationOptions,
    },
    Wishlist: {
      screen: Wishlist,
      navigationOptions,
    },
  },
  {
    initialRouteName: 'Home',
    mode: 'modal',
    headerMode: 'screen',
    defaultNavigationOptions,
  },
);

const Drawer = createDrawerNavigator(
  {
    Drawer: {
      screen: Route,
    },
  },
  {
    contentComponent: DrawerMenu,
  },
);

const Navigation = createAppContainer(Drawer);

export default Navigation;
