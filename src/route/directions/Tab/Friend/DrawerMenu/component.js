import React from 'react';
import {
  View,
  SafeAreaView,
  Image as Img,
  Text,
  TouchableOpacity,
} from 'react-native';
import I18n from 'i18n-js';
import ScalableImage from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Image from '../../../../../components/Base/Image';

// Helpers
import {getUser} from '../../../../../helpers';
import NavigationService from '../../../../../helpers/navigation';
import * as Images from '../../../../../helpers/images';
import {URI} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class DrawerMenu extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  navigateSettings = () => {
    NavigationService.navigate('Settings');
  };

  navigateEvent = () => {
    NavigationService.navigate('Events');
  };

  navigateCalendar = () => {
    NavigationService.navigate('Calendar');
  };

  navigateInviteFriends = () => {};

  logout = () => {
    this.props.logout();
  };

  render() {
    const {city, name, avatar_url} = getUser(this.props.user);
    return (
      <SafeAreaView style={base.flex}>
        <View style={base.wrap}>
          <View style={[base.row, base.line]}>
            {avatar_url ? (
              <Image
                uri={URI + avatar_url}
                style={base.image}
                containerStyle={base.image}
              />
            ) : (
              <Img source={Images.profile} style={base.image} />
            )}
            <View style={base.flex}>
              <View style={base.row}>
                <ScalableImage source={Images.location} height={wp(4)} />
                <Text style={base.location}>
                  {I18n.t('c')} {city}
                </Text>
              </View>
              <View style={base.row}>
                <Text style={base.text}>{name}</Text>
                <View style={base.flex} />
                <ScalableImage
                  width={wp(6)}
                  source={Images.settings}
                  onPress={this.navigateSettings}
                />
              </View>
              {/* <View style={base.align}>
                <Text style={base.followers}>1 {I18n.t('child')}</Text>
              </View> */}
            </View>
          </View>

          {/* <TouchableOpacity onPress={this.navigateEvent}>
            <Text style={base.nav}>{I18n.t('alert')}</Text>
          </TouchableOpacity> */}

          <TouchableOpacity onPress={this.logout}>
            <Text style={base.nav}>{I18n.t('logout')}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
