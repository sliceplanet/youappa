import {connect} from 'react-redux';
import component from './component';

import {getSettings} from '../../../../store/actions/settings';
import {getPublicUserPost} from '../../../../store/actions/post';
import {getFriend} from '../../../../store/actions/friend';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getPublicUserPost: item => dispatch(getPublicUserPost(item)),
    getSettings: item => dispatch(getSettings(item)),
    getFriend: item => dispatch(getFriend(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
