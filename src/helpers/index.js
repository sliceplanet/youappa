/* eslint-disable no-useless-escape */
/* eslint-disable no-undef */
import I18n from 'i18n-js';
import * as Images from './images';

export const LANG = ['ru', 'en', 'es'];

export const RELATIVE = () => {
  const r = [
    {
      text: I18n.t('husband'),
      id: 2,
    },
    {
      text: I18n.t('relative'),
      id: 3,
    },
    {
      text: I18n.t('friend'),
      id: 4,
    },
  ];
  return r;
};

export const FILTERS = [
  '_1977',
  'Aden',
  'Brannan',
  'Brooklyn',
  'Clarendon',
  'Earlybird',
  'Gingham',
  'Hudson',
  'Inkwell',
  'Kelvin',
  'Lark',
  'Lofi',
  'Maven',
  'Mayfair',
  'Moon',
  'Nashville',
  'Perpetua',
  'Reyes',
  'Rise',
  'Slumber',
  'Stinson',
  'Toaster',
  'Valencia',
  'Walden',
  'Willow',
  'Xpro2',
];

export const locales = {
  ru: {
    amDesignator: 'до полудня',
    pmDesignator: 'после полудня',
    dayNames: [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
    ],
    dayNamesShort: ['НД', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
    monthNames: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь',
    ],
    monthNamesShort: [
      'Янв',
      'Февр',
      'Мар',
      'Апр',
      'Мая',
      'Июн',
      'Июл',
      'Авг',
      'Сент',
      'Окт',
      'Нояб',
      'Дек',
    ],
  },
  en: {
    amDesignator: 'am',
    pmDesignator: 'pm',
    dayNames: [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ],
    dayNamesShort: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
    monthNames: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    monthNamesShort: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'June',
      'July',
      'Aug',
      'Sept',
      'Oct',
      'Nov',
      'Dec',
    ],
  },
  sp: {
    amDesignator: 'am',
    pmDesignator: 'pm',
    dayNames: [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ],
    dayNamesShort: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
    monthNames: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    monthNamesShort: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'June',
      'July',
      'Aug',
      'Sept',
      'Oct',
      'Nov',
      'Dec',
    ],
  },
};

export const COVERS = [
  null,
  Images.cover1,
  Images.cover2,
  Images.cover3,
  Images.cover4,
];

export function WISH() {
  const w = [
    {category: 0, title: I18n.t('to_the_hospital')},
    {category: 1, title: I18n.t('statement')},
    {category: 2, title: 'Baby shower'},
    {category: 3, title: I18n.t('photo_albums')},
    {category: 4, title: I18n.t('birthday_1_year')},
    {category: 5, title: I18n.t('furniture')},
    {category: 6, title: I18n.t('medicine')},
    {category: 7, title: I18n.t('cosmetics')},
    {category: 8, title: I18n.t('furniture')},
  ];
  return w;
}

export const AVATARS = [
  Images.frame1,
  Images.frame2,
  Images.frame3,
  Images.frame4,
  Images.frame5,
  Images.frame6,
  Images.frame7,
  Images.frame8,
  Images.frame9,
  Images.frame10,
  Images.frame11,
  Images.frame12,
  Images.frame13,
  Images.frame14,
  Images.frame15,
  Images.frame16,
  Images.frame17,
  Images.frame18,
];

export function checkEmail(mail) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(mail).toLowerCase());
}

export function getUser(user) {
  const {token} = user;
  const u = user.users.filter(e => e.token === token);
  if (u.length > 0) {
    return u[0];
  }
  return null;
}
