import {NavigationActions, StackActions} from 'react-navigation';

const navigator = {
  navigation: null,
  setTopLevelNavigator: navigatorRef => {
    navigator.navigation = navigatorRef;
  },
  navigate: (routeName, params) => {
    navigator.navigation.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      }),
    );
  },
  reset: (routeName, params) => {
    navigator.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName, params})],
      }),
    );
  },
  push: (routeName, params) => {
    navigator.navigation.dispatch(
      StackActions.push({
        routeName,
        params,
      }),
    );
  },
  goBack: () => {
    navigator.navigation.dispatch(NavigationActions.back());
  },
};

export default navigator;
